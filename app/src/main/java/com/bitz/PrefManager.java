package com.bitz;

import android.content.Context;
import android.content.SharedPreferences;

public class PrefManager {
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;
    int PRIVATE_MODE = 0;
    private static final String PREF_NAME = "androidhive-welcome";
    private static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";
    public static final String ID = "ID";
    public static final String Connection_USERID = "Connection_USERID";
    public static final String NMAE = "NMAE";
    public static final String EMAIL = "EMAIL";
    public static final String TOKEN = "TOKEN";
    public static final String CHANNEL = "CHANNEL";
    public static final String OTHER_ID = "OTHER_ID";
    public static final String OTHER_NAME = "OTHER_NAME";
    public static final String LOGINTOKEN = "LOGINTOKEN";
    public static final String USERTYPE = "USERTYPE";
    public static final String LOGIN_NAME_FINAL = "LOGIN_NAME_FINAL";
    public static final String FIREBAE_LOGIN_TOKEN = "FIREBAE_LOGIN_TOKEN";
    public static final String FIREBASE_USER_COUNT = "FIREBASE_USER_COUNT";
    public static final String FIREBASE_AUDIENCE_AUTO_ID = "FIREBASE_AUDIENCE_AUTO_ID";
    public static final String PROFILE_AVATAR = "PROFILE AVATAR";

    public PrefManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setFirstTimeLaunch(boolean isFirstTime) {
        editor.putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTime);
        editor.commit();
    }

    public boolean isFirstTimeLaunch() {
        return pref.getBoolean(IS_FIRST_TIME_LAUNCH, true);
    }

    public static SharedPreferences.Editor getEditor(Context context) {
        SharedPreferences sharedpreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        return editor;
    }

    public static SharedPreferences getSharedPreference(Context context) {
        SharedPreferences sharedpreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        return sharedpreferences;
    }

    public static String getString(Context context , String name) {
        SharedPreferences sharedPreferences=getSharedPreference(context);
        return sharedPreferences.getString(name, "");
    }

    public static void setString(Context context , String name , String value) {
        SharedPreferences.Editor editor=getEditor(context);
        editor.putString(name, value);
        editor.commit();
    }
}

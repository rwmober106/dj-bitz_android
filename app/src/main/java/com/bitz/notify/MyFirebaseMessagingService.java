
package com.bitz.notify;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.media.AudioAttributes;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.bitz.R;
import com.bitz.live_video.AudienceActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.List;
import java.util.Map;


public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    NotificationCompat.Builder notification;


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());
        Log.d(TAG, "From: " + remoteMessage.getData());



        Map<String, String> data = remoteMessage.getData();
        Log.d(TAG, "From: " + data.get(0));
        Object value = data.get(0);
        Object value1 = remoteMessage.getData();

        //Log.d(TAG, "Data: " + remoteMessage.getData()); //Whole data
        //Log.d(TAG, "Key Data : " +  remoteMessage.getData().get("key").toString());

        /*Log.d(TAG, "From value: " + value);
        Log.d(TAG, "From value1: " + remoteMessage.getNotification().getTitle());
        Log.d(TAG, "From value1: " + remoteMessage.getNotification().getBody());
        Log.d(TAG, "From value1: " + remoteMessage.getNotification().getBodyLocalizationKey());
        Log.d(TAG, "From value1: " + remoteMessage.getNotification().getTag());
        Log.d(TAG, "From value1: " + remoteMessage.getNotification().getTitleLocalizationKey());
        Log.d(TAG, "From value1: " + remoteMessage.getNotification().getBodyLocalizationArgs());
        Log.d(TAG, "From value1: " + remoteMessage.getNotification().getLink());*/



        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
        //sendNotification(remoteMessage.getData().get("message"),remoteMessage.getData().get("title"));

        //sendNotification(remoteMessage.getData().get("message"),remoteMessage.getData().get("title"));
        if (!isAppIsInBackground(getApplicationContext()))
        {
            //Toast.makeText(this, "Not Backgound", Toast.LENGTH_SHORT).show();
            //sendNotification(remoteMessage.getData().get("message"),remoteMessage.getData().get("title"));
        }
        else
        {
            //Toast.makeText(this, "Backgound", Toast.LENGTH_SHORT).show();
            //sendNotification(remoteMessage.getData().get("message"),remoteMessage.getData().get("title"));
        }
    }

    public static boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }

        return isInBackground;
    }

    @SuppressLint("WrongConstant")
    private void sendNotification(String messageBody, String title) {

        String id ="my_channel_01";
        Intent intent = new Intent(this, AudienceActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder notificationBulderOreo = new NotificationCompat.Builder(getApplicationContext(),id);
        notificationBulderOreo .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)
                .setPriority(Notification.PRIORITY_MAX);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "Waite customer";// The user-visible name of the channel.
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel(id,name,importance);
            AudioAttributes attributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                    .build();

            mChannel.setDescription(messageBody);
            mChannel.enableLights(true);
            mChannel.enableVibration(true);
            mChannel.setSound(defaultSoundUri,attributes);
            mChannel.setLightColor(getResources().getColor(R.color.colorAccent));
            notificationManager.createNotificationChannel(mChannel);

            notificationBulderOreo.setBadgeIconType(R.drawable.main_logo)
                    .setChannelId(id)
                    .setWhen(System.currentTimeMillis());
        }
        else {
            notificationBulderOreo.setColor(getResources().getColor(R.color.colorAccent));
        }

        notificationManager.notify(0, notificationBulderOreo.build());

        // mPlayer = MediaPlayer.create(MyFirebaseMessagingService.this, R.raw.sound1);
        // mPlayer.start();
    }
}
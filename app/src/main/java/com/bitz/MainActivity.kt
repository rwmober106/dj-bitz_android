package com.bitz

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.media.MediaPlayer
import android.net.Uri
import android.util.TypedValue
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.bitz.retrofit2.DJBitzInterFace
import com.bitz.retrofit2.DJBitzModel
import kotlinx.android.synthetic.main.activity_main.*
import android.os.*
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.bitz.extensions.config
import com.bitz.extensions.sendIntent
import com.bitz.utils.*
import com.google.firebase.messaging.FirebaseMessaging
import com.simplemobiletools.musicplayer.helpers.BusProvider
import com.squareup.otto.Bus
import com.squareup.otto.Subscribe
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {

    companion object {

        fun newInstance(): MainActivity {
            return MainActivity()
        }
    }

    val DJBitzApiServe by lazy {
        DJBitzInterFace.create()
    }

    private var initPadding = 60
    private var playingPadding = 110

    private var currentMixTab = -1

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_home -> {
                showHideCurrentMix(0)
                displayFragment(libraryFragment, "LibraryFragment")
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_artist -> {
                showHideCurrentMix(1)
                displayFragment(artistFragment, "ArtistFragment")
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_search -> {
                showHideCurrentMix(2)
                displayFragment(searchFragment, "LibraryFragment")
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_contact -> {
                showHideCurrentMix(3)
                displayFragment(contactFragment, "ArtistFragment")
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    private lateinit var libraryFragment: LibraryFragment
    private lateinit var artistFragment: ArtistFragment
    private lateinit var searchFragment: SearchFragment
    private lateinit var contactFragment: ContactFragment
    private lateinit var currentMixFragment: CurrentMixFragment
    //    var mediaPlayer = MediaPlayer()
    var bShowCurrentMixFragment = false

    var beat_play: ImageButton? = null
    var beat_loading: ProgressBar? = null
    private var beat_seek_bar: SeekBar? = null
    var beat_seek_bar_offset: Int = 3 // 3%
    var beat_seek_bar_length: Int = 10


    var currentMusicPlayer: Int = 0//"https://s3.amazonaws.com/kargopolov/kukushka.mp3"

    //var listMusicsPlayerNow: ArrayList<DJBitzModel.DJBitzMusicList.MusicRecords>? = null
    var listMusicsPlayer: ArrayList<DJBitzModel.DJBitzMusicList.MusicRecords>? = null
    var listMusicsPlayerShuffle: ArrayList<DJBitzModel.DJBitzMusicList.MusicRecords>? = null
    var songCurrentPlayer:DJBitzModel.DJBitzMusicList.MusicRecords? = null
    var bottomBar: BottomNavigationView? = null

    var isPlaying = false

    lateinit var bus: Bus


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bottomBar = findViewById(R.id.navigation)

        // Display library fragment on init
        libraryFragment = LibraryFragment.newInstance()
        artistFragment = ArtistFragment.newInstance()
        searchFragment = SearchFragment.newInstance()
        contactFragment = ContactFragment.newInstance()
        currentMixFragment = CurrentMixFragment.newInstance()

        supportFragmentManager
            .beginTransaction()
            .add(R.id.frame_currentmix, currentMixFragment, "CurrentMixFragment")
            .addToBackStack("CurrentMixFragment")
            .commit()

        displayFragment(libraryFragment, "LibraryFragment")

        bottomBar?.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

        beat_play = findViewById<ImageButton>(R.id.beat_play)
        beat_loading = findViewById<ProgressBar>(R.id.beat_loading)
        beat_seek_bar = findViewById<SeekBar>(R.id.beat_seek_bar)

        // set style, just once
        beat_seek_bar!!.progress = 0
        beat_seek_bar!!.max = 0

        beat_seek_bar!!.setPadding(0,0,0,0)


        frame.setPadding(0,0,0, initPadding.toPx())

        beat_play?.setOnClickListener(View.OnClickListener {

            if (isPlaying) {
                pauseMedia()
                beat_play?.setImageResource(R.mipmap.ic_play)
            } else {
                playMedia()
                beat_play?.setImageResource(R.mipmap.ic_pause)
            }
        })

        beat_close_player.setOnClickListener(View.OnClickListener {
            if (isPlaying) {

                sendIntent(FINISH)
            }
            listMusicsPlayer = null
            rlPlayer.visibility = View.GONE

            frame.setPadding(0,0,0, initPadding.toPx())
        })

        beat_seek_bar?.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                // Write code to perform some action when progress is changed.
                if (fromUser) {

                    var realProgress = (progress - beat_seek_bar_offset*beat_seek_bar_length/100) //*seekBar.max/beat_seek_bar_length/100
                    Intent(this@MainActivity, MusicService::class.java).apply {
                        putExtra(SONG_POS, realProgress)
                        startService(this)
                    }

                    if (beat_loading?.visibility != View.VISIBLE) {
                        beat_loading?.visibility = View.VISIBLE
                        beat_play?.visibility = View.GONE
                    }
                }
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {
                // Write code to perform some action when touch is started.

            }

            override fun onStopTrackingTouch(seekBar: SeekBar) {
                // Write code to perform some action when touch is stopped.

            }
        })

        if (isPlaying) {
            rlPlayer.visibility = View.VISIBLE

            frame.setPadding(0,0,0, playingPadding.toPx())
            try {

                if (frame_currentmix.visibility == View.VISIBLE) {
                    rlPlayer.visibility = View.GONE
                    frame.setPadding(0,0,0, initPadding.toPx())
                }
            } catch (e: Exception) {

            }
        }

        rlPlayer.setOnClickListener(View.OnClickListener {
            // restore current mix screen
            try {
                if (frame_currentmix.visibility != View.VISIBLE) {
                    getMusicInfo(songCurrentPlayer!!.id)
                    //addCurrentMixFragment("CurrentMixFragment")
                    addCurrentMixFragment("CurrentMixFragment", false)
                    bShowCurrentMixFragment = false
                }


            }catch (e: Exception){

            }
        })

        // [START subscribe_topics]
        FirebaseMessaging.getInstance().subscribeToTopic("new-mix")
            .addOnCompleteListener { task ->
            }
        // [END subscribe_topics]


        bus = BusProvider.instance
        bus.register(this)


    }

    override fun onDestroy() {
        super.onDestroy()

        bus.unregister(this)

        sendIntent(FINISH)

    }

    fun showHideCurrentMix(index: Int){
        // Hide current mix
        if (currentMixTab >= 0) {
            if (currentMixTab != index) {
                frame_currentmix.visibility = View.GONE
                rlPlayer.visibility = View.VISIBLE
            } else {
                frame_currentmix.visibility = View.VISIBLE
                rlPlayer.visibility = View.GONE
            }
        }
    }

    fun displayFragment(fragment: Fragment, tag: String) {
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.frame, fragment, tag)
            .commit()
    }

    fun hideCurrentMixFragment() {
        try {

            if (isPlaying) {
                rlPlayer.visibility = View.VISIBLE
                frame.setPadding(0,0,0, playingPadding.toPx())
            } else {
                rlPlayer.visibility = View.GONE
                frame.setPadding(0,0,0, initPadding.toPx())
            }

            frame_currentmix.visibility = View.GONE
            currentMixTab = -1

        } catch (e: java.lang.Exception) {

        }
    }

    private fun addCurrentMixFragment(tag: String, initPlayer: Boolean = true) {
        frame_currentmix.visibility = View.VISIBLE
        if (initPlayer) {
            currentMixFragment.initPlayer()
        }

        if (libraryFragment.isVisible){
            currentMixTab = 0
        }
        else if (artistFragment.isVisible){
            currentMixTab = 1
        }
        else if (searchFragment.isVisible){
            currentMixTab = 2
        }
        else if (contactFragment.isVisible){
            currentMixTab = 3
        }
        else{
            currentMixTab = -1
        }
    }

    override fun onResume() {
        super.onResume()
        if (listMusicsPlayer != null && listMusicsPlayer?.size!! > 0) {
            rlPlayer.visibility = View.VISIBLE
            frame.setPadding(0,0,0, playingPadding.toPx())
        }
    }

    var exit = false
    override fun onBackPressed() {

        if (exit) {
//            if (isPlaying) {
//                mediaPlayer.stop()
//            }
//            finish() // finish activity
            super.onBackPressed()
        } else {
            Toast.makeText(
                this, "Press Back again to Exit.",
                Toast.LENGTH_SHORT
            ).show()
            exit = true
            Handler().postDelayed({ exit = false }, (3 * 1000).toLong())

        }
    }

    fun nextMusic() {
        if (listMusicsPlayer != null) {
            sendIntent(NEXT)
        }
    }

    fun prevMusic() {
        if (listMusicsPlayer != null) {
            sendIntent(PREVIOUS)
        }
    }

    fun showError(error: String?) {
        try {

            showHideProgressbar(false)
            var message = error ?: getString(R.string.messager_load_server_error)
            AlertDialog.Builder(this)
                .setTitle(R.string.title_load_server_error)
                .setMessage(message)
                .setPositiveButton(
                    android.R.string.ok
                ) { dialog, which -> dialog.dismiss() }

                .show()
        } catch (ex: java.lang.Exception) {

        }
    }

    fun showResult(result: DJBitzModel.DJBitzMusicInfo.Result){

        if (result != null && result.status.toLowerCase().equals("success")) {

            currentMixFragment.textNameDJ?.text = songCurrentPlayer?.DJ
            currentMixFragment.textNameMusic?.text = songCurrentPlayer?.name
            currentMixFragment.setImageAvatar(songCurrentPlayer!!.DJ)
            currentMixFragment.beatSeekBar?.max = getMaxDuration(songCurrentPlayer?.duration)
            beat_seek_bar?.max = getMaxDuration(songCurrentPlayer?.duration)

            textNameMusicPlayer.text = songCurrentPlayer?.name

            if (!songCurrentPlayer?.likes.isNullOrEmpty()) {
                currentMixFragment.textCountLikes?.text = songCurrentPlayer?.likes
            }
            else{
                currentMixFragment.textCountLikes?.text = "0"
            }

            if (!songCurrentPlayer?.playCounts.isNullOrEmpty()) {
                currentMixFragment.textCountListened?.text = songCurrentPlayer?.playCounts
            }
            else{
                currentMixFragment.textCountListened?.text = "0"
            }

            if (result.comments != null) {
                currentMixFragment.setCommentAdapter(result.comments)
            }
        }
    }

    fun getMaxDuration(strDuration: String?): Int{
        var max = 0
        val split = strDuration?.split(":")

        max += split!!.get(2).toInt()
        max += (split!!.get(1).toInt() * 60).toInt()
        max += (split!!.get(0).toInt()  * 60 * 60).toInt()

        return max
    }


    fun playMedia(path: String) {

        if (path.equals(Constants.BASE_URL)){
            nextMusic()
        }
        else {
            try {
                if (bShowCurrentMixFragment) {

                    addCurrentMixFragment("CurrentMixFragment")
                    bShowCurrentMixFragment = false
                }


            }catch (e: Exception){

            }

            runOnUiThread({
                beat_play?.visibility = View.GONE
                beat_loading?.visibility = View.VISIBLE

                try {

                    if (frame_currentmix.visibility == View.VISIBLE) {

                        currentMixFragment.beatPlay?.visibility = View.GONE
                        currentMixFragment.beatLoading?.visibility = View.VISIBLE
                    }
                } catch (e: Exception) {

                }

            })

            //val fileUri = Uri.parse(path)
//            val sharedPref = getPreferences(Context.MODE_PRIVATE)
//            var bShuffle = sharedPref.getBoolean("booleanShuffle", false)
//            listMusicsPlayerNow = ArrayList<DJBitzModel.DJBitzMusicList.MusicRecords>()

//            if (bShuffle) {
//                listMusicsPlayerNow!!.addAll(listMusicsPlayerShuffle!!)
//            }
//            else {
//                listMusicsPlayerNow!!.addAll(listMusicsPlayer!!)
//            }

//            songCurrentPlayer = listMusicsPlayerNow?.get(currentMusicPlayer)

            songCurrentPlayer = listMusicsPlayer?.get(currentMusicPlayer)

            val idMusic = songCurrentPlayer?.id!!

            getMusicInfo(idMusic)

            runOnUiThread {
                Intent(this, MusicService::class.java).apply {
                    //data = fileUri
                    action = INIT_PATH
                    putExtra(INIT_PATH, listMusicsPlayer)
                    putExtra(INIT_PATH_SHUFFLE, listMusicsPlayerShuffle)
                    putExtra(SONG_POS, currentMusicPlayer)
                    startService(this)

                    //currentMixFragment.beatPlay?.visibility = View.VISIBLE
                    //currentMixFragment.beatLoading?.visibility = View.GONE
                    isPlaying = true
                }
            }

            beat_seek_bar?.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
                override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {

                }

                override fun onStartTrackingTouch(seekBar: SeekBar) {
                }

                override fun onStopTrackingTouch(seekBar: SeekBar) {
                    Intent(this@MainActivity, MusicService::class.java).apply {
                        putExtra(PROGRESS, seekBar.progress)
                        action = SET_PROGRESS
                        startService(this)
                    }
                }
            })

            currentMixFragment.beatSeekBar?.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
                override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {

                }

                override fun onStartTrackingTouch(seekBar: SeekBar) {
                }

                override fun onStopTrackingTouch(seekBar: SeekBar) {
                    Intent(this@MainActivity, MusicService::class.java).apply {
                        putExtra(PROGRESS, seekBar.progress)
                        action = SET_PROGRESS
                        startService(this)
                    }
                }
            })

        }
    }

    fun getMusicInfo(idMusic: String){
        try {
            showHideProgressbar(true)

            val params = java.util.HashMap<String, String>()
            params["uid"] = Constants.userInfo?.userInfo?.id ?: ""
            params["mid"] = idMusic

            DJBitzApiServe.getMusicInfo(params).enqueue(object : Callback<DJBitzModel.DJBitzMusicInfo.Result> {

                override fun onResponse(
                    call: Call<DJBitzModel.DJBitzMusicInfo.Result>,
                    result: Response<DJBitzModel.DJBitzMusicInfo.Result>
                ) {

                    if (result != null && result.body() != null &&  result.body()!!.status.toLowerCase().equals(
                            "success"
                        )
                    ) {

                        showHideProgressbar(false)
                        showResult(result.body()!!)
                    }
                    else {
                        //error
                        showError(null)
                    }

                }

                override fun onFailure(call: Call<DJBitzModel.DJBitzMusicInfo.Result>, t: Throwable) {
                    showError(null)
                }
            })

        }catch (e: Exception){
            e.stackTrace
        }
    }

    fun playMedia() {

        beat_play?.setImageResource(R.mipmap.ic_pause)
        sendIntent(PLAYPAUSE)
    }

    fun pauseMedia() {

        beat_play?.setImageResource(R.mipmap.ic_play)
        sendIntent(PLAYPAUSE)
    }

    fun dpToPx(dp: Int): Int {
        return TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, dp.toFloat(), Resources.getSystem().displayMetrics
        ).toInt()
    }


    fun Int.toPx(): Int = (this * Resources.getSystem().displayMetrics.density).toInt()

    fun View.hideKeyboard() {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(windowToken, 0)
    }

    fun showHideProgressbar(show: Boolean){
        if (show){
            progressIndicator.visibility = View.VISIBLE
        }
        else{
            progressIndicator.visibility = View.GONE
        }
    }



    @Subscribe
    fun progressUpdated(event: Events.ProgressUpdated) {
        val progress = event.progress
        //song_progressbar.progress = progress
        //getSongsAdapter()?.updateSongProgress(progress)
        if (isPlaying) {

            beat_play?.setImageResource(R.mipmap.ic_pause)
            runOnUiThread(Runnable {

                if (progress > 0) {
                    beat_play?.visibility = View.VISIBLE
                    beat_loading?.visibility = View.GONE
                }

                try {


                    if (frame_currentmix.visibility == View.VISIBLE) {
                        currentMixFragment.beatPlay?.setImageResource(R.mipmap.ic_pause)
                        if (progress > 0) {
                            currentMixFragment.beatPlay?.visibility = View.VISIBLE
                            currentMixFragment.beatLoading?.visibility = View.GONE
                        }
                    }
                } catch (e: Exception) {

                }
            })

            //rlPlayer.visibility = View.VISIBLE
            //frame.setPadding(0,0,0, playingPadding.toPx())

            try {

                if (frame_currentmix.visibility == View.VISIBLE) {
                    rlPlayer.visibility = View.GONE
                    frame.setPadding(0,0,0, 50.toPx())
                    // Displaying Total Duration time
                    currentMixFragment.textMaxDuration?.setText("" + currentMixFragment.milliSecondsToTimer(getMaxDuration(songCurrentPlayer?.duration) - progress))
                    // Displaying time completed playing
                    currentMixFragment.textCurrentDuration?.setText("" + currentMixFragment.milliSecondsToTimer(progress))

                    currentMixFragment.beatSeekBar?.progress = (progress)
                }
            } catch (e: Exception) {

            }

            beat_seek_bar_length = getMaxDuration(songCurrentPlayer?.duration)
            beat_seek_bar?.progress = (progress + beat_seek_bar_offset*getMaxDuration(songCurrentPlayer?.duration)/100)
        }

    }

    fun mediaPrepare(){
        runOnUiThread({
            beat_play?.visibility = View.GONE
            beat_loading?.visibility = View.VISIBLE

            try {

                if (frame_currentmix.visibility == View.VISIBLE) {

                    currentMixFragment.beatPlay?.visibility = View.GONE
                    currentMixFragment.beatLoading?.visibility = View.VISIBLE
                }
            } catch (e: Exception) {

            }
        })
    }

    @Subscribe
    fun handlePrepare(event: Events.HandlePrepare) {
        mediaPrepare()
    }

    @Subscribe
    fun songChangedEvent(event: Events.SongChanged) {
        songCurrentPlayer = event.song
        if (songCurrentPlayer!= null)
            getMusicInfo(songCurrentPlayer!!.id)

    }

    @Subscribe
    fun songStateChanged(event: Events.SongStateChanged) {
        isPlaying = event.isPlaying
        runOnUiThread({
            if (isPlaying){

                //beat_play?.visibility = View.VISIBLE
                //beat_loading?.visibility = View.GONE

                try {

                    if (frame_currentmix.visibility == View.VISIBLE) {

                        currentMixFragment.beatPlay?.visibility = View.VISIBLE
                        currentMixFragment.beatLoading?.visibility = View.GONE
                    }
                } catch (e: Exception) {

                }
            }

            beat_play?.setImageDrawable(resources.getDrawable(if (isPlaying) R.mipmap.ic_pause else R.mipmap.ic_play))
            currentMixFragment.beatPlay?.setImageDrawable(resources.getDrawable(if (isPlaying) R.mipmap.ic_pause else R.mipmap.ic_play))
        })
    }
}

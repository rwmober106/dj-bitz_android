package com.bitz

import android.Manifest
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.bitz.retrofit2.DJBitzInterFace
import com.bitz.retrofit2.DJBitzModel
import com.bitz.utils.Constants
import com.bitz.utils.PreferencesHelper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class SplashActivity : AppCompatActivity() {


    val DJBitzApiServe by lazy {
        DJBitzInterFace.create()
    }

    var permission = arrayOf(
        Manifest.permission.ACCESS_COARSE_LOCATION,
        Manifest.permission.ACCESS_FINE_LOCATION,
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.CAMERA,
        Manifest.permission.RECORD_AUDIO,
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.RECEIVE_SMS,
        Manifest.permission.READ_SMS,
        Manifest.permission.SEND_SMS
    )
    val PERMISSION_CODE = 100

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        registerNotification()

        //System.out.println("asnmmsnasamnnmsa="+PrefManager.getString(this,PrefManager.TOKEN))


//        val handler = Handler()
//        handler.postDelayed({
//            val intent = Intent(this, LoginActivity::class.java)
//            startActivity(intent)
//            finish()
//        }, 500)

//        val path = "android.resource://" + packageName + "/" + R.raw.djbitz
//        videoView.setVideoURI(Uri.parse(path))
//        videoView.setOnCompletionListener {
//            val handler = Handler()
//            handler.postDelayed({
//                val intent = Intent(this, MainActivity::class.java)
//                startActivity(intent)
//                finish()
//            }, 500)
//        }
//        videoView.start()

        //ProjectUtils.hasPermissionInManifest(this@SplashActivity, this@SplashActivity.PERMISSION_CODE, permission)
        if (ProjectUtils.hasPermissionInManifest(this@SplashActivity, this@SplashActivity.PERMISSION_CODE, permission))
        {
            val bFistOpen = PreferencesHelper.preferencesHelper.isFirstOpenApp(this)
            val handler = Handler()
            handler.postDelayed({
                if (bFistOpen) {
                    val intent = Intent(this, LoginActivity::class.java)
                    startActivity(intent)
                    finish()
                } else {
                    //postLogin()
                    val intent = Intent(this@SplashActivity, MainActivity1::class.java)
                    startActivity(intent)
                    finish()
                }
            },500)
        }
        else
        {
            val bFistOpen = PreferencesHelper.preferencesHelper.isFirstOpenApp(this)
            val handler = Handler()
            handler.postDelayed({
                if (bFistOpen) {
                    val intent = Intent(this, LoginActivity::class.java)
                    startActivity(intent)
                    finish()
                } else {
                    postLogin()
//                    val intent = Intent(this@SplashActivity, MainActivity1::class.java)
//                    startActivity(intent)
//                    finish()
                }
            },3000)
        }


//        if (!Session.getuserId(SplashActivity.this).equals("")) {
//            if (ProjectUtils.isNetworkConnected1(SplashActivity.this, root_layout)) {
//                authenticate();
//            }
//
//        } else {
    }

        fun postLogin(){
        try {
            val params = HashMap<String, String>()
//            params["name"] = editName.text.toString()
            params["email"] = PreferencesHelper.preferencesHelper.getEmailLogin(this)
            params["password"] =  PreferencesHelper.preferencesHelper.getPasswordLogin(this)
            params["FCM_token"] = PrefManager.getString(this,PrefManager.TOKEN)

            DJBitzApiServe.postLogin(params).enqueue(object : Callback<DJBitzModel.DJBitzUserInfo.Result> {

                override fun onResponse(
                    call: Call<DJBitzModel.DJBitzUserInfo.Result>,
                    response: Response<DJBitzModel.DJBitzUserInfo.Result>
                ) {

                    if (response.isSuccessful && response.body() != null && response.body()!!.status.toLowerCase().equals("success")){
                        try {
                            System.out.println("asnmmsnasamnnmsa="+response.body())
                            Constants.userInfo = response.body()

                            //PreferencesHelper.preferencesHelper.setFirstOpenApp(this@SplashActivity, false)
                            //PreferencesHelper.preferencesHelper.setEmailLogin(this@SplashActivity, editEmail.text.toString())
                            //PreferencesHelper.preferencesHelper.setPasswordLogin(this@SplashActivity, editPassword.text.toString())
                            //Constants.userInfo = response.body()

//                            PrefManager.setString(this@SplashActivity, PrefManager.ID, response.body()!!.userInfo.id)
//                            PrefManager.setString(this@SplashActivity, PrefManager.OTHER_ID, response.body()!!.userInfo.id)
//                            PrefManager.setString(this@SplashActivity, PrefManager.NMAE, response.body()!!.userInfo.username)
//                            PrefManager.setString(this@SplashActivity, PrefManager.OTHER_NAME, response.body()!!.userInfo.username)
//                            PrefManager.setString(this@SplashActivity, PrefManager.USERTYPE, response.body()!!.userInfo.isDjs)

                            val intent = Intent(this@SplashActivity, MainActivity1::class.java)
                            startActivity(intent)
                            finish()

                            /*val intent = Intent(this@SplashActivity, MainActivity::class.java)
                            startActivity(intent)
                            finish()*/
                        }catch (ex: Exception){
                            ex.printStackTrace()
                            showError(null)
                        }

                    }
                    else{
                        //error
                        if (response.body()!= null) {
                            showError(response.body()!!.msg)
                        }else{
                            showError(null)
                        }
                    }
                }

                override fun onFailure(call: Call<DJBitzModel.DJBitzUserInfo.Result>, t: Throwable) {
                    showError(null)
                }
            })
        }catch (e: Exception){
            e.stackTrace
        }
    }

    fun showError(error: String?){
        try {
            val intent = Intent(this@SplashActivity, LoginActivity::class.java)
            startActivity(intent)
            finish()
        }catch (ex: java.lang.Exception){

        }
    }

    private fun registerNotification() {
        System.out.println("samnasmasmnnmas=Gourav")
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Create channel to show notifications.
            val channelId = getString(R.string.default_notification_channel_id)
            val channelName = getString(R.string.default_notification_channel_name)
            val notificationManager = getSystemService(NotificationManager::class.java)
            notificationManager.createNotificationChannel(NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_LOW))
        }
    }
}

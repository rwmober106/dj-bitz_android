package com.bitz

import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Intent
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import com.bitz.live_video.Constants
import com.bitz.live_video.activities.LiveAudienceActivity
import com.bitz.retrofit2.DJBitzInterFace
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.row_live_user.view.*
import okhttp3.OkHttpClient
import org.json.JSONObject
import java.util.concurrent.TimeUnit


class UserListAdapter(private val songs: ArrayList<UserList>, private val viewStyle: Int? = 0, val mContext: MainActivity1) : RecyclerView.Adapter<UserListAdapter.SongRowHolder>() {
    var onItemClick: (((UserList), adapterPosition: Int) -> Unit)? = null

    val DJBitzApiServe by lazy {
        DJBitzInterFace.create()
    }

    override fun getItemCount() = songs.size

    override fun onBindViewHolder(holder: UserListAdapter.SongRowHolder, position: Int) {
        val song = songs[position]
        holder.bind(song)
    }

    fun showError(error: String?) {
        try {
            if (mContext != null) {
                mContext.showHideProgressbar(false)
            }

            var message = error ?: mContext.getString(R.string.messager_load_server_error)
            AlertDialog.Builder(mContext)
                .setTitle(R.string.title_load_server_error)
                .setMessage(message)
                .setPositiveButton(
                    android.R.string.ok
                ) { dialog, which -> dialog.dismiss() }

                .show()
        } catch (ex: java.lang.Exception) {

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserListAdapter.SongRowHolder {

        var inflatedView = parent.inflate(R.layout.row_live_user, false)

        return SongRowHolder(inflatedView)
    }

    inner class SongRowHolder(v: View) : RecyclerView.ViewHolder(v), View.OnClickListener {
        private var view: View = v
        private var song: UserList? = null

        init {
            v.setOnClickListener(this)
        }

        override fun onClick(v: View) {
            onItemClick?.invoke(songs[adapterPosition], adapterPosition)

        }


        fun bind(songNow: UserList) {
            if (viewStyle == 0) {
                song = songNow
                view.textViewTitle.text = songNow.username
                view.textViewEmail.text = songNow.email
                if (songNow.profile_avatar != null && !songNow.profile_avatar.isEmpty()) {
                    var url = com.bitz.utils.Constants.BASE_URL + songNow.profile_avatar.replace("\\","")

                    Picasso.get()
                        .load(url)
                        .placeholder(R.mipmap.ic_avatar)
                        .into(view.ivProfileAvatar)
                }

                view.btjoin.setOnClickListener {

                    System.out.println("asnmmsnasamnnmsa_U_id="+songNow.userid)
                    System.out.println("asnmmsnasamnnmsa_U_Username="+songNow.username)
                    System.out.println("asnmmsnasamnnmsa_U_channel="+songNow.getChannelName())

                    //PrefManager.setString(context, PrefManager.Connection_USERID, userId);
                    PrefManager.setString(mContext, PrefManager.Connection_USERID, "244")

                    PrefManager.setString(mContext, PrefManager.NMAE, songNow.username)
                    PrefManager.setString(mContext, PrefManager.CHANNEL, songNow.getChannelName())
                    PrefManager.setString(mContext, PrefManager.TOKEN, songNow.token)

                    val progressDialog = ProgressDialog(mContext)
                    progressDialog.setMessage("Please Wait...")
                    progressDialog.setCancelable(false)
                    makePost(progressDialog)
                }
            }
        }
    }

    private fun makePost(progressDialog: ProgressDialog) {
        progressDialog.show()
        AndroidNetworking.upload(mContext.getString(R.string.LIVE_URL)+"GenerateToken/generate_token?")
            .setOkHttpClient(OkHttpClient.Builder().connectTimeout(60, TimeUnit.SECONDS).readTimeout(60, TimeUnit.SECONDS).writeTimeout(60, TimeUnit.SECONDS).build())
            .addMultipartParameter("userId", PrefManager.getString(mContext, PrefManager.ID))
            .addMultipartParameter("userName", PrefManager.getString(mContext, PrefManager.NMAE))
            .addMultipartParameter("channelName", PrefManager.getString(mContext, PrefManager.CHANNEL))
            .setPriority(Priority.MEDIUM)
            .build()
            .getAsJSONObject(object : JSONObjectRequestListener {
                override fun onResponse(obj: JSONObject) {
                    val status = obj.optString("status")
                    progressDialog.dismiss()
                    Log.e("Response--", obj.toString())
                    if (status == "1") {
                        val data_object = obj.optJSONObject("AllData")
                        val userId = data_object.optString("userId").trim { it <= ' ' }
                        val channelName = data_object.optString("channelName").trim { it <= ' ' }
                        val TokenWithUid = data_object.optString("TokenWithUid").trim { it <= ' ' }
                        val TokenWithUserAccount = data_object.optString("TokenWithUserAccount").trim { it <= ' ' }
                        println("smamsmsamnasmasas_TokenWithUid=$TokenWithUid")
                        println("smamsmsamnasmasas_channelName=$channelName")
                        //
                        PrefManager.setString(mContext, PrefManager.Connection_USERID, userId)
                        PrefManager.setString(mContext, PrefManager.TOKEN, TokenWithUid)
                        PrefManager.setString(mContext, PrefManager.CHANNEL, channelName)
                        val intent = Intent(mContext.getIntent())
                        intent.putExtra(Constants.KEY_CLIENT_ROLE, "2")
                        intent.setClass(mContext, LiveAudienceActivity::class.java)
                        mContext.startActivity(intent)
                        mContext.finish()

                        //intent.setClass(AudienceActivity.this, LiveBoardCastActivity.class);
                        //mContext.startActivityForResult(intent, 2);

                    } else {
                        Toast.makeText(mContext, "please try again!", Toast.LENGTH_SHORT).show()
                    }
                }

                override fun onError(anError: ANError) {
                    progressDialog.dismiss()
                    try {
                    } catch (e: Exception) {
                        e.printStackTrace()
                        //ProjectUtils.showLog("MAKEPOST", "Exception = " + e.getMessage());
                        Toast.makeText(
                            mContext,
                            "Something Went Wrong",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
            })
    }
}

package com.bitz

import android.app.AlertDialog
import android.content.Intent
import android.graphics.Paint
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.bitz.retrofit2.DJBitzInterFace
import com.bitz.retrofit2.DJBitzModel
import kotlinx.android.synthetic.main.activity_forgot_password.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class ForgotPasswordActivity : AppCompatActivity() {

    val DJBitzApiServe by lazy {
        DJBitzInterFace.create()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_password)

        btnReset.setOnClickListener {
            postResetPassword()
        }

        textViewTitle.setPaintFlags(textViewTitle.getPaintFlags() or Paint.UNDERLINE_TEXT_FLAG)

        login.setPaintFlags(login.getPaintFlags() or Paint.UNDERLINE_TEXT_FLAG)

        login.setOnClickListener {
            finish()
        }

    }

    fun postResetPassword(){
        try {
            if (editPassword.text.toString().equals(editConfirmPassword.text.toString())) {
                val params = HashMap<String, String>()
//            params["name"] = editName.text.toString()
                params["registered_email"] = editEmail.text.toString()
                params["password"] = editPassword.text.toString()

                showHideProgressbar(true)
                DJBitzApiServe.postResetPassword(params).enqueue(object : Callback<DJBitzModel.ResponseBody.Result> {

                    override fun onResponse(
                        call: Call<DJBitzModel.ResponseBody.Result>,
                        response: Response<DJBitzModel.ResponseBody.Result>
                    ) {
                        if (response.isSuccessful && response.body() != null && response.body()!!.status.toLowerCase().equals("success")){

                            showHideProgressbar(false)
                            finish()
                        } else {
                            //error
                            if (response.body()!= null) {
                                showError(response.body()!!.msg)
                            }else{
                                showError(null)
                            }
                        }
                    }

                    override fun onFailure(call: Call<DJBitzModel.ResponseBody.Result>, t: Throwable) {
                        showError(null)
                    }
                })
            }
        }catch (e: Exception){
            e.stackTrace
        }
    }

    fun showError(error: String?){
        try {
            showHideProgressbar(false)
            var message = error?: getString(R.string.messager_load_server_error)
            AlertDialog.Builder(this)
                .setTitle(R.string.title_load_server_error)
                .setMessage(message)
                .setPositiveButton(
                    android.R.string.ok
                ) { dialog, which -> dialog.dismiss() }

                .show()
        }catch (ex: java.lang.Exception){

        }
    }

    fun showHideProgressbar(show: Boolean){
        if (show){
            progressIndicator.visibility = View.VISIBLE
        }
        else{
            progressIndicator.visibility = View.GONE
        }
    }
}

package com.bitz;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.ExifInterface;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.format.DateUtils;
import android.text.format.Time;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.OvershootInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.material.snackbar.Snackbar;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.sql.Timestamp;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ProjectUtils {
    public static final String TAG = "ProjectUtility";
    private static final String VERSION_UNAVAILABLE = "N/A";
    private static AlertDialog dialog;
    private static Toast toast;
    private static ProgressDialog mProgressDialog;

    public static int getScreenWidth() {
        return Resources.getSystem().getDisplayMetrics().widthPixels;
    }

    public static int getScreenHeight() {
        return Resources.getSystem().getDisplayMetrics().heightPixels;
    }

    public static int dp2px(Context context, float dpValue) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dpValue, context.getResources().getDisplayMetrics());
    }

    public static int sp2px(Context context, float spValue) {
        return (int) (spValue * context.getResources().getDisplayMetrics().scaledDensity + 0.5f);
    }

    public static int px2sp(Context context, float pxValue) {
        return (int) (pxValue / context.getResources().getDisplayMetrics().scaledDensity + 0.5f);
    }

    //For Changing Status Bar Color if Device is above 5.0(Lollipop)
    public static void changeStatusBarColor(Activity activity) {
        Window window = activity.getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(activity.getResources().getColor(R.color.colorPrimary));
        }
    }

    public static void changeStatusBarColorNew(Activity activity, int color) {
        Window window = activity.getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(activity.getResources().getColor(color));
        }
    }

    //For Progress Dialog
    public static ProgressDialog getProgressDialog(Context context) {
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please Wait...");
        return progressDialog;
    }

    //For Long Period Toast Message
    public static void showLong(Context context, String message) {
        if (message == null) {
            return;
        }
        if (toast == null && context != null) {
            toast = Toast.makeText(context, message, Toast.LENGTH_LONG);
        }
        if (toast != null) {
            toast.setText(message);
            toast.show();
        }
    }

    //Here Show ProgressDialog
    public static ProgressDialog showProgressDialog(Context context, String title, String message, boolean isCancelable) {
        mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setTitle(title);
        mProgressDialog.setMessage(message);
        mProgressDialog.show();
        mProgressDialog.setCancelable(isCancelable);
        return mProgressDialog;
    }

    // Static method to pause the progress dialog.
    public static void pauseProgressDialog() {
        try {
            if (mProgressDialog != null) {
                mProgressDialog.cancel();
                mProgressDialog.dismiss();
                mProgressDialog = null;
            }
        } catch (IllegalArgumentException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Static method to cancel the Dialog.
     */
    public static void cancelDialog() {
        try {
            if (dialog != null) {
                dialog.cancel();
                dialog.dismiss();
                dialog = null;
            }
        } catch (IllegalArgumentException ex) {
            ex.printStackTrace();
        }
    }

    public static void hideDialog() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
            dialog.cancel();
            dialog = null;
        }
    }

    public static AlertDialog showCustomtDialog(Context context,
                                                String title, String msg, String[] btnText, int layout_id,
                                                DialogInterface.OnClickListener listener) {
        if (listener == null)
            listener = new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    paramDialogInterface.dismiss();
                }
            };

        LayoutInflater factory = LayoutInflater.from(context);
        final View textEntryView = factory.inflate(layout_id,
                null);
        Builder builder = new Builder(context);
        builder.setTitle(title);
        // builder.setResponseMessage(msg);
        // builder.setView(mEmail_forgot);

        builder.setPositiveButton(btnText[0], listener);
        if (btnText.length != 1) {
            builder.setNegativeButton(btnText[1], listener);
        }
        dialog = builder.create();
        dialog.setCancelable(false);
        dialog.setView(textEntryView, 10, 10, 10, 10);
        dialog.show();
        return dialog;

    }

    public static boolean isEmailValid(String email) {
        String expression = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        if (matcher.matches()) {
            return true;
        } else if (email.equals("")) {
            return false;
        }
        return false;
    }

    public static boolean isPhoneNumberValid(String number) {
        String regexStr = "^((0)|(91)|(00)|[1-9]){1}[0-9]{3,14}$";

        return number.length() >= 8 && number.length() <= 12 && number.matches(regexStr) != false;
    }

    public static boolean isPasswordValid(String number) {
        String regexStr = " (?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{8,20})$";

        return number.length() > 5;
    }

    public static boolean isEditTextFilled(EditText text) {
        return text.getText() != null && text.getText().toString().trim().length() > 0;
    }

    //Pan card format
    public static boolean panCardFormat(EditText text) {
        String value = text.getText().toString().trim();

        Pattern pattern = Pattern.compile("[A-Z]{5}[0-9]{4}[A-Z]{1}");

        Matcher matcher = pattern.matcher(value);

        // Check if pattern matches
        if (matcher.matches()) {
            Log.e("Matching", "Yes");

            return true;
        } else {
            Log.e("Matching", "No");

            return false;
        }
    }

    public static long currentTimeInMillis() {
        Time time = new Time();
        time.setToNow();
        return time.toMillis(false);
    }



    public static boolean isNetworkConnected1(Context mContext, View view) {
        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        android.net.NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        android.net.NetworkInfo data = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        if ((wifi != null & data != null) && (wifi.isConnected() | data.isConnected())) {
            return true;
        } else {
            Snackbar snackbar = Snackbar.make(view, "No Internet Connection", Snackbar.LENGTH_INDEFINITE);

            // Changing action button text color
            View sbView = snackbar.getView();
            TextView textView = sbView.findViewById(R.id.snackbar_text);
            textView.setTextColor(Color.WHITE);
            snackbar.show();

            return false;
        }
    }

    public static boolean isMyServiceRunning(Class<?> serviceClass, Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public static int getAppVersion(Context ctx) {
        try {
            PackageInfo packageInfo = ctx.getPackageManager().getPackageInfo(ctx.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    public static float convertPixelsToDp(float px, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return dp;
    }

    public static float getDpi(Activity activity) {
        float dp = 0;
        DisplayMetrics metrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        if (metrics.density == 3.0) {
            dp = 1;
        }
        return dp;
    }

    public static void putBitmapInDiskCache(int blobId, Bitmap avatar, Context mContext) {
        // Create a path pointing to the system-recommended cache dir for the app, with sub-dir named
        File cacheDir = new File(mContext.getCacheDir(), "thumbnails-ICares");
        if (!cacheDir.exists())
            cacheDir.mkdir();
        // Create a path in that dir for a file, named by the default hash of the url

        File cacheFile = new File(cacheDir, "" + blobId);
        try {
            // Create a file at the file path, and open it for writing obtaining the output stream
            cacheFile.createNewFile();
            FileOutputStream fos = new FileOutputStream(cacheFile);
            // Write the bitmap to the output stream (and thus the file) in PNG format (lossless compression)
            avatar.compress(Bitmap.CompressFormat.PNG, 100, fos);
            // Flush and close the output stream
            fos.flush();
            fos.close();
            avatar.recycle();
        } catch (Exception e) {
            // Log anything that might go wrong with IO to file
            Log.e("IMAGE CACHE", "Error when saving image to cache. ", e);
        }
    }

    public static Bitmap getBitmapFromDiskCache(int blobId, Context mContext) {
        // Create a path pointing to the system-recommended cache dir for the app, with sub-dir named
        Bitmap avatar = null;
        File cacheDir = new File(mContext.getCacheDir(), "thumbnails-ICares");
        // Create a path in that dir for a file, named by the default hash of the url

        File cacheFile = new File(cacheDir, "" + blobId);
        try {
            if (cacheFile.exists()) {
                FileInputStream fis = new FileInputStream(cacheFile);
                // Read a bitmap from the file (which presumable contains bitmap in PNG format, since
                // that's how files are created)
                avatar = BitmapFactory.decodeStream(fis);
                // Write the bitmap to the output stream (and thus the file) in PNG format (lossless compression)
            }
            // Create a file at the file path, and open it for writing obtaining the output stream
            // Flush and close the output stream
        } catch (Exception e) {
            // Log anything that might go wrong with IO to file
            Log.e("IMAGE CACHE", "Error when saving image to cache. ", e);
        }
        return avatar;
    }

    public static void saveImage(Bitmap finalBitmap, int fileName) {
        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/Faashio/avatar");
        myDir.mkdirs();
        Random generator = new Random();
        int n = 10000;
        n = generator.nextInt(n);
        String fname = fileName + ".jpg";
        File file = new File(myDir, fname);
        if (file.exists()) file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Bitmap getBitmapFromExternalStorage(int fileName) {
        Bitmap bitmap = null;
        String root = Environment.getExternalStorageDirectory().toString();
        File f = new File(root + "/ICares/avatar/" + fileName + ".jpg");
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        try {
            bitmap = BitmapFactory.decodeStream(new FileInputStream(f), null, options);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return bitmap;
    }

    public static String getImagePathExternalStorage(String fileName) {
        String bitmapPath = null;
        String root = Environment.getExternalStorageDirectory().toString();
        File f = new File(root + "/ICares/avatar/" + fileName + ".jpg");
        String loadURL = "file://" + Environment.getExternalStorageDirectory() + "/ICares/avatar/" + fileName;
        bitmapPath = f.getAbsolutePath();
        return loadURL;
    }

    public static String getInitials(String str) {
        String userInitials = "";
        if (str.length() > 1) {
            String[] array = str.split(" ");
            if (array.length == 1) {
                String firstLatter = String.valueOf(array[0].charAt(0)).toUpperCase();
                String secLatter = String.valueOf(array[0].charAt(1)).toUpperCase();
                userInitials = firstLatter + secLatter;
            } else if (array.length == 2) {
                String firstLatter = String.valueOf(array[0].charAt(0)).toUpperCase();
                String secLatter = String.valueOf(array[1].charAt(0)).toUpperCase();
                userInitials = firstLatter + secLatter;
            }
        } else if (str.length() == 1) {
            userInitials = str;
        }
        return userInitials;
    }

    public static int getCameraPhotoOrientation(Context context, Uri imageUri, String imagePath) {
        int rotate = 0;
        try {
            context.getContentResolver().notifyChange(imageUri, null);
            File imageFile = new File(imagePath);
            ExifInterface exif = new ExifInterface(
                    imageFile.getAbsolutePath());
            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;
            }

            Log.v(TAG, "Exif orientation: " + orientation);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rotate;
    }

    public static void setBadge(Context context, int count) {
        String launcherClassName = getLauncherClassName(context);
        if (launcherClassName == null) {
            return;
        }
        Intent intent = new Intent("android.intent.action.BADGE_COUNT_UPDATE");
        intent.putExtra("badge_count", count);
        intent.putExtra("badge_count_package_name", context.getPackageName());
        intent.putExtra("badge_count_class_name", launcherClassName);
        context.sendBroadcast(intent);
    }

    public static String getLauncherClassName(Context context) {
        PackageManager pm = context.getPackageManager();

        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);

        List<ResolveInfo> resolveInfos = pm.queryIntentActivities(intent, 0);
        for (ResolveInfo resolveInfo : resolveInfos) {
            String pkgName = resolveInfo.activityInfo.applicationInfo.packageName;
            if (pkgName.equalsIgnoreCase(context.getPackageName())) {
                String className = resolveInfo.activityInfo.name;
                return className;
            }
        }
        return null;
    }

    public static void soundPlayer(Context mContext, int resourceId) {
        MediaPlayer player = MediaPlayer.create(mContext, resourceId);
        if (player.isPlaying()) {
            player.stop();
        } else {
            player.start();
        }
    }

    public static boolean hasFroyo() {
        // Can use static final constants like FROYO, declared in later versions
        // of the OS since they are inlined at compile time. This is guaranteed behavior.
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO;
    }

    public static boolean hasGingerbread() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD;
    }

    public static boolean hasHoneycomb() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB;
    }

    public static boolean hasHoneycombMR1() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1;
    }

    public static boolean hasJellyBean() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN;
    }

    public static boolean hasICS() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH;
    }

    public static boolean hasKitKat() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
    }

    public static String getVersionName(Context context) {
        // Get app version
        PackageManager pm = context.getPackageManager();
        String packageName = context.getPackageName();
        String versionName;
        try {
            PackageInfo info = pm.getPackageInfo(packageName, 0);
            versionName = info.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            versionName = VERSION_UNAVAILABLE;
        }
        return versionName;
    }

    public static String calculateAge(String strDate) {
        int years = 0;
        int months = 0;
        int days = 0;

        try {
            long timeInMillis = Long.parseLong(strDate);
            Date birthDate = new Date(timeInMillis);

            //create calendar object for birth day
            Calendar birthDay = Calendar.getInstance();
            birthDay.setTimeInMillis(birthDate.getTime());
            //create calendar object for current day
            long currentTime = System.currentTimeMillis();
            Calendar now = Calendar.getInstance();
            now.setTimeInMillis(currentTime);
            //Get difference between years
            years = now.get(Calendar.YEAR) - birthDay.get(Calendar.YEAR);
            int currMonth = now.get(Calendar.MONTH) + 1;
            int birthMonth = birthDay.get(Calendar.MONTH) + 1;
            //Get difference between months
            months = currMonth - birthMonth;
            //if month difference is in negative then reduce years by one and calculate the number of months.
            if (months < 0) {
                years--;
                months = 12 - birthMonth + currMonth;
                if (now.get(Calendar.DATE) < birthDay.get(Calendar.DATE))
                    months--;
            } else if (months == 0 && now.get(Calendar.DATE) < birthDay.get(Calendar.DATE)) {
                years--;
                months = 11;
            }
            //Calculate the days
            if (now.get(Calendar.DATE) > birthDay.get(Calendar.DATE))
                days = now.get(Calendar.DATE) - birthDay.get(Calendar.DATE);
            else if (now.get(Calendar.DATE) < birthDay.get(Calendar.DATE)) {
                int today = now.get(Calendar.DAY_OF_MONTH);
                now.add(Calendar.MONTH, -1);
                days = now.getActualMaximum(Calendar.DAY_OF_MONTH) - birthDay.get(Calendar.DAY_OF_MONTH) + today;
            } else {
                days = 0;
                if (months == 12) {
                    years++;
                    months = 0;
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
        //Create new Age object
        return years + " years " + months + " months " + days + " days";
    }

    public static boolean hasPermissionInManifest(Activity activity, int requestCode, String[] permissionName) {
        for (String s : permissionName) {
            if (ContextCompat.checkSelfPermission(activity, s) != PackageManager.PERMISSION_GRANTED) {
                // Some permissions are not granted, ask the user.
                ActivityCompat.requestPermissions(activity, permissionName, requestCode);
                return false;
            }
        }

        return true;
    }

    public static Date getDate(int year, int month, int day) {
        Calendar calendar = Calendar.getInstance();
        calendar.clear();
        calendar.set(Calendar.DATE, day);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.YEAR, year);
        return calendar.getTime();
    }

    public static String getTimeStamp(int year, int month, int day) {
        Calendar calendar = Calendar.getInstance();
        calendar.clear();
        calendar.set(Calendar.DATE, day);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.YEAR, year);
        return "" + calendar.getTimeInMillis();
    }

    public static String getFormattedDate(String strDate) {
        String formattedDate = "";
        try {
            long timeInMillis = Long.parseLong(strDate);
            Date date = new Date(timeInMillis);
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

            formattedDate = formatter.format(strDate);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return formattedDate;
    }

    public static void animateLike(final ImageView mLikeImage) {

        mLikeImage.setVisibility(View.VISIBLE);
        mLikeImage.setScaleY(0f);
        mLikeImage.setScaleX(0f);

        AnimatorSet animatorSet = new AnimatorSet();

        ObjectAnimator likeScaleUpYAnimator = ObjectAnimator
                .ofFloat(mLikeImage, ImageView.SCALE_Y, 0f, 1f);
        likeScaleUpYAnimator.setDuration(400);
        likeScaleUpYAnimator.setInterpolator(new OvershootInterpolator());

        ObjectAnimator likeScaleUpXAnimator = ObjectAnimator
                .ofFloat(mLikeImage, ImageView.SCALE_X, 0f, 1f);
        likeScaleUpXAnimator.setDuration(400);
        likeScaleUpXAnimator.setInterpolator(new OvershootInterpolator());

        ObjectAnimator likeScaleDownYAnimator = ObjectAnimator
                .ofFloat(mLikeImage, ImageView.SCALE_Y, 1f, 0f);
        likeScaleDownYAnimator.setDuration(100);

        ObjectAnimator likeScaleDownXAnimator = ObjectAnimator
                .ofFloat(mLikeImage, ImageView.SCALE_X, 1f, 0f);
        likeScaleDownXAnimator.setDuration(100);

        animatorSet.playTogether(likeScaleUpXAnimator,
                likeScaleUpYAnimator);

        animatorSet.play(likeScaleDownXAnimator).
                with(likeScaleDownYAnimator).
                after(800);

        animatorSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mLikeImage.setVisibility(View.GONE);

            }
        });

        animatorSet.start();
    }

    public static void animateLikes(final ImageView mLikeImage) {

        mLikeImage.setVisibility(View.VISIBLE);
        mLikeImage.setScaleY(0f);
        mLikeImage.setScaleX(0f);

        AnimatorSet animatorSet = new AnimatorSet();

        ObjectAnimator likeScaleUpYAnimator = ObjectAnimator
                .ofFloat(mLikeImage, ImageView.SCALE_Y, 0f, 1f);
        likeScaleUpYAnimator.setDuration(400);
        likeScaleUpYAnimator.setInterpolator(new OvershootInterpolator());

        ObjectAnimator likeScaleUpXAnimator = ObjectAnimator
                .ofFloat(mLikeImage, ImageView.SCALE_X, 0f, 1f);
        likeScaleUpXAnimator.setDuration(400);
        likeScaleUpXAnimator.setInterpolator(new OvershootInterpolator());

        ObjectAnimator likeScaleDownYAnimator = ObjectAnimator
                .ofFloat(mLikeImage, ImageView.SCALE_Y, 0f, 1f);
        likeScaleDownYAnimator.setDuration(100);

        ObjectAnimator likeScaleDownXAnimator = ObjectAnimator
                .ofFloat(mLikeImage, ImageView.SCALE_X, 0f, 1f);
        likeScaleDownXAnimator.setDuration(100);

        animatorSet.playTogether(likeScaleUpXAnimator, likeScaleUpYAnimator);


        animatorSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mLikeImage.setVisibility(View.VISIBLE);

            }
        });

        animatorSet.start();
    }

    public static void animateFollow(final RelativeLayout mLikeImage) {

        mLikeImage.setVisibility(View.VISIBLE);
        mLikeImage.setScaleY(0f);
        mLikeImage.setScaleX(0f);

        AnimatorSet animatorSet = new AnimatorSet();

        ObjectAnimator likeScaleUpYAnimator = ObjectAnimator
                .ofFloat(mLikeImage, ImageView.SCALE_Y, 0f, 1f);
        likeScaleUpYAnimator.setDuration(400);
        likeScaleUpYAnimator.setInterpolator(new OvershootInterpolator());

        ObjectAnimator likeScaleUpXAnimator = ObjectAnimator
                .ofFloat(mLikeImage, ImageView.SCALE_X, 0f, 1f);
        likeScaleUpXAnimator.setDuration(400);
        likeScaleUpXAnimator.setInterpolator(new OvershootInterpolator());

        ObjectAnimator likeScaleDownYAnimator = ObjectAnimator
                .ofFloat(mLikeImage, ImageView.SCALE_Y, 0f, 1f);
        likeScaleDownYAnimator.setDuration(100);

        ObjectAnimator likeScaleDownXAnimator = ObjectAnimator
                .ofFloat(mLikeImage, ImageView.SCALE_X, 0f, 1f);
        likeScaleDownXAnimator.setDuration(100);

        animatorSet.playTogether(likeScaleUpXAnimator, likeScaleUpYAnimator);


        animatorSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mLikeImage.setVisibility(View.VISIBLE);

            }
        });

        animatorSet.start();
    }

    public static String getFormattedTime(String strTime) {
        String formattedTime = "";
        try {
            long timeInMillis = Long.parseLong(strTime);
            Date date = new Date(timeInMillis);
            SimpleDateFormat formatter = new SimpleDateFormat("hh:mm");

            formattedTime = formatter.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return formattedTime;
    }

    public static int getIntMonth(String date) {
        String[] dateArr = date.split("/");
        int month = Integer.parseInt(dateArr[1]);

        return month;
    }

    public static String getStrMonth(String strDate) {
        Date date = new Date(Long.parseLong(strDate));

        String month = null;
        try {
            //  date = df.parse(strDate);
            Format formatter = new SimpleDateFormat("MMM yyyy");
            month = formatter.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return month;
    }

    public static String getStringMonth(String strDate) {
        Date date = new Date(Long.parseLong(strDate));

        String month = null;
        try {
            //  date = df.parse(strDate);
            Format formatter = new SimpleDateFormat("MMM");
            month = formatter.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return month;
    }

    public static String getFormatedDate(String strDate) {
        Date date = new Date(Long.parseLong(strDate));
        String formattedDate = null;
        SimpleDateFormat formatter = new SimpleDateFormat("MMM dd, yyyy");
        try {
            formattedDate = formatter.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return formattedDate;
    }

    public static String getConvertedTime(String strTime) {
        String convertedTime = null;

        String[] timeArr = strTime.split(":");
        int hour = Integer.parseInt(timeArr[0]);

        if (hour < 12) {
            convertedTime = strTime + " AM";
        } else {
            convertedTime = strTime + " PM";
        }

        return convertedTime;
    }

    public static ArrayList<Integer> getMonthLst() {
        ArrayList<Integer> months = new ArrayList<>();
        for (int i = 1; i <= 12; i++) {
            months.add(i);
        }
        return months;
    }

    public static String firstDayOfMonth() {
        String formattedDate = "";
        Calendar calendar = Calendar.getInstance();

        while (calendar.get(Calendar.DATE) > 1) {
            calendar.add(Calendar.DATE, -1);
        }

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        try {
            formattedDate = formatter.format(calendar.getTime());
            Log.e("<--First Day of Month :", formattedDate);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return formattedDate;
    }

    public static String lastDayOfMonth() {
        String formattedDate = "";
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        try {
            formattedDate = formatter.format(calendar.getTime());
            Log.e("<--Last Day of Month :", formattedDate);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return formattedDate;
    }

    public static String lastDayOfMonthFormatted() {
        String formattedDate = "";
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));

        SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
        try {
            formattedDate = formatter.format(calendar.getTime());
            Log.e("<--Last Day of Month :", formattedDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return formattedDate;
    }

    public static String firstDayOfWeekFormatted() {
        String formattedDate = "";
        Calendar calendar = Calendar.getInstance();
        while (calendar.get(Calendar.DAY_OF_WEEK) > calendar.getFirstDayOfWeek()) {
            calendar.add(Calendar.DATE, -1);
        }

        SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
        try {
            formattedDate = formatter.format(calendar.getTime());
            Log.e("<--First Day of Week:", formattedDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return formattedDate;
    }

    public static String firstDayOfWeek() {
        String formattedDate = "";
        Calendar calendar = Calendar.getInstance();
        while (calendar.get(Calendar.DAY_OF_WEEK) > calendar.getFirstDayOfWeek()) {
            calendar.add(Calendar.DATE, -1);
        }

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        try {
            formattedDate = formatter.format(calendar.getTime());
            Log.e("<--First Day of Week:", formattedDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return formattedDate;
    }

    public static String lastDayOfWeek() {
        String formattedDate = "";
        Calendar calendar = Calendar.getInstance();
        while (calendar.get(Calendar.DAY_OF_WEEK) > calendar.getFirstDayOfWeek()) {
            calendar.add(Calendar.DATE, -1);
        }
        calendar.add(Calendar.DATE, 6);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        try {
            formattedDate = formatter.format(calendar.getTime());
            Log.e("<--last Day of Week :", formattedDate);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return formattedDate;
    }

    public static String lastDayOfWeekFormatted() {
        String formattedDate = "";
        Calendar calendar = Calendar.getInstance();
        while (calendar.get(Calendar.DAY_OF_WEEK) > calendar.getFirstDayOfWeek()) {
            calendar.add(Calendar.DATE, -1);
        }
        calendar.add(Calendar.DATE, 6);
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
        try {
            formattedDate = formatter.format(calendar.getTime());
            Log.e("<--last Day of Week :", formattedDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return formattedDate;
    }

    public static String convertTimestampToDate(long timestamp) {
        Timestamp tStamp = new Timestamp(timestamp);
        SimpleDateFormat simpleDateFormat;
        if (DateUtils.isToday(timestamp)) {
            simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            return "Today";
        } else {
            simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            return simpleDateFormat.format(tStamp);
        }
    }

    public static String convertTimestampToTime(long timestamp) {
        Timestamp tStamp = new Timestamp(timestamp);
        SimpleDateFormat simpleDateFormat;
        if (DateUtils.isToday(timestamp)) {
            simpleDateFormat = new SimpleDateFormat("hh:mm a");
            return simpleDateFormat.format(tStamp);
        } else {
            simpleDateFormat = new SimpleDateFormat("hh:mm a");
            return simpleDateFormat.format(tStamp);
        }
    }

    public static boolean isCurrentDate(Date date) {
        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date todayDate = new Date(System.currentTimeMillis());
            String todayDateStr = format.format(todayDate);
            Date convertedTodayDate = format.parse(todayDateStr);

            String oldDate = format.format(date);
            Date convertedOldDate = format.parse(oldDate);

            return convertedOldDate.equals(convertedTodayDate);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static void showLog(String tag, String text) {
        Log.e(tag, text);
    }

    public static int dpToPx(int dp, Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        int pixel = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        Log.e("Converted Pixel", "" + pixel);
        return pixel;
    }

    //To check collection empty or not
    public static boolean isEmpty(Collection obj) {
        return obj == null || obj.isEmpty();
    }

    public static String maskString(String strText, int start, int end, char maskChar)
            throws Exception {
        if (strText == null || strText.equals(""))
            return "";

        if (start < 0)
            start = 0;

        if (end > strText.length())
            end = strText.length();

        if (start > end)
            throw new Exception("End index cannot be greater than start index");

        int maskLength = end - start;

        if (maskLength == 0)
            return strText;

        StringBuilder sbMaskString = new StringBuilder(maskLength);

        for (int i = 0; i < maskLength; i++) {
            sbMaskString.append(maskChar);
        }

        return strText.substring(0, start)
                + sbMaskString.toString()
                + strText.substring(start + maskLength);
    }

    public static int dpToPx1(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    //For hide keyboard
    public static void hideKeyboard(Activity activity) {
        View view = activity.findViewById(android.R.id.content);
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static boolean whatsappInstalledOrNot(Context context, String uri) {
        PackageManager pm = context.getPackageManager();
        boolean app_installed = false;
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            app_installed = true;
        } catch (PackageManager.NameNotFoundException e) {
            app_installed = false;
        }
        return app_installed;
    }

    //Share app
    public static void shareApp(Context mContext) {
        final String appPackageName = BuildConfig.APPLICATION_ID;
        final String appName = mContext.getString(R.string.app_name);
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        String shareBodyText = "https://play.google.com/store/apps/details?id=" + appPackageName;
        shareIntent.putExtra(Intent.EXTRA_SUBJECT, appName);
        shareIntent.putExtra(Intent.EXTRA_TEXT, shareBodyText);
        mContext.startActivity(Intent.createChooser(shareIntent, "Share with"));
    }

    //Here enable/disable child views
    public static void setViewAndChildrenEnabled(View view, boolean enabled) {
        view.setEnabled(enabled);
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            for (int i = 0; i < viewGroup.getChildCount(); i++) {
                View child = viewGroup.getChildAt(i);
                setViewAndChildrenEnabled(child, enabled);
            }
        }
    }

    //Aadhar Card Format
    public static class aadharCardFormatWatcher implements TextWatcher {

        private static final char space = '-';

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            // Remove spacing char
            if (s.length() > 0 && (s.length() % 5) == 0) {
                final char c = s.charAt(s.length() - 1);
                if (space == c) {
                    s.delete(s.length() - 1, s.length());
                }
            }
            // Insert char where needed.
            if (s.length() > 0 && (s.length() % 5) == 0) {
                char c = s.charAt(s.length() - 1);
                // Only if its a digit where there should be a space we insert a space
                if (Character.isDigit(c) && TextUtils.split(s.toString(), String.valueOf(space)).length <= 3) {
                    s.insert(s.length() - 1, String.valueOf(space));
                }
            }
        }
    }
}
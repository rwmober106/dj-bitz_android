package com.bitz.live_video;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bitz.PrefManager;
import com.bitz.R;
import com.bitz.live_video.activities.LiveAudienceActivity;
import com.bitz.live_video.activities.LiveBoardCastActivity;

import org.json.JSONObject;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;

import static android.widget.Toast.makeText;

public class AudienceActivity extends AppCompatActivity {
    Button btjoin;
    TextView tvusernmae;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audience);

        tvusernmae=findViewById(R.id.tvusernmae);
        btjoin=findViewById(R.id.btjoin);

        tvusernmae.setText(PrefManager.getString(this, PrefManager.NMAE));

        btjoin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("asnmmsnasamnnmsa_A_id="+PrefManager.getString(AudienceActivity.this, PrefManager.ID));
                System.out.println("asnmmsnasamnnmsa_A_Username="+PrefManager.getString(AudienceActivity.this, PrefManager.NMAE));
                System.out.println("asnmmsnasamnnmsa_A_channel="+PrefManager.getString(AudienceActivity.this, PrefManager.CHANNEL));

                final ProgressDialog progressDialog = new ProgressDialog(AudienceActivity.this);
                progressDialog.setMessage("Please Wait...");
                progressDialog.setCancelable(false);
                makePost(progressDialog);
            }
        });

    }

    private void makePost(final ProgressDialog progressDialog) {

        progressDialog.show();

        AndroidNetworking.upload(getString(R.string.LIVE_URL)+"GenerateToken/generate_token?").setOkHttpClient(new OkHttpClient.Builder().connectTimeout(60, TimeUnit.SECONDS).readTimeout(60, TimeUnit.SECONDS).writeTimeout(60, TimeUnit.SECONDS).build())
                .addMultipartParameter("userId", PrefManager.getString(AudienceActivity.this, PrefManager.ID))
                .addMultipartParameter("userName", PrefManager.getString(AudienceActivity.this, PrefManager.NMAE))
                .addMultipartParameter("channelName", PrefManager.getString(AudienceActivity.this, PrefManager.CHANNEL))
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject obj) {
                        String status = obj.optString("status");
                        progressDialog.dismiss();

                        Log.e("Response--", String.valueOf(obj));

                        if (status.equals("1")) {

                            JSONObject data_object = obj.optJSONObject("AllData");
                            String userId = data_object.optString("userId").trim();
                            String channelName = data_object.optString("channelName").trim();
                            String TokenWithUid = data_object.optString("TokenWithUid").trim();
                            String TokenWithUserAccount = data_object.optString("TokenWithUserAccount").trim();
                            System.out.println("smamsmsamnasmasas_TokenWithUid="+TokenWithUid);
                            System.out.println("smamsmsamnasmasas_channelName="+channelName);
                            //
                            PrefManager.setString(AudienceActivity.this, PrefManager.Connection_USERID, userId);
                            PrefManager.setString(AudienceActivity.this, PrefManager.TOKEN, TokenWithUid);
                            PrefManager.setString(AudienceActivity.this, PrefManager.CHANNEL, channelName);

                            Intent intent = new Intent(getIntent());
                            intent.putExtra(Constants.KEY_CLIENT_ROLE, "2");
                            intent.setClass(AudienceActivity.this, LiveAudienceActivity.class);
                            //intent.setClass(AudienceActivity.this, LiveBoardCastActivity.class);
                            startActivity(intent);
                            finish();

                        }
                        else
                        {
                            makeText(AudienceActivity.this, "please try again!", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        progressDialog.dismiss();
                        try {

                        } catch (Exception e) {
                            e.printStackTrace();
                            //ProjectUtils.showLog("MAKEPOST", "Exception = " + e.getMessage());
                            makeText(AudienceActivity.this, "Something Went Wrong", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

}

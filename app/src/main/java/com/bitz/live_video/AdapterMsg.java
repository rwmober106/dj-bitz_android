package com.bitz.live_video;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.bitz.R;
import com.bitz.utils.Constants;
import com.squareup.picasso.Picasso;

import java.util.List;

public class AdapterMsg extends RecyclerView.Adapter<AdapterMsg.MyViewHolder> {
    List<MsgBean> beanList;
    Activity activity;
    public AdapterMsg(List<MsgBean> beanList, Activity activity)
    {
        this.beanList=beanList;
        this.activity=activity;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        TextView msg, tvusename;
        RelativeLayout container;
        ImageView ivlike;
        ImageButton ivProfileAvatar;
        public MyViewHolder(View userview){
            super(userview);
            msg=(TextView)userview.findViewById(R.id.msg);
            tvusename=(TextView)userview.findViewById(R.id.tvusename);
            container=(RelativeLayout)userview.findViewById(R.id.container);
            ivlike=(ImageView)userview.findViewById(R.id.ivlike);
            ivProfileAvatar=(ImageButton)userview.findViewById(R.id.beat_avatar);
        }
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.row_comment2,parent,false);
        MyViewHolder hold=new MyViewHolder(view);
        return  hold;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position)
    {
        final MsgBean bean = beanList.get(position);

        holder.tvusename.setText(bean.getAudience_name());

        if (!bean.getProfile_avatar().trim().isEmpty()) {
            String url = Constants.BASE_URL + bean.getProfile_avatar().trim().replace("\\","");

            Picasso.get()
                    .load(url)
                    .placeholder(R.mipmap.ic_avatar)
                    .into(holder.ivProfileAvatar);
        }

        System.out.println("gouravkakakakakakkas ADApter="+bean.getMessage()+"=null="+bean.getAudience_name());

        if(!bean.isLike)
        {
            if(bean.getMessage().trim().equalsIgnoreCase("null"))
            {
                holder.msg.setText("");
            }
            else
            {
                holder.ivlike.setVisibility(View.GONE);
                holder.msg.setVisibility(View.VISIBLE);
                holder.msg.setText(bean.getMessage());
            }
        }
        else
        {
            holder.ivlike.setVisibility(View.VISIBLE);
            holder.msg.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount()
    {
        return beanList.size();
    }
}

package com.bitz.live_video.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bitz.MainActivity1;
import com.bitz.PrefManager;
import com.bitz.R;
import com.bitz.live_video.AdapterMsg;
import com.bitz.live_video.Constants;
import com.bitz.live_video.MsgBean;
import com.bitz.live_video.stats.LocalStatsData;
import com.bitz.live_video.stats.RemoteStatsData;
import com.bitz.live_video.stats.StatsData;
import com.bitz.live_video.ui.VideoGridContainer;
import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.Query;
import com.firebase.client.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import io.agora.rtc.IRtcEngineEventHandler;
import io.agora.rtc.video.VideoEncoderConfiguration;
import tyrantgit.widget.HeartLayout;

public class LiveAudienceActivity extends RtcBaseActivity {
    private static final String TAG = LiveAudienceActivity.class.getSimpleName();
    private VideoGridContainer mVideoGridContainer;
    private ImageView mMuteAudioBtn;
    private ImageView mMuteVideoBtn;
    private VideoEncoderConfiguration.VideoDimensions mVideoDimension;
    EditText etcomment;
    ImageView ivsend, ivheart;
    LinearLayout llcomments;
    Firebase ref_comment, ref_live_count, ref_time, ref_like, ref_broadcast_live;
    Query mQuery;
    ScrollView scrollView;
    long count;
    ImageView ivcloss;
    TextView tvusernmae, tvminute, tvview;
    private Timer mTimer = new Timer();
    private HeartLayout mHeartLayout;
    ArrayList<String> views = new ArrayList<>();
    ArrayList<String> view_remove = new ArrayList<>();
    List<MsgBean> beanList = new ArrayList<>();
    RecyclerView rv_user;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_live_audience_room);

        tvview = findViewById(R.id.tvview);
        tvusernmae = findViewById(R.id.tvusernmae);
        //tvusernmae.setText(PrefManager.getString(this, PrefManager.OTHER_NAME));
        tvusernmae.setText(PrefManager.getString(this, PrefManager.NMAE));

        mHeartLayout = (HeartLayout) findViewById(R.id.heart_layout);
        mTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                mHeartLayout.post(new Runnable() {
                    @Override
                    public void run() {
                        mHeartLayout.addHeart(randomColor());
                    }
                });
            }
        }, 1000, 800);

        initUI();
        initData();
    }

    private int randomColor() {
        return Color.parseColor("#8100be");
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void initUI() {
        TextView roomName = findViewById(R.id.live_room_name);
        //roomName.setText("Photography");
        //roomName.setText("c50fce8328014152e8b82ee86eb4d82f");
        //roomName.setText(config().getChannelName());
        System.out.println("smamsmsamnasmasas_LIVE_AUDIA_CHANNELWithUid=" + PrefManager.getString(LiveAudienceActivity.this, PrefManager.OTHER_ID));
        System.out.println("smamsmsamnasmasas_LIVE_AUDIA_CHANNELWithUid=" + PrefManager.getString(this, PrefManager.CHANNEL) + "");
        roomName.setText(PrefManager.getString(this, PrefManager.CHANNEL) + "");
        roomName.setSelected(true);

        initUserIcon();

        int role = getIntent().getIntExtra(Constants.KEY_CLIENT_ROLE, 2);
        boolean isBroadcaster = (role == 1);

        tvminute = findViewById(R.id.tvminute);
        ivheart = findViewById(R.id.ivheart);
        ivcloss = findViewById(R.id.ivcloss);
        llcomments = findViewById(R.id.llcomments);
        ivsend = findViewById(R.id.ivsend);
        etcomment = findViewById(R.id.etcomment);
        mMuteVideoBtn = findViewById(R.id.live_btn_mute_video);
        mMuteVideoBtn.setActivated(isBroadcaster);
        scrollView = (ScrollView) findViewById(R.id.scrollView);
        mMuteAudioBtn = findViewById(R.id.live_btn_mute_audio);
        mMuteAudioBtn.setActivated(isBroadcaster);
        //
        rv_user = (RecyclerView) findViewById(R.id.rv_user);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(LiveAudienceActivity.this);
        rv_user.setLayoutManager(layoutManager);
        rv_user.setItemAnimator(new DefaultItemAnimator());

        ivcloss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                close();
            }
        });

        ImageView beautyBtn = findViewById(R.id.live_btn_beautification);
        beautyBtn.setActivated(true);
        rtcEngine().setBeautyEffectOptions(beautyBtn.isActivated(), Constants.DEFAULT_BEAUTY_OPTIONS);

        mVideoGridContainer = findViewById(R.id.live_video_grid_layout);
        mVideoGridContainer.setStatsManager(statsManager());

        rtcEngine().setClientRole(role);
        if (isBroadcaster) startBroadcast();

        System.out.println("gouravkadam=" + isBroadcaster);
        System.out.println("gouravkadam=" + mVideoGridContainer.isActivated());

        Firebase.setAndroidContext(this);
        try {
            ref_comment = new Firebase("https://dj-bitz.firebaseio.com/" + "Comments").child(PrefManager.getString(this, PrefManager.CHANNEL));
            ref_live_count = new Firebase("https://dj-bitz.firebaseio.com/" + "LiveCount").child(PrefManager.getString(this, PrefManager.CHANNEL));
            ref_time = new Firebase("https://dj-bitz.firebaseio.com/" + "Time").child(PrefManager.getString(this, PrefManager.CHANNEL));
            ref_like = new Firebase("https://dj-bitz.firebaseio.com/" + "Like").child(PrefManager.getString(this, PrefManager.CHANNEL));
            ref_broadcast_live = new Firebase("https://dj-bitz.firebaseio.com/" + "BLive").child(PrefManager.getString(this, PrefManager.CHANNEL));
        } catch (Exception e) {
            e.printStackTrace();
        }

        ivsend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String messageText = etcomment.getText().toString().trim();
                if (!messageText.equals("")) {
                    Map<String, Object> map = new HashMap<String, Object>();
                    map.put("user_id", PrefManager.getString(LiveAudienceActivity.this, PrefManager.OTHER_ID));
                    map.put("comment", messageText);
                    map.put("isLike", false);
                    map.put("username", PrefManager.getString(LiveAudienceActivity.this, PrefManager.LOGIN_NAME_FINAL));
                    ref_comment.push().setValue(map);
                    etcomment.setText("");
                    scrollView.fullScroll(View.FOCUS_DOWN);
                }
            }
        });

        //ref_live_count.getKey()

        System.out.println("message_message=" + PrefManager.getString(this, PrefManager.CHANNEL));


        Timer t1 = new Timer();
        t1.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mHeartLayout.setVisibility(View.GONE);
                    }
                });

            }
        }, 0, 10000);

        ivheart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mHeartLayout.setVisibility(View.VISIBLE);
                Map<String, Object> map = new HashMap<String, Object>();
                map.put("username", PrefManager.getString(LiveAudienceActivity.this, PrefManager.LOGIN_NAME_FINAL));
                map.put("like", "1");
                map.put("user_id", PrefManager.getString(LiveAudienceActivity.this, PrefManager.OTHER_ID));
                ref_like.push().setValue(map);

                Map<String, Object> map1 = new HashMap<String, Object>();
                map1.put("user_id", PrefManager.getString(LiveAudienceActivity.this, PrefManager.OTHER_ID));
                map1.put("comment", "");
                map1.put("isLike", true);
                map1.put("username", PrefManager.getString(LiveAudienceActivity.this, PrefManager.LOGIN_NAME_FINAL));
                ref_comment.push().setValue(map1);
                etcomment.setText("");
                scrollView.fullScroll(View.FOCUS_DOWN);
            }
        });

        try {
            ref_comment.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    Map map = dataSnapshot.getValue(Map.class);

                    System.out.println("sasasasasa1212121212_ref_comment=" + map.toString());

                    String message = map.get("comment").toString().trim();
                    String audience_name = map.get("username").toString().trim();
                    Boolean isLike = (Boolean) dataSnapshot.child("isLike").getValue();
                    addMessageBox(message, audience_name, isLike);
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {
                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                }

                @Override
                public void onCancelled(FirebaseError firebaseError) {
                }
            });

            ref_broadcast_live.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    Map map = dataSnapshot.getValue(Map.class);

                    System.out.println("sasasasasa1212121212_ref_comment=" + map.toString());

                    /*Boolean blive = (Boolean) dataSnapshot.child("blive").getValue();
                    if (!blive) {
                        Toast.makeText(LiveAudienceActivity.this, "User exit.", Toast.LENGTH_SHORT).show();
                        close();
                    }*/
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {
                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                }

                @Override
                public void onCancelled(FirebaseError firebaseError) {
                }
            });

            /*ref_live_count.child("user_id").child(PrefManager.getString(LiveAudienceActivity.this, PrefManager.ID)).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if(dataSnapshot.exists()){
                        System.out.println("message_message_new=" + "IFIF");
                    } else
                    {
                        System.out.println("message_message_new=" + "ELSEELSE");
                        Map<String, String> map1 = new HashMap<String, String>();
                        map1.put("user_id", PrefManager.getString(LiveAudienceActivity.this, PrefManager.ID));
                        ref_live_count.push().setValue(map1);
                    }
                }

                @Override
                public void onCancelled(FirebaseError firebaseError) { }
            });*/

            ref_live_count.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    int size = (int) dataSnapshot.getChildrenCount();
                    tvview.setText(size + "");
                    System.out.println("sasasasasasasasasasa_onDataChange=" + size);
                }

                @Override
                public void onCancelled(FirebaseError firebaseError) {

                }
            });

            //map1.put("user_id", PrefManager.getString(LiveAudienceActivity.this, PrefManager.ID));

            ref_live_count.orderByChild("user_id").equalTo(PrefManager.getString(LiveAudienceActivity.this, PrefManager.ID)).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot)
                {
                    if(dataSnapshot.exists())
                    {
                        System.out.println("message_message_new=" + "IFIF");
                    }
                    else
                    {
                        System.out.println("message_message_new=" + "ELSEELSE");
                        Map<String, String> map1 = new HashMap<String, String>();
                        map1.put("user_id", PrefManager.getString(LiveAudienceActivity.this, PrefManager.ID));
                        ref_live_count.push().setValue(map1);
                    }
                    String auti_id = dataSnapshot.getKey();
                    System.out.println("message_message_new=" + auti_id);
                    for (DataSnapshot child : dataSnapshot.getChildren()) {
                        String auto_id = child.getKey();
                        PrefManager.setString(LiveAudienceActivity.this, PrefManager.FIREBASE_AUDIENCE_AUTO_ID, auto_id);
                        System.out.println("message_message_new key"+ child.getKey());
                        System.out.println("message_message_new ref"+child.getRef().toString());
                        System.out.println("message_message_new val"+ child.getValue().toString());
                    }
                }

                @Override
                public void onCancelled(FirebaseError firebaseError) {

                }
            });

            ref_live_count.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    //views.clear();
                    String key = ref_live_count.child(PrefManager.getString(LiveAudienceActivity.this, PrefManager.ID)).push().getKey();

                    String auti_id = dataSnapshot.getKey();

                    System.out.println("message_message=" + key + "<>" + auti_id);

                    //PrefManager.setString(LiveAudienceActivity.this, PrefManager.FIREBASE_AUDIENCE_AUTO_ID, auti_id);

                    for (DataSnapshot ds : dataSnapshot.getChildren()) {
                        int number = ds.getValue(Integer.class);
                        views.add(number + "");
                    }
                    //tvview.setText(views.size()+"");
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                    /*views.clear();
                    System.out.println("LIVEAUDIENCE_onChildChanged="+dataSnapshot.getKey());
                    for(DataSnapshot ds : dataSnapshot.getChildren()) {
                        int number = ds.getValue(Integer.class);
                        views.add(number+"");
                    }
                    view_count = views.size();
                    tvview.setText(views.size()+"");*/
                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {
                    //views.clear();
                    //auti_id = dataSnapshot.getKey();
                    System.out.println("LIVEAUDIENCE_onChildRemoved=" + dataSnapshot.getKey());
                    for (DataSnapshot ds : dataSnapshot.getChildren()) {
                        int number = ds.getValue(Integer.class);
                        view_remove.add(number + "");
                    }
                    int view = views.size();
                    int remove = view_remove.size();
                    int total = view - remove;
                    //tvview.setText(total+"");
                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                   /* views.clear();
                    System.out.println("LIVEAUDIENCE_onChildMoved="+dataSnapshot.getKey());
                    for(DataSnapshot ds : dataSnapshot.getChildren()) {
                        int number = ds.getValue(Integer.class);
                        views.add(number+"");
                    }
                    view_count = views.size();
                    tvview.setText(views.size()+"");*/
                }

                @Override
                public void onCancelled(FirebaseError firebaseError) {
                }
            });

            ref_time.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    tvminute.setText(dataSnapshot.getValue() + "");
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                    tvminute.setText(dataSnapshot.getValue() + "");
                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {
                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                }

                @Override
                public void onCancelled(FirebaseError firebaseError) {
                }
            });
            ref_like.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    Map map = dataSnapshot.getValue(Map.class);

                    System.out.println("sasasasasa1212121212_ref_like=" + map.toString());

                    String like = map.get("like").toString().trim();
                    if (like.equalsIgnoreCase("1")) {
                        mHeartLayout.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                    Map map = dataSnapshot.getValue(Map.class);

                    System.out.println("sasasasasa1212121212_ref_like" + map.toString());

                    String like = map.get("like").toString().trim();
                    if (like.equalsIgnoreCase("1")) {
                        mHeartLayout.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {
                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                }

                @Override
                public void onCancelled(FirebaseError firebaseError) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("message_message=" + e.getMessage());
        }
    }

    private void close() {
        //System.out.println("message_message_auto id=" + PrefManager.getString(LiveAudienceActivity.this, PrefManager.FIREBASE_AUDIENCE_AUTO_ID));
        //System.out.println("message_message=" + PrefManager.getString(this, PrefManager.CHANNEL));
        if(!PrefManager.getString(LiveAudienceActivity.this, PrefManager.FIREBASE_AUDIENCE_AUTO_ID).isEmpty())
        {
            Firebase ref_live_count = new Firebase("https://dj-bitz.firebaseio.com/" + "LiveCount").child(PrefManager.getString(this, PrefManager.CHANNEL)).child(PrefManager.getString(LiveAudienceActivity.this, PrefManager.FIREBASE_AUDIENCE_AUTO_ID));
            ref_live_count.removeValue();
            System.out.println("message_message=" + ref_live_count);
            finish();
            Intent intent = new Intent(LiveAudienceActivity.this, MainActivity1.class);
            startActivity(intent);
        }
        else
        {
            Toast.makeText(this, "try again!", Toast.LENGTH_SHORT).show();
        }
        //Firebase ref_live_count = new Firebase("https://dj-bitz.firebaseio.com/" + "LiveCount").child(PrefManager.getString(this, PrefManager.CHANNEL)).child(PrefManager.getString(LiveAudienceActivity.this, PrefManager.FIREBASE_AUDIENCE_AUTO_ID));
        //ref_live_count.removeValue();

        //System.out.println("message_message=" + ref_live_count);
        //ref_comment.removeValue();
        //ref_live_count.removeValue();
        //ref_time.removeValue();
        //ref_like.removeValue();
        //ref_broadcast_live.removeValue();

        /*Intent i = new Intent(LiveAudienceActivity.this, MainActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);*/
        //finish();

        //Intent intent = getIntent();
        //finish();
        //startActivity(intent);

        //asas
    }

    public void addMessageBox(String message, String audience_name, Boolean isLike) {
        MsgBean bean2 = new MsgBean();
        bean2.setAudience_name(audience_name);
        bean2.setLike(isLike);
        bean2.setMessage(message);
        beanList.add(bean2);

        final AdapterMsg adapterMsg = new AdapterMsg(beanList, LiveAudienceActivity.this);
        rv_user.setAdapter(adapterMsg);

        rv_user.scrollToPosition(beanList.size() - 1);
    }

    private void initUserIcon() {
        Bitmap origin = BitmapFactory.decodeResource(getResources(), R.drawable.fake_user_icon);
        RoundedBitmapDrawable drawable = RoundedBitmapDrawableFactory.create(getResources(), origin);
        drawable.setCircular(true);
        ImageView iconView = findViewById(R.id.live_name_board_icon);
        iconView.setImageDrawable(drawable);
    }

    private void initData() {
        mVideoDimension = Constants.VIDEO_DIMENSIONS[config().getVideoDimenIndex()];
    }

    @Override
    protected void onGlobalLayoutCompleted() {
    }

    private void startBroadcast() {
        rtcEngine().setClientRole(1);
        SurfaceView surface = prepareRtcVideo(0, true);
        mVideoGridContainer.addUserVideoSurface(0, surface, true);
        System.out.println("gouravkadam=" + "startBroadcast");
        mMuteAudioBtn.setActivated(true);
    }

    private void stopBroadcast() {
        rtcEngine().setClientRole(2);
        removeRtcVideo(0, true);
        mVideoGridContainer.removeUserVideo(0, true);
        System.out.println("gouravkadam=" + "stopBroadcast");
        mMuteAudioBtn.setActivated(false);
    }

    @Override
    public void onJoinChannelSuccess(String channel, int uid, int elapsed) {
        // Do nothing at the moment
    }

    @Override
    public void onUserJoined(int uid, int elapsed) {
        // Do nothing at the moment
    }

    @Override
    public void onUserOffline(final int uid, int reason) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                removeRemoteUser(uid);
                System.out.println("gouravkadam=" + "removeRemoteUser");
            }
        });
    }

    @Override
    public void onFirstRemoteVideoDecoded(final int uid, int width, int height, int elapsed) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                renderRemoteUser(uid);
                System.out.println("gouravkadam=" + "renderRemoteUser" + "<fun>");
            }
        });
    }

    private void renderRemoteUser(int uid) {
        SurfaceView surface = prepareRtcVideo(uid, false);
        mVideoGridContainer.addUserVideoSurface(uid, surface, false);
        System.out.println("gouravkadam=" + "renderRemoteUser");
    }

    private void removeRemoteUser(int uid) {
        removeRtcVideo(uid, false);
        mVideoGridContainer.removeUserVideo(uid, false);
        System.out.println("gouravkadam=" + "removeRemoteUser");
    }

    @Override
    public void onLocalVideoStats(IRtcEngineEventHandler.LocalVideoStats stats) {
        if (!statsManager().isEnabled()) return;

        LocalStatsData data = (LocalStatsData) statsManager().getStatsData(0);
        if (data == null) return;

        data.setWidth(mVideoDimension.width);
        data.setHeight(mVideoDimension.height);
        data.setFramerate(stats.sentFrameRate);
    }

    @Override
    public void onRtcStats(IRtcEngineEventHandler.RtcStats stats) {
        if (!statsManager().isEnabled()) return;

        LocalStatsData data = (LocalStatsData) statsManager().getStatsData(0);
        if (data == null) return;

        data.setLastMileDelay(stats.lastmileDelay);
        data.setVideoSendBitrate(stats.txVideoKBitRate);
        data.setVideoRecvBitrate(stats.rxVideoKBitRate);
        data.setAudioSendBitrate(stats.txAudioKBitRate);
        data.setAudioRecvBitrate(stats.rxAudioKBitRate);
        data.setCpuApp(stats.cpuAppUsage);
        data.setCpuTotal(stats.cpuAppUsage);
        data.setSendLoss(stats.txPacketLossRate);
        data.setRecvLoss(stats.rxPacketLossRate);
    }

    @Override
    public void onNetworkQuality(int uid, int txQuality, int rxQuality) {
        if (!statsManager().isEnabled()) return;

        StatsData data = statsManager().getStatsData(uid);
        if (data == null) return;

        data.setSendQuality(statsManager().qualityToString(txQuality));
        data.setRecvQuality(statsManager().qualityToString(rxQuality));
    }

    @Override
    public void onRemoteVideoStats(IRtcEngineEventHandler.RemoteVideoStats stats) {
        if (!statsManager().isEnabled()) return;

        RemoteStatsData data = (RemoteStatsData) statsManager().getStatsData(stats.uid);
        if (data == null) return;

        System.out.println("gouravkadam_v_data=" + data);

        data.setWidth(stats.width);
        data.setHeight(stats.height);
        data.setFramerate(stats.rendererOutputFrameRate);
        data.setVideoDelay(stats.delay);
    }

    @Override
    public void onRemoteAudioStats(IRtcEngineEventHandler.RemoteAudioStats stats) {
        if (!statsManager().isEnabled()) return;

        RemoteStatsData data = (RemoteStatsData) statsManager().getStatsData(stats.uid);
        if (data == null) return;

        System.out.println("gouravkadam_remove_data=" + data);

        data.setAudioNetDelay(stats.networkTransportDelay);
        data.setAudioNetJitter(stats.jitterBufferDelay);
        data.setAudioLoss(stats.audioLossRate);
        data.setAudioQuality(statsManager().qualityToString(stats.quality));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        close();
        statsManager().clearAllData();
    }

    public void onLeaveClicked(View view) {
        finish();
    }

    public void onSwitchCameraClicked(View view) {
        rtcEngine().switchCamera();
    }

    public void onBeautyClicked(View view) {
        view.setActivated(!view.isActivated());
        rtcEngine().setBeautyEffectOptions(view.isActivated(), Constants.DEFAULT_BEAUTY_OPTIONS);
    }

    public void onMoreClicked(View view) {
        // Do nothing at the moment
    }

    public void onPushStreamClicked(View view) {
        // Do nothing at the moment
    }

    public void onMuteAudioClicked(View view) {
        if (!mMuteVideoBtn.isActivated()) return;

        rtcEngine().muteLocalAudioStream(view.isActivated());
        view.setActivated(!view.isActivated());
    }

    public void onMuteVideoClicked(View view) {
        if (view.isActivated()) {
            stopBroadcast();
        } else {
            startBroadcast();
        }
        view.setActivated(!view.isActivated());
    }
}

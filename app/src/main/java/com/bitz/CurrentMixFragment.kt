package com.bitz

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bitz.extensions.config
import com.bitz.retrofit2.DJBitzInterFace
import com.bitz.retrofit2.DJBitzModel
import com.bitz.utils.Constants
import com.firebase.client.*
import com.squareup.picasso.Picasso
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.row_comment.view.*
import kotlinx.android.synthetic.main.row_comment.view.editComment
import kotlinx.android.synthetic.main.row_song.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList


class CurrentMixFragment : Fragment() {

    companion object {
        fun newInstance(): CurrentMixFragment {
            return CurrentMixFragment()
        }
    }

    val DJBitzApiServe by lazy {
        DJBitzInterFace.create()
    }

    var beatPlay:ImageButton? = null
    var beatNext:ImageButton? = null
    var beatPrev:ImageButton? = null
    var beatRepeat:ImageButton? = null
    var beatShuffle:ImageButton? = null
    var beatShare:ImageButton? = null
    var beatSeekBar: SeekBar? = null
    var textMaxDuration: TextView? = null
    var textCurrentDuration: TextView? = null
    var textNameDJ: TextView? = null
    var textCountLikes: TextView? = null
    var textCountListened: TextView? = null
    var textNameMusic: TextView? = null
    var beatLoading: ProgressBar? = null
//    var bRepeat = false
//    var bShuffle = false
    var reference1: Firebase? = null
    var count: Long = 0
    var llcomments: LinearLayout? = null
    var sellerid_chat_with: String? = null
    var reference2:Firebase? = null
    var beatAvatar: ImageButton? = null
    var recyclerView: RecyclerView? = null
    lateinit var buttonBack: ImageButton
    lateinit var buttonSearch: SearchView
    lateinit var commentAdapter: CommentAdapter
    var arrListComment = ArrayList<DJBitzModel.DJBitzMusicList.Comment>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var view = inflater?.inflate(R.layout.fragment_current_mix, container, false)

        textMaxDuration = view.findViewById<TextView>(R.id.textMaxDuration)
        textCurrentDuration = view.findViewById<TextView>(R.id.textCurrentDuration)
        beatPlay = view.findViewById<ImageButton>(R.id.beat_play)
        beatPrev = view.findViewById<ImageButton>(R.id.beat_prev)
        beatNext = view.findViewById<ImageButton>(R.id.beat_next)
        llcomments = view.findViewById<LinearLayout>(R.id.llcomments)

        beatSeekBar = view.findViewById<SeekBar>(R.id.beat_seek_bar)
        beatLoading = view.findViewById<ProgressBar>(R.id.beat_loading)

        buttonBack = view.findViewById<ImageButton>(R.id.buttonBack)
        buttonSearch = view.findViewById<SearchView>(R.id.buttonSearch)
        recyclerView = view.findViewById<RecyclerView>(R.id.recyclerView)


        val type0 = DJBitzModel.DJBitzMusicList.Comment(type = 0)
        arrListComment = ArrayList<DJBitzModel.DJBitzMusicList.Comment>()
        arrListComment.add(type0)

        val linearLayoutManager = LinearLayoutManager(context)
        //gridLayoutManager = GridLayoutManager(this, 2)
        recyclerView?.layoutManager = linearLayoutManager

        commentAdapter = CommentAdapter(arrListComment)
        recyclerView?.adapter = commentAdapter


        var title = view.findViewById<TextView>(R.id.message)
        title.text = "CURRENT MIX"

        return view
    }

    fun initPlayer(){
        var mainActivity = (activity as MainActivity1)
        mainActivity.rlPlayer.visibility = View.GONE
        mainActivity.bottomBar?.visibility = View.GONE
        if(mainActivity.listMusicsPlayer != null && mainActivity.listMusicsPlayer!!.size > mainActivity.currentMusicPlayer) {

            var listMusics: List<DJBitzModel.DJBitzMusicList.MusicRecords>?
            listMusics = mainActivity.listMusicsPlayer
            if (mainActivity.config.isShuffleEnabled){
                listMusics = mainActivity.listMusicsPlayerShuffle
            }
            buttonSearch.visibility = View.GONE
            buttonBack.setOnClickListener(View.OnClickListener {
                mainActivity.bottomBar?.visibility = View.VISIBLE
                mainActivity.hideCurrentMixFragment()
            })

            if (mainActivity.songCurrentPlayer != null) {
                textMaxDuration?.text = totalTime((mainActivity.songCurrentPlayer)?.duration!!)
                textNameDJ?.text = (mainActivity.songCurrentPlayer)?.DJ
                textNameMusic?.text = (mainActivity.songCurrentPlayer)?.name
            }

            beatPlay?.visibility = View.GONE
            beatLoading?.visibility = View.VISIBLE


            beatPlay?.setOnClickListener(View.OnClickListener {

                if (mainActivity.isPlaying){
                    mainActivity.pauseMedia()
                    beatPlay?.setImageResource(R.mipmap.ic_play)
                }
                else{
                    mainActivity.playMedia()
                    beatPlay?.setImageResource(R.mipmap.ic_pause)
                }
            })

            beatPrev?.setOnClickListener(View.OnClickListener {

                if (mainActivity.currentMusicPlayer > 0) {

                    beatPlay?.visibility = View.GONE
                    beatLoading?.visibility = View.VISIBLE
                    mainActivity.currentMusicPlayer--
                    mainActivity.prevMusic()
                }

            })

            beatNext?.setOnClickListener(View.OnClickListener {
                if (mainActivity.currentMusicPlayer < listMusics!!.size - 1) {
                    beatPlay?.visibility = View.GONE
                    beatLoading?.visibility = View.VISIBLE
                    mainActivity.currentMusicPlayer++
                    mainActivity.nextMusic()
                }
            })

            beatShare?.setOnClickListener(View.OnClickListener {
                shareIntent()

            })

            var duration = mainActivity.getMaxDuration((mainActivity.songCurrentPlayer)?.duration!!)
            textMaxDuration?.text = "";//milliSecondsToTimer(duration)
            textCurrentDuration?.text = ""
            beatSeekBar?.max = duration

        }
    }

    fun setCommentAdapter(comments: List<DJBitzModel.DJBitzMusicList.Comment>){
        val type0 = DJBitzModel.DJBitzMusicList.Comment(type = 0)
        arrListComment.clear()
        arrListComment.add(type0)
        if(comments != null) {
            arrListComment.addAll(comments)
        }
        commentAdapter.notifyDataSetChanged()
    }

    fun shareIntent(){
        var shareBody = ""

        shareBody += ((activity as MainActivity1).songCurrentPlayer)?.name
        shareBody +=  "\n\n" + ((activity as MainActivity1).songCurrentPlayer)?.DJ
        shareBody +=  "\n\n" + ((activity as MainActivity1).songCurrentPlayer)?.description
        shareBody +=  "\n\n" + Constants.BASE_URL + ((activity as MainActivity1).songCurrentPlayer)?.music

        val myShareIntent =
            Intent(Intent.ACTION_SEND)
        myShareIntent.setType ("text/plain" )
        myShareIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        activity?.startActivity(myShareIntent)
    }

    fun totalTime(totalTimes: String):String{
        var timesResult = ""
        try {
            val split = totalTimes.split(":")

            if (!split.get(0).equals("00")){
                if (split.get(0).substring(0,1).equals("0")){
                    timesResult += split.get(0).substring(1,2) + ":"
                }
                else {
                    timesResult += split.get(0) + ":"
                }
            }

            timesResult += split.get(1) + ":"
            timesResult += split.get(2)

        }catch (e: Exception){
            e.stackTrace
        }

        return timesResult
    }

    fun milliSecondsToTimer(seconds: Int): String {
        var finalTimerString = ""
        var minutesString = ""
        var secondsString = ""

        if (seconds >= 0) {
            // Convert total duration into time
            val hours = (seconds / ( 60 * 60)).toInt()
            val minutes = (seconds % ( 60 * 60)).toInt() / (60)
            val secondss = (seconds % ( 60 * 60) % ( 60)).toInt()
            // Add hours if there
            if (hours > 0) {
                finalTimerString = "$hours:"

            }

            // Add hours if there
            if (minutes > 0) {
                minutesString = "$minutes:"
                if (minutes < 10)
                    minutesString = "0$minutes:"
            }

            if (minutesString.isEmpty()) {
                minutesString = "00:"
            }

            // Prepending 0 to seconds if it is one digit
            if (secondss < 10) {
                secondsString = "0$secondss"
            } else {
                secondsString = "" + secondss
            }

            finalTimerString = "$finalTimerString$minutesString$secondsString"

            // return timer string
            return finalTimerString
        }

        return "";
    }

    fun showError(error: String?) {
        try {
            if (activity != null) {
                (activity as MainActivity1).showHideProgressbar(false)
            }

            var message = error ?: getString(R.string.messager_load_server_error)
            AlertDialog.Builder(activity)
                .setTitle(R.string.title_load_server_error)
                .setMessage(message)
                .setPositiveButton(
                    android.R.string.ok
                ) { dialog, which -> dialog.dismiss() }

                .show()
        } catch (ex: java.lang.Exception) {

        }
    }

    fun showResult(result: DJBitzModel.DJBitzDJList.Result, id: String){

        result.result.djsRecords.forEach { djInfo->
            if (djInfo.name.equals(id)){
                if (djInfo.avatar_url != null && !djInfo.avatar_url.isEmpty()) {
                    var url = Constants.BASE_URL + djInfo.avatar_url.replace("\\","")

                    Picasso.get()
                        .load(url)
                        //.placeholder(R.mipmap.sampledjavatar)
                        .into(beatAvatar)
                }
            }
        }

    }

    fun setImageAvatar(id: String){
        if (activity != null) {
            (activity as MainActivity1).showHideProgressbar(true)
        }
        DJBitzApiServe.getDJList()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result ->
                    if (result != null && result.status.toLowerCase().equals(
                            "success"
                        )
                    ) {
                        if (activity != null) {
                            (activity as MainActivity1).showHideProgressbar(false)
                        }
                        showResult(result, id)
                    }
                    else {
                        //error
                        if (result != null && result.msg != null) {
                            showError(result.msg)
                        }else{
                            showError(null)
                        }
                    }
                },
                { error -> showError(null) }
            )

    }

    fun setImageRepeatShuffe(bRepeat: Boolean, bShuffle: Boolean){
        if (bRepeat){
            beatRepeat?.setImageResource(R.mipmap.ic_repeat)
        }
        else{
            beatRepeat?.setImageResource(R.mipmap.ic_repeat_disble)
        }

        if (bShuffle){
            beatShuffle?.setImageResource(R.mipmap.ic_shuffle)
        }
        else{
            beatShuffle?.setImageResource(R.mipmap.ic_shuffle_disble)
        }
    }


    inner class CommentAdapter(private val comments: List<DJBitzModel.DJBitzMusicList.Comment>) : RecyclerView.Adapter<CommentAdapter.CommentRowHolder>() {
        var onItemClick: (((DJBitzModel.DJBitzMusicList.Comment), adapterPosition: Int) -> Unit)? = null

        val DJBitzApiServe by lazy {
            DJBitzInterFace.create()
        }

        override fun getItemCount() = comments.size

        override fun getItemViewType(position: Int): Int {
            return comments.get(position).type
        }

        override fun onBindViewHolder(holder: CommentAdapter.CommentRowHolder, position: Int) {
            val comment = comments[position]
            holder.bind(comment)  }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CommentAdapter.CommentRowHolder {

            var inflatedView = parent.inflate(R.layout.row_comment, false)

            if (viewType == 0){
                inflatedView = parent.inflate(R.layout.row_comment_currentmix_type0, false)
            }

            return  CommentRowHolder(inflatedView)
        }

        inner class CommentRowHolder(v: View) : RecyclerView.ViewHolder(v), View.OnClickListener {
            private var view: View = v

            init {
                v.setOnClickListener(this)
            }

            override fun onClick(v: View) {
                onItemClick?.invoke(comments[adapterPosition], adapterPosition)

            }

            fun bind(commentNow: DJBitzModel.DJBitzMusicList.Comment) {

                if (commentNow.type == 1) {
                    if (commentNow.comment != null && !commentNow.comment.isEmpty()) {
                        view.editComment.setText(commentNow.comment)
                    }

                    if (commentNow.username != null && !commentNow.username.isEmpty()) {
                        view.textName.setText(commentNow.username)
                    }

                    if (commentNow.profile_avatar != null && !commentNow.profile_avatar.isEmpty()) {
                        var url = Constants.BASE_URL + commentNow.profile_avatar.replace("\\","")

                        Picasso.get()
                            .load(url)
                            .placeholder(R.mipmap.ic_avatar)
                            .into(view.beat_avatar)
                    }

                    if (commentNow.user_id != null && commentNow.user_id.equals(Constants.userInfo?.userInfo?.id)){
                        view.deleteComment.visibility = View.VISIBLE
                    }
                    else{
                        view.deleteComment.visibility = View.GONE
                    }
                    view.deleteComment.setOnClickListener {
                        try {



                            AlertDialog.Builder(activity)
                                .setTitle(R.string.title_delete_comment)
                                .setMessage(R.string.mess_delete_comment)
                                .setPositiveButton(
                                   R.string.yes
                                ) {
                                        dialog, which ->
                                    try {
                                        val params = HashMap<String, String>()
                                        params["email"] = Constants.userInfo?.userInfo?.email ?: ""
                                        params["comment_id"] = commentNow.id

                                        if (activity != null) {
                                            (activity as MainActivity1).showHideProgressbar(true)
                                        }

                                        DJBitzApiServe.postDeleteComment(params)
                                            .enqueue(object : Callback<DJBitzModel.ResponseBody.Result> {

                                                override fun onResponse(
                                                    call: Call<DJBitzModel.ResponseBody.Result>,
                                                    response: Response<DJBitzModel.ResponseBody.Result>
                                                ) {
                                                    if (response.isSuccessful && response.body() != null && response.body()!!.status.toLowerCase().equals(
                                                            "success"
                                                        )
                                                    ) {
                                                        if (activity != null) {
                                                            (activity as MainActivity1).showHideProgressbar(false)
                                                        }
                                                        (activity as MainActivity1).getMusicInfo(commentNow.music_id)
                                                    }
                                                    else {
                                                        //error
                                                        if (response.body()!= null) {
                                                            showError(response.body()!!.msg)
                                                        }else{
                                                            showError(null)
                                                        }
                                                    }
                                                }

                                                override fun onFailure(call: Call<DJBitzModel.ResponseBody.Result>, t: Throwable) {
                                                    showError(null)
                                                }
                                            })
                                    } catch (e: Exception) {
                                        e.stackTrace
                                    }
                                    dialog.dismiss()
                                }
                                .setNegativeButton(R.string.no){
                                        dialog, which ->
                                    dialog.dismiss()
                                }
                                .show()
                        } catch (ex: java.lang.Exception) {

                        }
                    }

//                if (commentNow.avatar_url != null && !commentNow.avatar_url.isEmpty()) {
//                    var url = Constants.BASE_URL + commentNow.avatar_url.replace("\\","")
//
//                    Picasso.get()
//                        .load(url)
//                        //.placeholder(R.mipmap.sampledjavatar)
//                        .into(view.beat_avatar)
//                }
                }
                else if (commentNow.type == 0) {

                    textNameDJ = view.findViewById<TextView>(R.id.textNameDJ)
                    textNameMusic = view.findViewById<TextView>(R.id.textNameMusic)

                    beatRepeat = view.findViewById<ImageButton>(R.id.beat_repeat)
                    beatShuffle = view.findViewById<ImageButton>(R.id.beat_shuffle)
                    beatShare = view.findViewById<ImageButton>(R.id.beat_share)
                    beatAvatar = view.findViewById<ImageButton>(R.id.beat_avatar)
                    textCountLikes = view.findViewById<TextView>(R.id.countLikes)
                    textCountListened = view.findViewById<TextView>(R.id.textCountListened)

                    val imgFavourite = view.findViewById<ImageView>(R.id.imgFavourite)

                    val mainActivity = activity as MainActivity1
                    val sharedPref = mainActivity.getPreferences(Context.MODE_PRIVATE)
                    var bRepeat = sharedPref.getBoolean("booleanRepeat", false)
                    var bShuffle = sharedPref.getBoolean("booleanShuffle", false)
                    mainActivity.config.repeatSong = bRepeat
                    mainActivity.config.isShuffleEnabled = bShuffle
                    setImageRepeatShuffe(bRepeat, bShuffle)

                    beatRepeat?.setOnClickListener(View.OnClickListener {

                        val repeatSong = !mainActivity.config.repeatSong

                        val editor = sharedPref.edit()
                        editor.putBoolean("booleanRepeat",repeatSong)
                        editor.putBoolean("booleanShuffle", false)
                        editor.commit()
                        setImageRepeatShuffe(repeatSong, false)

                        mainActivity.config.repeatSong = repeatSong
                    })

                    beatShuffle?.setOnClickListener(View.OnClickListener {


                        val shuffleSong = !mainActivity.config.isShuffleEnabled
                        mainActivity.config.isShuffleEnabled = shuffleSong

                        val editor = sharedPref.edit()
                        editor.putBoolean("booleanShuffle",shuffleSong)
                        editor.putBoolean("booleanRepeat",mainActivity.config.repeatSong)
                        editor.commit()
                        setImageRepeatShuffe(mainActivity.config.repeatSong, shuffleSong)
//                if (shuffleSong){
//                    listMusicsPlayerShuffle = listMusicsPlayerShuffle?.shuffled()
//                }

                    })


                    val btnSendComment = view.findViewById<Button>(R.id.btnSendComment)
                    val editComment = view.findViewById<EditText>(R.id.editComment)
                    val like = view.findViewById<RelativeLayout>(R.id.rlLike)

                    if (((activity as MainActivity1).songCurrentPlayer)?.is_liked.equals("0")) {
                        imgFavourite.setImageResource(R.mipmap.ic_favourite)
                    }
                    else{
                        imgFavourite.setImageResource(R.mipmap.ic_favourite_filled)
                    }

                    like.setOnClickListener {
                        if (((activity as MainActivity1).songCurrentPlayer)?.is_liked.equals("0")) {
                            try {
                                val params = HashMap<String, String>()
                                params["email"] = Constants.userInfo?.userInfo?.email ?: ""
                                params["music_id"] = ((activity as MainActivity1).songCurrentPlayer)?.id ?: "0"

                                if (activity != null) {
                                    (activity as MainActivity1).showHideProgressbar(true)
                                }

                                DJBitzApiServe.postLike(params).enqueue(object : Callback<DJBitzModel.ResponseBody.Result> {

                                    override fun onResponse(
                                        call: Call<DJBitzModel.ResponseBody.Result>,
                                        response: Response<DJBitzModel.ResponseBody.Result>
                                    ) {
                                        if (response.isSuccessful && response.body() != null && response.body()!!.status.toLowerCase().equals(
                                                "success"
                                            )
                                        ) {
                                            if (activity != null) {
                                                (activity as MainActivity1).showHideProgressbar(false)
                                            }

                                           textCountLikes!!.text = (textCountLikes!!.text.toString().toInt() + 1).toString()
                                            imgFavourite.setImageResource(R.mipmap.ic_favourite_filled)
                                            ((activity as MainActivity1).songCurrentPlayer)?.is_liked = "1"
//                                            notifyItemChanged(adapterPosition)
                                        }
                                        else {
                                            //error
                                            if (response.body() != null && response.body()!!.msg != null) {
                                                showError(response.body()!!.msg)
                                            }else{
                                                showError(null)
                                            }
                                        }
                                    }

                                    override fun onFailure(call: Call<DJBitzModel.ResponseBody.Result>, t: Throwable) {
                                        showError(null)
                                    }
                                })
                            } catch (e: Exception) {
                                e.stackTrace
                                showError(null)
                            }
                        } else {
                            try {
                                val params = HashMap<String, String>()
                                params["email"] = Constants.userInfo?.userInfo?.email ?: ""
                                params["music_id"] = ((activity as MainActivity1).songCurrentPlayer)?.id ?: "0"

                                if (activity != null) {
                                    (activity as MainActivity1).showHideProgressbar(true)
                                }

                                DJBitzApiServe.postDisLike(params)
                                    .enqueue(object : Callback<DJBitzModel.ResponseBody.Result> {

                                        override fun onResponse(
                                            call: Call<DJBitzModel.ResponseBody.Result>,
                                            response: Response<DJBitzModel.ResponseBody.Result>
                                        ) {
                                            if (response.isSuccessful && response.body() != null && response.body()!!.status.toLowerCase().equals(
                                                    "success"
                                                )
                                            ) {
                                                if (activity != null) {
                                                    (activity as MainActivity1).showHideProgressbar(false)
                                                }
                                                imgFavourite.setImageResource(R.mipmap.ic_favourite)
                                                textCountLikes!!.text = (textCountLikes!!.text.toString().toInt() - 1).toString()

                                                ((activity as MainActivity1).songCurrentPlayer)?.is_liked = "0"
//                                                notifyItemChanged(adapterPosition)
                                            }
                                            else {
                                                //error
                                                if (response.body()!= null) {
                                                    showError(response.body()!!.msg)
                                                }else{
                                                    showError(null)
                                                }
                                            }
                                        }

                                        override fun onFailure(call: Call<DJBitzModel.ResponseBody.Result>, t: Throwable) {
                                            showError(null)
                                        }
                                    })
                            } catch (e: Exception) {
                                e.stackTrace
                            }
                        }
                    }

                    btnSendComment.setOnClickListener {
                        try {
                            if (activity != null) {
                                (activity as MainActivity1).showHideProgressbar(true)
                            }

                            // Hide keyboard
                            view.btnSendComment.hideKeyboard()

                            if ((activity as MainActivity1).songCurrentPlayer != null) {
                                val params = HashMap<String, String>()
                                params["email"] = Constants.userInfo?.userInfo?.email ?: ""
                                params["music_id"] = (activity as MainActivity1).songCurrentPlayer!!.id

                                params["comment"] = editComment.text.toString()

                                (activity as MainActivity1).showHideProgressbar(true)
                                DJBitzApiServe.postAddComment(params).enqueue(object : Callback<DJBitzModel.ResponseBody.Result> {

                                    override fun onResponse(
                                        call: Call<DJBitzModel.ResponseBody.Result>,
                                        response: Response<DJBitzModel.ResponseBody.Result>
                                    ) {
                                        if (response.isSuccessful && response.body() != null && response.body()!!.status.toLowerCase().equals("success")){

                                            if (activity != null) {
                                                (activity as MainActivity1).showHideProgressbar(false)
                                            }
                                            editComment.setText("")

                                            if ((activity as MainActivity1).songCurrentPlayer!= null) {
                                                (activity as MainActivity1).getMusicInfo((activity as MainActivity1).songCurrentPlayer!!.id)
                                            }
                                        }
                                        else {
                                            //error
                                            if (response.body() != null && response.body()!!.msg != null) {
                                                showError(response.body()!!.msg)
                                            }else{
                                                showError(null)
                                            }
                                        }
                                    }

                                    override fun onFailure(call: Call<DJBitzModel.ResponseBody.Result>, t: Throwable) {
                                        showError(null)
                                    }
//
                                })
                            }
                        } catch (e: Exception) {
                            e.stackTrace
                        }
                    }
                }
            }
        }
    }

    fun View.hideKeyboard() {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(windowToken, 0)
    }


    fun Int.toPx(): Int = (this * Resources.getSystem().displayMetrics.density).toInt()
}
package com.bitz

import android.app.AlertDialog
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.SearchView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bitz.retrofit2.DJBitzInterFace
import com.bitz.retrofit2.DJBitzModel
import com.bitz.utils.Constants
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.forEach
import kotlin.collections.set
import kotlin.collections.shuffled
import kotlin.collections.toList


class LibrarySongsFragment : Fragment() {

    companion object {

        fun newInstance(): LibrarySongsFragment {
            return LibrarySongsFragment()
        }
    }

    val DJBitzApiServe by lazy {
        DJBitzInterFace.create()
    }

    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var adapter: RecyclerAdapter
    private var songsList = ArrayList<DJBitzModel.DJBitzMusicList.MusicRecords>()
    private var songsListAll = ArrayList<DJBitzModel.DJBitzMusicList.MusicRecords>()
    private lateinit var recyclerView: RecyclerView
    var showToolbar: Boolean = false
    var textTitle: String = ""
    var textSearch: String = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var view = inflater?.inflate(R.layout.fragment_library_songs, container, false)

        recyclerView = view.findViewById<RecyclerView>(R.id.recyclerView)

        linearLayoutManager = LinearLayoutManager(context)
        //gridLayoutManager = GridLayoutManager(this, 2)
        recyclerView.layoutManager = linearLayoutManager

        adapter = RecyclerAdapter(songsList, 0, (activity as MainActivity1))
        adapter.onItemClick = { song, index ->
            (activity as MainActivity1).currentMusicPlayer = index
            (activity as MainActivity1).listMusicsPlayer = songsList
            (activity as MainActivity1).listMusicsPlayerShuffle = ArrayList<DJBitzModel.DJBitzMusicList.MusicRecords>()
            (activity as MainActivity1).listMusicsPlayerShuffle!!.addAll(songsList)
            val listShuffer = (activity as MainActivity1).listMusicsPlayerShuffle!!.toList().shuffled()
            (activity as MainActivity1).listMusicsPlayerShuffle!!.clear()
            (activity as MainActivity1).listMusicsPlayerShuffle!!.addAll(listShuffer)
            (activity as MainActivity1).bShowCurrentMixFragment = true

//            val sharedPref = activity!!.getPreferences(Context.MODE_PRIVATE)
//            var bShuffle = sharedPref.getBoolean("booleanShuffle", false)
//            if (bShuffle) {
//                (activity as MainActivity).currentMusicPlayer =
//                    (activity as MainActivity).listMusicsPlayerShuffle!!.indexOf(song)
//            }

            var url = Constants.BASE_URL + song.music
            (activity as MainActivity1).playMedia(url)
        }

        var mainActivity = (activity as MainActivity1)
        if (mainActivity.isPlaying) {
            mainActivity.rlPlayer.visibility = View.VISIBLE
        }
        else{
            mainActivity.rlPlayer.visibility = View.GONE
        }

        recyclerView.adapter = adapter


        if (showToolbar) {
            var buttonBack = view.findViewById<ImageButton>(R.id.buttonBack)
            buttonBack.setOnClickListener(View.OnClickListener {
                var libraryFragment = (activity as MainActivity1).supportFragmentManager .findFragmentByTag("LibraryFragment") as LibraryFragment
                if (libraryFragment != null) {
                    libraryFragment.childFragmentManager.popBackStack()
                }
            })

            var buttonSearch = view.findViewById<SearchView>(R.id.buttonSearch)
            buttonSearch.setOnQueryTextListener(searchQueryListener)

            buttonSearch.setOnSearchClickListener(View.OnClickListener {
                //search is expanded
                buttonSearch.setBackground(resources.getDrawable(R.drawable.rounded_rectangle_ivorygray_searchview_24dp))
            })

            buttonSearch.setOnCloseListener { buttonSearch.setBackgroundColor(resources.getColor(android.R.color.transparent))
                false }

            val id = buttonSearch.getContext().getResources().getIdentifier("android:id/search_src_text", null, null)
            var editText =  buttonSearch.findViewById<TextView>(id)
            editText.setTextColor(Color.LTGRAY)
            editText.setHintTextColor(Color.LTGRAY)

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val face  = getResources().getFont(R.font.museosanscyrl_100)
                editText.setTypeface(face)

            } else {

            }



            var message = view.findViewById<ImageButton>(R.id.message) as TextView
            message.setText(textTitle)

        }
        else{
            var toolbar = view.findViewById<View>(R.id.toolbar)
            toolbar.visibility = View.GONE
        }

        return view
    }

    fun getListMusic(id: String){
        if (activity != null) {
            (activity as MainActivity1).showHideProgressbar(true)
        }

        val params = HashMap<String, String>()
        params["genreId"] = id
        params["uid"] = Constants.userInfo?.userInfo?.id ?: ""

        DJBitzApiServe.getMusicListWithGenre(params).enqueue(object : Callback<DJBitzModel.DJBitzMusicList.Result> {

            override fun onResponse(
                call: Call<DJBitzModel.DJBitzMusicList.Result>,
                result: Response<DJBitzModel.DJBitzMusicList.Result>
            ) {

                if (result != null && result.body() != null &&  result.body()!!.status.toLowerCase().equals(
                        "success"
                    )
                ) {
                    if (activity != null) {
                        (activity as MainActivity1).showHideProgressbar(false)
                    }

                    showResult(result.body()!!)
                }
                else {
                    //error
                    if (result.body() != null && result.body()!!.msg != null) {
                        showError(result.body()!!.msg)
                    }else{
                        showError(null)
                    }
                }

            }

            override fun onFailure(call: Call<DJBitzModel.DJBitzMusicList.Result>, t: Throwable) {
                showError(null)
            }
        })
    }

    fun getListMusic()
    {
        if (activity != null) {
            (activity as MainActivity1).showHideProgressbar(true)
        }
        val params = HashMap<String, String>()
        params["uid"] = Constants.userInfo?.userInfo?.id ?: ""

        DJBitzApiServe.getMusicList(params).enqueue(object : Callback<DJBitzModel.DJBitzMusicList.Result> {

            override fun onResponse(call: Call<DJBitzModel.DJBitzMusicList.Result>, result: Response<DJBitzModel.DJBitzMusicList.Result>) {

                if (result != null && result.body() != null &&  result.body()!!.status.toLowerCase().equals("success"))
                {
                    if (activity != null) {
                        (activity as MainActivity1).showHideProgressbar(false)
                    }

                    showResult(result.body()!!)

                }
                else {
                    //error
                    if (result.body() != null && result.body()!!.msg != null) {
                        //showError(result.body()!!.msg)
                        System.out.println("saasasasasasass="+"IFIF")
                    }else{
                        //showError(null)
                        System.out.println("saasasasasasass="+"ELSELSE")
                    }
                }

            }

            override fun onFailure(call: Call<DJBitzModel.DJBitzMusicList.Result>, t: Throwable) {
                //showError(null)
                System.out.println("saasasasasasass="+"FAIL")
            }
        })
    }

    fun getListMusicTopten(){
        if (activity != null) {
            (activity as MainActivity1).showHideProgressbar(true)
        }
        val params = HashMap<String, String>()
        params["uid"] = Constants.userInfo?.userInfo?.id ?: ""

        DJBitzApiServe.getTopMusicList(params).enqueue(object : Callback<DJBitzModel.DJBitzTopMusicList.Result> {

            override fun onResponse(
                call: Call<DJBitzModel.DJBitzTopMusicList.Result>,
                result: Response<DJBitzModel.DJBitzTopMusicList.Result>
            ) {

                if (result != null && result.body() != null &&  result.body()!!.status.toLowerCase().equals("success"))
                {
                    if (activity != null) {
                        (activity as MainActivity1).showHideProgressbar(false)
                    }

                    showResultTopMusics(result.body()!!)
                }
                else {
                    //showError(null)
                }

            }

            override fun onFailure(call: Call<DJBitzModel.DJBitzTopMusicList.Result>, t: Throwable) {
                //showError(null)
            }
        })

    }

    fun showError(error: String?) {
        try {
            if (activity != null) {
                (activity as MainActivity1).showHideProgressbar(false)
            }

            var message = error ?: getString(R.string.messager_load_server_error)
            AlertDialog.Builder(activity)
                .setTitle(R.string.title_load_server_error)
                .setMessage(message)
                .setPositiveButton(
                    android.R.string.ok
                ) { dialog, which -> dialog.dismiss() }

                .show()
        } catch (ex: java.lang.Exception) {

        }
    }

    fun showResult(result: DJBitzModel.DJBitzMusicList.Result){
        songsListAll.clear()
        result.result.musicRecords.forEach { song ->
            if(!song.music.isEmpty()){
                songsListAll.add(song)
            }
        }
//        songsListAll.addAll(result.result.musicRecords)
        filterSong()

    }

    fun showResultTopMusics(result: DJBitzModel.DJBitzTopMusicList.Result){
        songsListAll.clear()
        result.result.forEach { song ->
            if(!song.music.isEmpty()){
                songsListAll.add(song)
            }
        }
//        songsListAll.addAll(result.result)
        filterSong()

    }

    fun filterSong(){
        songsList.clear()
        if (!textSearch.isEmpty()) {
            songsListAll.forEach { song ->
                if (song.name.toLowerCase().contains(textSearch.toLowerCase())) {
                    songsList.add(song)

                }
            }
        }
        else{
            songsList.addAll(songsListAll)
        }

        adapter.notifyDataSetChanged()
    }

    private val searchQueryListener = object : SearchView.OnQueryTextListener {
        override fun onQueryTextSubmit(query: String): Boolean {

            return false
        }

        override fun onQueryTextChange(newText: String): Boolean {

            textSearch = newText

            filterSong()
            return true
        }

    }
}



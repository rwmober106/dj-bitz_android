package com.bitz

import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import com.bitz.retrofit2.DJBitzInterFace
import com.bitz.retrofit2.DJBitzModel
import com.bitz.utils.Constants
import com.bitz.utils.PreferencesHelper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_contact.*
import okhttp3.OkHttpClient
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.util.concurrent.TimeUnit


class ContactFragment : Fragment() {

    companion object {

        fun newInstance(): ContactFragment {
            return ContactFragment()
        }
    }


    val DJBitzApiServe by lazy {
        DJBitzInterFace.create()
    }

    var listDJ: List<DJBitzModel.DJBitzDJList.DJRecords>? = null
    val listDJName = arrayListOf<String>()
    var spinnerDJList: Spinner? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var view = inflater?.inflate(R.layout.fragment_contact, container, false)

        var frame = view.findViewById<FrameLayout>(R.id.frame)
        frame.visibility = View.VISIBLE

        var buttonSignOut = view.findViewById<Button>(R.id.btnSignOut)

        buttonSignOut.setOnClickListener {
            signOut()
        }


        var message = view.findViewById<ImageButton>(R.id.tvTitle) as TextView
        message.setText("CONTACT US")
        var mainActivity = (activity as MainActivity1)
        if (mainActivity.isPlaying) {
            mainActivity.rlPlayer.visibility = View.VISIBLE
        } else {
            mainActivity.rlPlayer.visibility = View.GONE
        }

        spinnerDJList = view.findViewById(R.id.spinnerDJList)

        var buttonConfirm = view.findViewById<Button>(R.id.buttonConfirm)
        var textViewEmail = view.findViewById<EditText>(R.id.textViewEmail)
        var textViewFirstName = view.findViewById<EditText>(R.id.textViewFirstName)
        var textViewLastName = view.findViewById<EditText>(R.id.textViewLastName)
        var textViewDate = view.findViewById<EditText>(R.id.textViewDate)
        var textViewEvent = view.findViewById<EditText>(R.id.textViewFirstName)
        var textViewNote = view.findViewById<EditText>(R.id.textViewNote)

        var ivContact = view.findViewById<ImageView>(R.id.imageView)
        var buttonBack = view.findViewById<ImageButton>(R.id.buttonBack)
        buttonBack.visibility = View.GONE
        var buttonSearch = view.findViewById<SearchView>(R.id.buttonSearch)
        buttonSearch.visibility = View.GONE
        var rlContainer = view.findViewById<RelativeLayout>(R.id.rlContent)
        var title = view.findViewById<ImageButton>(R.id.message) as TextView
        title.visibility = View.GONE

        var buttonProfile = view.findViewById<Button>(R.id.btnProfile)
        buttonProfile.setOnClickListener {
            var profileFragment = ProfileFragment.newInstance()
//            childFragmentManager.beginTransaction().add(R.id.frame, profileFragment, tag).addToBackStack("ProfileFragment").commit()
            childFragmentManager.beginTransaction().replace(R.id.frame, profileFragment, tag).addToBackStack(null).commit()

            message.setText("PROFILE")
            frame.visibility = View.VISIBLE
            btnProfile.visibility = View.GONE
            btnSignOut.visibility = View.GONE
            ivContact.visibility = View.GONE
            rlContainer.visibility = View.GONE

            buttonBack.visibility = View.VISIBLE
            buttonBack.setOnClickListener(View.OnClickListener {
                childFragmentManager.popBackStack()
                buttonBack.setOnClickListener(null)
                buttonBack.visibility = View.GONE
                buttonSearch.visibility = View.GONE
                frame.visibility = View.GONE
                message.setText("CONTACT US")
                btnProfile.visibility = View.VISIBLE
                btnSignOut.visibility = View.VISIBLE
                ivContact.visibility = View.VISIBLE
                rlContainer.visibility = View.VISIBLE
            })

        }

        buttonConfirm.setOnClickListener({
            val params = HashMap<String, String>()
            params["sender_email"] = textViewEmail.text.toString()
            params["first_name"] = textViewFirstName.text.toString()
            params["last_name"] = textViewLastName.text.toString()

            var djPref = listDJ?.get(spinnerDJList?.selectedItemPosition!!)?.id
            params["dj_pref"] = djPref.toString()
            params["event_date"] = textViewDate.text.toString()
            params["event_name"] = textViewEvent.text.toString()
            params["note"] = textViewNote.text.toString()

            if (activity != null) {
                (activity as MainActivity1).showHideProgressbar(true)
            }

            DJBitzApiServe.postRequest(params).enqueue(object : Callback<DJBitzModel.DJBitzReQuest.Result> {

                override fun onResponse(
                    call: Call<DJBitzModel.DJBitzReQuest.Result>,
                    response: Response<DJBitzModel.DJBitzReQuest.Result>
                ) {
                    if (activity != null) {
                        (activity as MainActivity1).showHideProgressbar(false)
                    }
                }

                override fun onFailure(call: Call<DJBitzModel.DJBitzReQuest.Result>, t: Throwable) {
                    if (activity != null) {
                        (activity as MainActivity1).showHideProgressbar(false)
                    }
                }
            })
        })

        if (activity != null) {
            (activity as MainActivity1).showHideProgressbar(true)
        }

        DJBitzApiServe.getDJList()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result ->
                    if (result != null && result.status.toLowerCase().equals(
                            "success"
                        )
                    ) {
                        if (activity != null) {
                            (activity as MainActivity1).showHideProgressbar(false)
                        }

                        showResult(result)
                    }
                    else {
                        //error
                        if (result != null && result.msg != null) {
                            showError(result.msg)
                        }else{
                            showError(null)
                        }
                    }
                },
                { error -> showError(null) }
            )

        return view
    }

    fun signOut(){
        val progressDialog = ProgressDialog(activity)
        progressDialog.setMessage("Please Wait...")
        progressDialog.setCancelable(false)
        updateLiveStatus(progressDialog)

    }

    private fun updateLiveStatus(progressDialog: ProgressDialog) {
        progressDialog.show()
        AndroidNetworking.upload(getString(R.string.LIVE_URL)+"GenerateToken/updateLiveStatus?userId=" + PrefManager.getString(activity, PrefManager.ID) + "&liveStatus=0").setOkHttpClient(OkHttpClient.Builder().connectTimeout(60, TimeUnit.SECONDS).readTimeout(60, TimeUnit.SECONDS).writeTimeout(60, TimeUnit.SECONDS).build()).addMultipartParameter("userId", PrefManager.getString(activity, PrefManager.ID))
            .addMultipartParameter("liveStatus", "0")
            .setPriority(Priority.MEDIUM)
            .build()
            .getAsJSONObject(object : JSONObjectRequestListener {
                override fun onResponse(obj: JSONObject) {
                    val status = obj.optString("status")
                    progressDialog.dismiss()
                    Constants.userInfo = null

                    PreferencesHelper.preferencesHelper.setFirstOpenApp(activity!!, true)
                    PreferencesHelper.preferencesHelper.setEmailLogin(activity!!, "")
                    PreferencesHelper.preferencesHelper.setPasswordLogin(activity!!, "")

                    PrefManager.setString(activity, PrefManager.ID, "")
                    PrefManager.setString(activity, PrefManager.OTHER_ID, "")
                    PrefManager.setString(activity, PrefManager.NMAE, "")
                    PrefManager.setString(activity, PrefManager.LOGIN_NAME_FINAL, "")
                    PrefManager.setString(activity, PrefManager.OTHER_NAME, "")
                    PrefManager.setString(activity, PrefManager.USERTYPE, "")

                    try {
                        clearApplicationData()
                    } catch (e: java.lang.Exception) {
                        e.printStackTrace()
                    }

                    val intent = Intent(activity, LoginActivity::class.java)
                    startActivity(intent)
                    activity!!.finish()
                }

                override fun onError(anError: ANError) {
                    progressDialog.dismiss()
                    try {
                    } catch (e: Exception) {
                        e.printStackTrace()
                        Toast.makeText(activity, "Something Went Wrong", Toast.LENGTH_SHORT).show()
                    }
                }
            })
    }

    fun showError(error: String?) {
        try {
            if (activity != null) {
                (activity as MainActivity1).showHideProgressbar(false)
            }

            var message = error ?: getString(R.string.messager_load_server_error)
            AlertDialog.Builder(activity)
                .setTitle(R.string.title_load_server_error)
                .setMessage(message)
                .setPositiveButton(
                    android.R.string.ok
                ) { dialog, which -> dialog.dismiss() }

                .show()
        } catch (ex: java.lang.Exception) {

        }
    }

    fun showResult(result: DJBitzModel.DJBitzDJList.Result){

        listDJ = result.result.djsRecords

        listDJName.clear()

        result.result.djsRecords.forEach { djInfo->
            listDJName.add(djInfo.name)
        }
// Create an ArrayAdapter
        var adapter = ArrayAdapter(
            context!!,
            R.layout.spinner_item, listDJName
        )
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        // Apply the adapter to the spinner
        spinnerDJList?.adapter = adapter
    }

    fun clearApplicationData() {
        val cache: File = activity!!.getCacheDir()
        val appDir = File(cache.parent)
        if (appDir.exists()) {
            val children = appDir.list()
            for (s in children) {
                if (s != "lib") {
                    deleteDir(File(appDir, s))
                    Log.i(
                        "TAG",
                        "**************** File /data/data/APP_PACKAGE/$s DELETED *******************"
                    )
                }
            }
        }
    }

    fun deleteDir(dir: File?): Boolean {
        if (dir != null && dir.isDirectory) {
            val children = dir.list()
            for (i in children.indices) {
                val success = deleteDir(File(dir, children[i]))
                if (!success) {
                    return false
                }
            }
        }
        return dir!!.delete()
    }


}
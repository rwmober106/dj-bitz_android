package com.bitz

import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import com.bitz.live_video.activities.LiveBoardCastActivity
import com.bitz.retrofit2.DJBitzInterFace
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_live_user.view.*
import okhttp3.OkHttpClient
import org.json.JSONArray
import org.json.JSONObject
import java.util.concurrent.TimeUnit


class LiveUserFragment : Fragment() {

    companion object {

        fun newInstance(): LiveUserFragment {
            return LiveUserFragment()
        }
    }

    val DJBitzApiServe by lazy {
        DJBitzInterFace.create()
    }

    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var adapter: UserListAdapter
    private var songsList = ArrayList<UserList>()
    private lateinit var recyclerView: RecyclerView
    var showToolbar: Boolean = false
    var textTitle: String = ""
    var alertDialog1: AlertDialog? = null
    var textSearch: String = ""
    private lateinit var tvnodata : TextView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var view = inflater?.inflate(R.layout.fragment_live_user, container, false)

        recyclerView = view.findViewById<RecyclerView>(R.id.recyclerView)
        tvnodata = view.findViewById<TextView>(R.id.tvnodata)

        System.out.println("asnmmsnasamnnmsa="+textTitle)

        linearLayoutManager = LinearLayoutManager(context)
        //gridLayoutManager = GridLayoutManager(this, 2)
        recyclerView.layoutManager = linearLayoutManager

        //getAllLiveCustomers()

        view.rlgolive.setOnClickListener {
            PrefManager.setString(activity, PrefManager.FIREBASE_USER_COUNT, "")
            live_popup()


            /*Firebase.setAndroidContext(activity)
            val ref_broadcast_live = Firebase("https://dj-bitz.firebaseio.com/" + "Comments").child("019297a63f7f081d41cb38078f2ad917").child("-MDZYewiG9fqXksXTYat")
            ref_broadcast_live.removeValue()
            Toast.makeText(activity, "please try again!", Toast.LENGTH_SHORT).show()*/
        }

        view.tvgolive.setOnClickListener {
            live_popup()
        }

        System.out.println("asnmmsnasamnnmsa="+PrefManager.getString(activity,PrefManager.USERTYPE))
        if(PrefManager.getString(activity,PrefManager.USERTYPE).equals("2", ignoreCase = true))
        {
            view.rlgolive.setVisibility(View.GONE)
        }
        else
        {
            view.rlgolive.setVisibility(View.VISIBLE)
        }

        var mainActivity = (activity as MainActivity1)
        if (mainActivity.isPlaying) {
            mainActivity.rlPlayer.visibility = View.VISIBLE
        }
        else{
            mainActivity.rlPlayer.visibility = View.GONE
        }


        if (showToolbar) {
            var buttonBack = view.findViewById<ImageButton>(R.id.buttonBack)
            buttonBack.setOnClickListener(View.OnClickListener {
                var libraryFragment = (activity as MainActivity1).supportFragmentManager .findFragmentByTag("LibraryFragment") as LibraryFragment
                if (libraryFragment != null) {
                    libraryFragment.childFragmentManager.popBackStack()
                }
            })

            var buttonSearch = view.findViewById<SearchView>(R.id.buttonSearch)

            buttonSearch.setOnSearchClickListener(View.OnClickListener {
                //search is expanded
                buttonSearch.setBackground(resources.getDrawable(R.drawable.rounded_rectangle_ivorygray_searchview_24dp))
            })

            buttonSearch.setOnCloseListener { buttonSearch.setBackgroundColor(resources.getColor(android.R.color.transparent))
                false }

            val id = buttonSearch.getContext().getResources().getIdentifier("android:id/search_src_text", null, null)
            var editText =  buttonSearch.findViewById<TextView>(id)
            editText.setTextColor(Color.LTGRAY)
            editText.setHintTextColor(Color.LTGRAY)

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val face  = getResources().getFont(R.font.museosanscyrl_100)
                editText.setTypeface(face)

            } else {

            }



            var message = view.findViewById<ImageButton>(R.id.message) as TextView
            message.setText(textTitle)

        }
        else{
            var toolbar = view.findViewById<View>(R.id.toolbar)
            toolbar.visibility = View.GONE
        }

        return view
    }

    fun live_popup()
    {
        val li1 = LayoutInflater.from(activity)
        val promptsView = li1.inflate(R.layout.video_popup, null)
        val alertDialogBuilder1 = AlertDialog.Builder(activity)
        alertDialogBuilder1.setView(promptsView)

        val tv_cancel = promptsView.findViewById<View>(R.id.tv_cancel) as TextView
        val rllive = promptsView.findViewById<View>(R.id.rllive) as RelativeLayout

        rllive.setOnClickListener {
            alertDialog1?.dismiss()
            generate_token();
        }

        tv_cancel.setOnClickListener { alertDialog1?.dismiss() }

        alertDialog1 = alertDialogBuilder1.create()
        alertDialog1?.show()
        alertDialog1?.getWindow()?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        alertDialog1?.setCancelable(false)
    }

    fun generate_token()
    {
        val progressDialog = ProgressDialog(activity)
        progressDialog.setMessage("Please wait...")
        progressDialog.show()
        progressDialog.setCancelable(false)

        System.out.println("check_user_name="+getString(R.string.LIVE_URL)+"GenerateToken/generate_token?userId="+ PrefManager.getString(activity, PrefManager.ID)+"&userName="+PrefManager.getString(activity, PrefManager.LOGIN_NAME_FINAL)+"&channelName="+"")

        AndroidNetworking.post(getString(R.string.LIVE_URL)+"GenerateToken/generate_token?userId="+ PrefManager.getString(activity, PrefManager.ID)+"&userName="+PrefManager.getString(activity, PrefManager.LOGIN_NAME_FINAL)+"&channelName="+"").setOkHttpClient(OkHttpClient.Builder().connectTimeout(60, TimeUnit.SECONDS).readTimeout(60, TimeUnit.SECONDS).writeTimeout(60, TimeUnit.SECONDS).build())
            .setPriority(Priority.HIGH)
            .build()
        /*AndroidNetworking.upload(getString(R.string.LIVE_URL)+"GenerateToken/generate_token?userId="+ PrefManager.getString(activity, PrefManager.ID)+"&userName="+PrefManager.getString(activity, PrefManager.LOGIN_NAME_FINAL)+"&channelName="+"")
            .setOkHttpClient(OkHttpClient.Builder().connectTimeout(60, TimeUnit.SECONDS).readTimeout(60, TimeUnit.SECONDS).writeTimeout(60, TimeUnit.SECONDS).build())
            .addMultipartParameter("userId", PrefManager.getString(activity, PrefManager.ID))
            .addMultipartParameter("userName", PrefManager.getString(activity, PrefManager.LOGIN_NAME_FINAL))
            .addMultipartParameter("channelName", "")
            .setPriority(Priority.HIGH)
            .build()*/
            .getAsJSONObject(object : JSONObjectRequestListener {
                override fun onResponse(obj: JSONObject) {
                    progressDialog.dismiss()
                    println("check_user_name=$obj")
                    val status = obj.optString("status")
                    //System.out.println("check_user_nameLIVE="+status)
                    if(status=="1")
                    {
                        val data_object = obj.optJSONObject("AllData")
                        val userId = data_object.optString("userId").trim()
                        val channelName = data_object.optString("channelName").trim()
                        val TokenWithUid = data_object.optString("TokenWithUid").trim()
                        val TokenWithUserAccount = data_object.optString("TokenWithUserAccount").trim()
                        System.out.println("smamsmsamnasmasas_TokenWithUid="+TokenWithUid)
                        System.out.println("smamsmsamnasmasas_channelName="+channelName)
                        //
                        PrefManager.setString(activity, PrefManager.Connection_USERID, userId)
                        PrefManager.setString(activity, PrefManager.TOKEN, TokenWithUid)
                        PrefManager.setString(activity, PrefManager.CHANNEL, channelName)
                        //
                        val intent = Intent(activity?.getIntent())
                        intent.putExtra(com.bitz.live_video.Constants.KEY_CLIENT_ROLE, 1)
                        intent.setClass(activity!!, LiveBoardCastActivity::class.java)
                        startActivity(intent)
                        //startActivityForResult(intent, 200)
                    }
                    else
                    {
                        Toast.makeText(activity, "please try again!", Toast.LENGTH_SHORT).show()
                    }
                }

                override fun onError(anError: ANError?) {
                    progressDialog.dismiss()
                }
            })
    }

    override fun onStart() {
        super.onStart()
        songsList.clear()
        getAllLiveCustomers()
    }

    fun getAllLiveCustomers()
    {
        val progressDialog = ProgressDialog(activity)
        progressDialog.setMessage("Please wait...")
        progressDialog.show()
        progressDialog.setCancelable(false)

        /*AndroidNetworking.post(getString(R.string.LOCAL_URL)+"dj_bitz/GenerateToken/getAllLiveCustomers").setOkHttpClient(
            OkHttpClient.Builder().connectTimeout(60, TimeUnit.SECONDS).readTimeout(60, TimeUnit.SECONDS).writeTimeout(60, TimeUnit.SECONDS).build())
            //.addMultipartParameter("userId", PrefManager.getString(mContext, PrefManager.ID))
            .setPriority(Priority.HIGH)
            .build()*/
        AndroidNetworking.upload(getString(R.string.LIVE_URL)+"GenerateToken/getAllLiveCustomers")
            .setOkHttpClient(OkHttpClient.Builder().connectTimeout(60, TimeUnit.SECONDS).readTimeout(60, TimeUnit.SECONDS).writeTimeout(60, TimeUnit.SECONDS).build())
            .addMultipartParameter("userId", PrefManager.getString(activity, PrefManager.ID))
            .setPriority(Priority.HIGH)
            .build()
            .getAsJSONObject(object : JSONObjectRequestListener {
                override fun onResponse(obj: JSONObject) {

                    progressDialog.dismiss()
                    //System.out.println("asasasasasas="+PrefManager.getString(activity, PrefManager.ID))
                    System.out.println("asasasasasas="+obj)
                    val status = obj.optString("status")
                    if(status=="1")
                    {
                        val data_object = obj.optString("customersList")

                        val jsonArray = JSONArray(data_object)
                        for (i in 0 until jsonArray.length()) {
                            val `object` = jsonArray.getJSONObject(i)
                            val id = `object`.getString("id")
                            val username = `object`.getString("username")
                            val email = `object`.getString("email")
                            val live_token = `object`.getString("live_token")
                            val channelName = `object`.getString("channelName")
                            val cba = UserList()
                            cba.setUserid(id)
                            cba.setEmail(email)
                            cba.setToken(live_token)
                            cba.setUsername(username)
                            cba.setChannelName(channelName)
                            songsList.add(cba)
                        }
                        if(activity==null)
                            return;
                        activity!!.runOnUiThread(Runnable
                        {
                            adapter = UserListAdapter(songsList, 0, (activity as MainActivity1))
                            recyclerView.adapter = adapter
                            tvnodata.setVisibility(View.GONE)
                        })
                    }
                    else
                    {
                        tvnodata.setVisibility(View.VISIBLE)
                    }
                }

                override fun onError(anError: ANError?) {
                    progressDialog.dismiss()
                }
            })
    }

    /*override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        super.onActivityResult(requestCode, resultCode, data)
        // check if the request code is same as what is passed  here it is 2
        if (requestCode == 2) {
            val message = data.getStringExtra("MESSAGE")
            asas
        }
    }*/

}



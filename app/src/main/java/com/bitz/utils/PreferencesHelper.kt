package com.bitz.utils

import android.app.Activity
import android.preference.PreferenceManager
import java.util.*


class PreferencesHelper {

    companion object {
        val preferencesHelper by lazy {
            PreferencesHelper()
        }
    }

    private val keyFistOpenApp = "fistOpenApp"
    private val keyEmailLogin = "keyEmailLogin"
    private val keyPasswordLogin = "keyPasswordLogin"

    fun getEmailLogin(context: Activity): String{
        var sharedPref = PreferenceManager.getDefaultSharedPreferences(context)

        val email = sharedPref.getString(keyEmailLogin, "")

        return email!!
    }

    fun setEmailLogin(context: Activity, email :String){
        var sharedPref = PreferenceManager.getDefaultSharedPreferences(context)

        val editor = sharedPref.edit()
        editor.putString(keyEmailLogin, email)
        editor.commit()
    }

    fun getPasswordLogin(context: Activity): String{
        var sharedPref = PreferenceManager.getDefaultSharedPreferences(context)

        val email = sharedPref.getString(keyPasswordLogin, "")

        return email!!
    }

    fun setPasswordLogin(context: Activity, password :String){
        var sharedPref = PreferenceManager.getDefaultSharedPreferences(context)

        val editor = sharedPref.edit()
        editor.putString(keyPasswordLogin, password)
        editor.commit()
    }

    fun isFirstOpenApp(context: Activity): Boolean {

        var sharedPref = PreferenceManager.getDefaultSharedPreferences(context) 


        var bFistOpen = sharedPref.getBoolean(keyFistOpenApp, true)

        return bFistOpen
    }

    fun setFirstOpenApp(context: Activity, first: Boolean){
        var sharedPref = PreferenceManager.getDefaultSharedPreferences(context) 

        val editor = sharedPref.edit()
        editor.putBoolean(keyFistOpenApp, false)
        editor.commit()
    }


    fun Int.getFormattedDuration(): String {
        val sb = StringBuilder(8)
        val hours = this / 3600
        val minutes = this % 3600 / 60
        val seconds = this % 60

        if (this >= 3600) {
            sb.append(String.format(Locale.getDefault(), "%02d", hours)).append(":")
        }

        sb.append(String.format(Locale.getDefault(), "%02d", minutes))
        sb.append(":").append(String.format(Locale.getDefault(), "%02d", seconds))
        return sb.toString()
    }
}

package com.bitz

import android.app.AlertDialog
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bitz.retrofit2.DJBitzInterFace
import com.bitz.retrofit2.DJBitzModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


class ArtistFragment : Fragment() {

    companion object {
        fun newInstance(): ArtistFragment {
            return ArtistFragment()
        }
    }

    val DJBitzApiServe by lazy {
        DJBitzInterFace.create()
    }

    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var adapter: DJRecyclerAdapter
    private var djList = ArrayList<DJBitzModel.DJBitzDJList.DJRecords>()
    private lateinit var recyclerView: RecyclerView


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var view = inflater?.inflate(R.layout.fragment_artist, container, false)


        var buttonBack = view.findViewById<ImageButton>(R.id.buttonBack)
        buttonBack.visibility = View.GONE

        var buttonSearch = view.findViewById<SearchView>(R.id.buttonSearch)
        buttonSearch.visibility = View.GONE

        var divide = view.findViewById<View>(R.id.divide)
        divide.visibility = View.VISIBLE

        var message = view.findViewById<ImageButton>(R.id.message) as TextView
        message.setText("DJ PROFILE")

        recyclerView = view.findViewById<RecyclerView>(R.id.recyclerView)


        linearLayoutManager = LinearLayoutManager(context)
        //gridLayoutManager = GridLayoutManager(this, 2)
        recyclerView.layoutManager = linearLayoutManager

        adapter = DJRecyclerAdapter(djList)
        adapter.onItemClick = { song ->
            recyclerView.visibility = View.GONE

            var frame = view.findViewById<FrameLayout>(R.id.frame)
            frame.visibility = View.VISIBLE

            var artistProfile = ArtistProfileFragment.newInstance()
            artistProfile.djProfile = song
            artistProfile.getListMusic(song.id)
            childFragmentManager.beginTransaction().add(R.id.frame, artistProfile, tag).addToBackStack("ArtistProfileFragment").commit()

            buttonBack.visibility = View.VISIBLE
            buttonSearch.visibility = View.VISIBLE
            divide.visibility = View.GONE
            buttonBack.setOnClickListener(View.OnClickListener {
                childFragmentManager.popBackStack()
                buttonBack.setOnClickListener(null);
                buttonSearch.setOnQueryTextListener(null)
                buttonBack.visibility = View.GONE
                buttonSearch.visibility = View.GONE
                divide.visibility = View.VISIBLE

                recyclerView.visibility = View.VISIBLE
                frame.visibility = View.GONE
            })

            buttonSearch.setOnQueryTextListener(artistProfile.searchQueryListener)
            buttonSearch.setOnSearchClickListener(View.OnClickListener {
                //search is expanded
                buttonSearch.setBackground(resources.getDrawable(R.drawable.rounded_rectangle_ivorygray_searchview_24dp))
            })

            buttonSearch.setOnCloseListener { buttonSearch.setBackgroundColor(resources.getColor(android.R.color.transparent))
                false }

            val id = buttonSearch.getContext().getResources().getIdentifier("android:id/search_src_text", null, null)
            var editText =  buttonSearch.findViewById<TextView>(id)
            editText.setTextColor(Color.LTGRAY)
            editText.setHintTextColor(Color.LTGRAY)

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val face  = getResources().getFont(R.font.museosanscyrl_100)
                editText.setTypeface(face)
            }
        }

        recyclerView.adapter = adapter

        if (activity != null) {
            (activity as MainActivity1).showHideProgressbar(true)
        }

        DJBitzApiServe.getDJList()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result ->
                    if (activity != null) {
                        (activity as MainActivity1).showHideProgressbar(false)
                    }

                    if (result != null && result.status.toLowerCase().equals(
                            "success"
                        )
                    ) {
                        showResult(result)
                    }
                    else {
                        //error
                        if (result != null && result.msg != null) {
                            showError(result.msg)
                        }else{
                            showError(null)
                        }
                    }
                },
                { error -> showError(null) }
            )

        return view
    }

    fun showError(error: String?) {
        try {
            if (activity != null) {
                (activity as MainActivity1).showHideProgressbar(false)
            }

            var message = error ?: getString(R.string.messager_load_server_error)
            AlertDialog.Builder(activity)
                .setTitle(R.string.title_load_server_error)
                .setMessage(message)
                .setPositiveButton(
                    android.R.string.ok
                ) { dialog, which -> dialog.dismiss() }

                .show()
        } catch (ex: java.lang.Exception) {

        }
    }

    fun showResult(result: DJBitzModel.DJBitzDJList.Result){
        djList.clear()

        djList.addAll(result.result.djsRecords)
        adapter.notifyDataSetChanged()


    }
}
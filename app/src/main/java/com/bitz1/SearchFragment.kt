package com.bitz1

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AutoCompleteTextView
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bitz1.retrofit2.DJBitzInterFace
import com.bitz1.retrofit2.DJBitzModel
import com.bitz1.utils.Constants
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_search.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList

class SearchFragment : Fragment() {

    companion object {

        fun newInstance(): SearchFragment {
            return SearchFragment()
        }
    }

    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var adapter: RecyclerAdapter
    private var songsList = ArrayList<DJBitzModel.DJBitzMusicList.MusicRecords>()
    private var songsListAll = ArrayList<DJBitzModel.DJBitzMusicList.MusicRecords>()
    private lateinit var recyclerView: RecyclerView
    private var textSearch = ""

    val DJBitzApiServe by lazy {
        DJBitzInterFace.create()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var view = inflater?.inflate(R.layout.fragment_search, container, false)

        recyclerView = view.findViewById<RecyclerView>(R.id.recyclerView)


        linearLayoutManager = LinearLayoutManager(context)
        recyclerView.layoutManager = linearLayoutManager

        adapter = RecyclerAdapter(songsList, 0, (activity as MainActivity1))
        adapter.onItemClick = { song , index ->
            (activity as MainActivity1).currentMusicPlayer = index
            (activity as MainActivity1).listMusicsPlayer = songsList
            (activity as MainActivity1).listMusicsPlayerShuffle = ArrayList<DJBitzModel.DJBitzMusicList.MusicRecords>()
            (activity as MainActivity1).listMusicsPlayerShuffle!!.addAll(songsList)
            val listShuffer = (activity as MainActivity1).listMusicsPlayerShuffle!!.toList().shuffled()
            (activity as MainActivity1).listMusicsPlayerShuffle!!.clear()
            (activity as MainActivity1).listMusicsPlayerShuffle!!.addAll(listShuffer)
           (activity as MainActivity1).bShowCurrentMixFragment = true

//            val sharedPref = activity!!.getPreferences(Context.MODE_PRIVATE)
//            var bShuffle = sharedPref.getBoolean("booleanShuffle", false)
//            if (bShuffle) {
//                (activity as MainActivity).currentMusicPlayer =
//                    (activity as MainActivity).listMusicsPlayerShuffle!!.indexOf(song)
//            }

            var url = Constants.BASE_URL + song.music
            (activity as MainActivity1).playMedia(url)
        }

        var mainActivity = (activity as MainActivity1)
        if (mainActivity.isPlaying) {
            mainActivity.rlPlayer.visibility = View.VISIBLE
        }
        else{
            mainActivity.rlPlayer.visibility = View.GONE
        }

        recyclerView.adapter = adapter

        if (activity != null) {
            (activity as MainActivity1).showHideProgressbar(true)
        }

        val params = HashMap<String, String>()
        params["uid"] = Constants.userInfo?.userInfo?.id ?: ""


        DJBitzApiServe.getMusicList(params).enqueue(object : Callback<DJBitzModel.DJBitzMusicList.Result> {

            override fun onResponse(
                call: Call<DJBitzModel.DJBitzMusicList.Result>,
                result: Response<DJBitzModel.DJBitzMusicList.Result>
            ) {

                    if (result != null && result.body() != null &&  result.body()!!.status.toLowerCase().equals(
                            "success"
                        )
                    ) {
                        if (activity != null) {
                            (activity as MainActivity1).showHideProgressbar(false)
                        }

                        showResult(result.body()!!)
                    }
                    else {
                        //error
                        if (result.body() != null && result.body()!!.msg != null) {
                            showError(result.body()!!.msg)
                        }else{
                            showError(null)
                        }
                    }

            }

            override fun onFailure(call: Call<DJBitzModel.DJBitzMusicList.Result>, t: Throwable) {
                showError(null)
            }
        })


        var editText =  view.findViewById<AutoCompleteTextView>(R.id.edittextSearch)

        if (textSearch.isEmpty()){
            view.buttonClear.visibility = View.GONE
        }
        else{
            view.buttonClear.visibility = View.VISIBLE
        }

        editText.doOnTextChanged { text, start, count, after ->
            textSearch = text.toString()
            if (textSearch.isEmpty()){
                view.buttonClear.visibility = View.GONE
            }
            else{
                view.buttonClear.visibility = View.VISIBLE
            }
            filterSong()
        }

        view.buttonClear.setOnClickListener(View.OnClickListener {

            editText.setText("")

        })
        return view
    }

    fun showError(error: String?) {
        try {
            (activity as MainActivity1).showHideProgressbar(false)
            var message = error ?: getString(R.string.messager_load_server_error)
            AlertDialog.Builder(activity)
                .setTitle(R.string.title_load_server_error)
                .setMessage(message)
                .setPositiveButton(
                    android.R.string.ok
                ) { dialog, which -> dialog.dismiss() }

                .show()
        } catch (ex: java.lang.Exception) {

        }
    }

    fun showResult(result: DJBitzModel.DJBitzMusicList.Result){

        songsListAll.clear()
        result.result.musicRecords.forEach { song ->
            if(!song.music.isEmpty()){
                songsListAll.add(song)
            }
        }
//        songsListAll.addAll(result.result.musicRecords)
        filterSong()

    }

    fun filterSong(){
        songsList.clear()
        if (!textSearch.isEmpty()) {
            songsListAll.forEach { song ->
                if (song.name.toLowerCase().contains(textSearch.toLowerCase())) {
                    songsList.add(song)

                }
            }
        }
//        else{
//            songsList.addAll(songsListAll)
//        }

        adapter.notifyDataSetChanged()
    }
}
package com.bitz1.live_video.activities;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.view.SurfaceView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bitz1.PrefManager;
import com.bitz1.R;
import com.bitz1.live_video.AdapterMsg;
import com.bitz1.live_video.MsgBean;
import com.bitz1.live_video.stats.LocalStatsData;
import com.bitz1.live_video.stats.RemoteStatsData;
import com.bitz1.live_video.stats.StatsData;
import com.bitz1.live_video.ui.VideoGridContainer;
import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import io.agora.rtc.IRtcEngineEventHandler;
import io.agora.rtc.video.VideoEncoderConfiguration;
import okhttp3.OkHttpClient;
import tyrantgit.widget.HeartLayout;

import static android.widget.Toast.makeText;

public class LiveBoardCastActivity extends RtcBaseActivity {
    private static final String TAG = LiveBoardCastActivity.class.getSimpleName();
    int a =0;
    private VideoGridContainer mVideoGridContainer;
    private ImageView mMuteAudioBtn;
    private ImageView mMuteVideoBtn;
    private VideoEncoderConfiguration.VideoDimensions mVideoDimension;
    EditText etcomment;
    ImageView ivsend, ivheart;
    LinearLayout llcomments;
    Firebase  ref_comment, ref_live_count, ref_time, ref_like, ref_broadcast_live;
    long count;
    ScrollView scrollView;
    ImageView ivcloss, ivProfileAvatar;
    TextView tvusernmae, tvminute, tvview;
    private Timer mTimer = new Timer();
    private HeartLayout mHeartLayout;
    ArrayList<String> views=new ArrayList<>();
    ArrayList<String> view_remove=new ArrayList<>();
    ProgressDialog progressDialog;
    List<MsgBean> beanList=new ArrayList<>();
    int total;
    RecyclerView rv_user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_live_boardcast_room);

        tvview = findViewById(R.id.tvview);
        tvminute=findViewById(R.id.tvminute);
        tvusernmae=findViewById(R.id.tvusernmae);
        tvusernmae.setText(PrefManager.getString(this, PrefManager.LOGIN_NAME_FINAL));

        ivProfileAvatar = findViewById(R.id.ivLiveUserAvatar);
        if (PrefManager.PROFILE_AVATAR != null && !PrefManager.PROFILE_AVATAR.isEmpty()) {
            String url = getString(R.string.LIVE_URL) + PrefManager.PROFILE_AVATAR.replace("\\","");

            Picasso.get()
                    .load(url)
                    .placeholder(R.mipmap.ic_avatar)
                    .into(ivProfileAvatar);
        }

        mHeartLayout = (HeartLayout) findViewById(R.id.heart_layout);
        mTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                mHeartLayout.post(new Runnable() {
                    @Override
                    public void run() {
                        mHeartLayout.addHeart(randomColor());
                    }
                });
            }
        }, 1000, 800);

        initUI();
        initData();
    }

    private int randomColor() {
        return Color.parseColor("#8100be");
    }

    private void initUI() {
        TextView roomName = findViewById(R.id.live_room_name);
        //roomName.setText("Photography");
        //roomName.setText("c50fce8328014152e8b82ee86eb4d82f");
        //roomName.setText(config().getChannelName());
        System.out.println("smamsmsamnasmasas_LIVE_CHANNELWithUid="+PrefManager.getString(this,PrefManager.CHANNEL)+"");
        roomName.setText(PrefManager.getString(this,PrefManager.CHANNEL)+"");
        roomName.setSelected(true);
        
        initUserIcon();

        int role = getIntent().getIntExtra(com.bitz1.live_video.Constants.KEY_CLIENT_ROLE,2);
        boolean isBroadcaster =  (role == 1);

        ivcloss = findViewById(R.id.ivcloss);
        llcomments = findViewById(R.id.llcomments);
        ivsend = findViewById(R.id.ivsend);
        ivheart = findViewById(R.id.ivheart);
        etcomment = findViewById(R.id.etcomment);
        mMuteVideoBtn = findViewById(R.id.live_btn_mute_video);
        mMuteVideoBtn.setActivated(isBroadcaster);
        scrollView = (ScrollView)findViewById(R.id.scrollView);
        mMuteAudioBtn = findViewById(R.id.live_btn_mute_audio);
        mMuteAudioBtn.setActivated(isBroadcaster);
        //
        rv_user=(RecyclerView)findViewById(R.id.rv_user);
        RecyclerView.LayoutManager layoutManager=new LinearLayoutManager(LiveBoardCastActivity.this);
        rv_user.setLayoutManager(layoutManager);
        rv_user.setItemAnimator(new DefaultItemAnimator());


        ImageView beautyBtn = findViewById(R.id.live_btn_beautification);
        beautyBtn.setActivated(true);
        rtcEngine().setBeautyEffectOptions(beautyBtn.isActivated(), com.bitz1.live_video.Constants.DEFAULT_BEAUTY_OPTIONS);

        mVideoGridContainer = findViewById(R.id.live_video_grid_layout);
        mVideoGridContainer.setStatsManager(statsManager());

        progressDialog = new ProgressDialog(LiveBoardCastActivity.this);
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);

        rtcEngine().setClientRole(role);
        if (isBroadcaster) startBroadcast();

        Firebase.setAndroidContext(this);
        try
        {
            ref_comment = new Firebase("https://dj-bitz.firebaseio.com/" + "Comments").child(PrefManager.getString(this,PrefManager.CHANNEL));
            ref_live_count = new Firebase("https://dj-bitz.firebaseio.com/" + "LiveCount").child(PrefManager.getString(this,PrefManager.CHANNEL));
            ref_time = new Firebase("https://dj-bitz.firebaseio.com/" + "Time").child(PrefManager.getString(this,PrefManager.CHANNEL));
            ref_like = new Firebase("https://dj-bitz.firebaseio.com/" + "Like").child(PrefManager.getString(this,PrefManager.CHANNEL));
            ref_broadcast_live = new Firebase("https://dj-bitz.firebaseio.com/" + "BLive").child(PrefManager.getString(this,PrefManager.CHANNEL));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        ivcloss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                ref_comment.removeValue();
                ref_live_count.removeValue();
                ref_time.removeValue();
                ref_like.removeValue();
                updateLiveStatus();
                broad_user_offline();
            }
        });

        System.out.println("sasasasasasasasasasaidSave="+PrefManager.getString(this,PrefManager.CHANNEL));

        //user onlinr
        broad_user_online();

        ivsend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String messageText = etcomment.getText().toString().trim();
                if(!messageText.equals(""))
                {
                    Map<String,Object> map = new HashMap<>();
                    map.put("comment", messageText);
                    map.put("username", PrefManager.getString(LiveBoardCastActivity.this, PrefManager.LOGIN_NAME_FINAL));
                    map.put("user_id", PrefManager.getString(LiveBoardCastActivity.this, PrefManager.ID));
                    map.put("isLike", false);
                    map.put("profile_avatar", PrefManager.getString(LiveBoardCastActivity.this, PrefManager.PROFILE_AVATAR));
                    ref_comment.push().setValue(map);
                    etcomment.setText("");
                    scrollView.fullScroll(View.FOCUS_DOWN);
                }
            }
        });

        Timer t=new Timer();
        t.scheduleAtFixedRate(new TimerTask()
        {
            @Override
            public void run()
            {
                runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        a = a + 1;
                        Map<String, String> map = new HashMap<String, String>();
                        map.put("time", a+" min");
                        ref_time.setValue(map);
                    }
                });

            }
        },0,60000);

        Timer t1=new Timer();
        t1.scheduleAtFixedRate(new TimerTask()
        {
            @Override
            public void run()
            {
                runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        mHeartLayout.setVisibility(View.GONE);
                    }
                });

            }
        },0,10000);


        ivheart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Map<String, Object> map = new HashMap<String, Object>();
                map.put("username",PrefManager.getString(LiveBoardCastActivity.this, PrefManager.LOGIN_NAME_FINAL));
                map.put("like","1");
                map.put("user_id",PrefManager.getString(LiveBoardCastActivity.this, PrefManager.ID));
                map.put("profile_avatar", PrefManager.getString(LiveBoardCastActivity.this, PrefManager.PROFILE_AVATAR));
                ref_like.push().setValue(map);
                mHeartLayout.setVisibility(View.VISIBLE);

                Map<String, Object> map1 = new HashMap<String, Object>();
                map1.put("user_id", PrefManager.getString(LiveBoardCastActivity.this, PrefManager.OTHER_ID));
                map1.put("comment", "");
                map1.put("isLike", true);
                map1.put("username", PrefManager.getString(LiveBoardCastActivity.this, PrefManager.LOGIN_NAME_FINAL));
                map.put("profile_avatar", PrefManager.getString(LiveBoardCastActivity.this, PrefManager.PROFILE_AVATAR));
                ref_comment.push().setValue(map1);
                etcomment.setText("");
                scrollView.fullScroll(View.FOCUS_DOWN);
            }
        });

        try
        {
            ref_comment.addChildEventListener(new ChildEventListener()
            {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s)
                {
                    final String idSave = PrefManager.getString(LiveBoardCastActivity.this,PrefManager.FIREBASE_USER_COUNT);
                    System.out.println("sasasasasasasasasasa_onChildAdded="+idSave);

                    Map map = dataSnapshot.getValue(Map.class);
                    String message =map.get("comment").toString().trim();
                    String audience_name =map.get("username").toString().trim();
                    Boolean isLike = (Boolean) dataSnapshot.child("isLike").getValue();
                    String profile_avatar = map.get("profile_avatar").toString().trim();
                    addMessageBox(message, audience_name,isLike, profile_avatar);
                }
                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s)
                { }
                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot)
                { }
                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s)
                { }
                @Override
                public void onCancelled(FirebaseError firebaseError)
                { }
            });

            ref_live_count.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    int size = (int) dataSnapshot.getChildrenCount();
                    tvview.setText(size+"");
                    System.out.println("sasasasasasasasasasa_onDataChange=" + size);
                }

                @Override
                public void onCancelled(FirebaseError firebaseError) {

                }
            });

            ref_live_count.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    //views.clear();
                    System.out.println("sasasasasasasasasasa_onChildAdded=" + dataSnapshot.getKey()+"<>"+dataSnapshot.getChildrenCount());
                    for (DataSnapshot ds : dataSnapshot.getChildren()) {
                        int number = ds.getValue(Integer.class);
                        views.add(number + "");
                    }
                    //System.out.println("sasasasasasasasasasa_onChildAdded=" + views.size());
                    //tvview.setText(views.size() + "");
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                    System.out.println("sasasasasasasasasasa_onChildChanged=" + dataSnapshot.getKey());
                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {
                    System.out.println("sasasasasasasasasasa_onChildRemoved=" + dataSnapshot.getKey());
                    view_remove.clear();
                    for (DataSnapshot ds : dataSnapshot.getChildren()) {
                        int number = ds.getValue(Integer.class);
                        view_remove.add(number + "");
                    }
                    //System.out.println("sasasasasasasasasasa_onChildRemoved=" + view_remove.size());
                    //System.out.println("sasasasasasasasasasa_onChildRemoved=" + view_remove.size());
                    int view = views.size();
                    int remove = view_remove.size();
                    total = view - remove;
                    //System.out.println("sasasasasasasasasasa_onChildRemoved=" + total);
                    //tvview.setText(total + "");
                    if (total == 0) {
                        views.clear();
                        view_remove.clear();
                        ref_live_count.removeValue();
                        ref_comment.removeValue();
                        ref_like.removeValue();
                    }
                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                    System.out.println("sasasasasasasasasasa_onChildMoved=" + dataSnapshot.getKey());
                    /*views.clear();
                    System.out.println("sasasasasasasasasasa_onChildMoved="+dataSnapshot.getKey());
                    for(DataSnapshot ds : dataSnapshot.getChildren()) {
                        //int number = ds.getValue(Integer.class);
                        String number = ds.getKey();
                        views.add(number+"");
                    }
                    view_count = views.size();
                    tvview.setText(views.size()+"");*/
                }

                @Override
                public void onCancelled(FirebaseError firebaseError) {
                }
            });

            ref_time.addChildEventListener(new ChildEventListener()
            {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s)
                {
                    //System.out.println("asasassaas="+dataSnapshot+"<>"+s);
                    //System.out.println("asasassaas="+dataSnapshot.getKey()+"<>"+s);
                    System.out.println("asasassaas="+dataSnapshot.getValue()+"<>"+s);
                    //User user1 = dataSnapshot.getRef();

                    /*Map map = dataSnapshot.getValue(Map.class);
                    String time =map.get("time").toString().trim();
                    tvminute.setText(time);*/
                    tvminute.setText(dataSnapshot.getValue()+"");
                }
                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s)
                {
                    System.out.println("asasassaas_OCC="+dataSnapshot.getValue()+"<>"+s);
                    tvminute.setText(dataSnapshot.getValue()+"");
                }
                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot)
                {
                }
                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s)
                {
                }
                @Override
                public void onCancelled(FirebaseError firebaseError)
                {
                }
            });

            ref_like.addChildEventListener(new ChildEventListener()
            {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s)
                {
                    Map map = dataSnapshot.getValue(Map.class);
                    String like =map.get("like").toString().trim();
                    if(like.equalsIgnoreCase("1"))
                    {
                        mHeartLayout.setVisibility(View.VISIBLE);
                    }
                }
                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s)
                {
                    Map map = dataSnapshot.getValue(Map.class);
                    String like =map.get("like").toString().trim();
                    if(like.equalsIgnoreCase("1"))
                    {
                        mHeartLayout.setVisibility(View.VISIBLE);
                    }
                }
                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot)
                {
                }
                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s)
                {
                }
                @Override
                public void onCancelled(FirebaseError firebaseError)
                {
                }
            });
        }
        catch (Exception e)
        {
            e.printStackTrace();
            System.out.println("message_message="+e.getMessage());
        }
    }

    public void addMessageBox(String message,String audience_name,Boolean isLike, String profile_avatar)
    {
        MsgBean bean2=new MsgBean();
        bean2.setAudience_name(audience_name);
        bean2.setLike(isLike);
        bean2.setMessage(message);
        bean2.setProfile_avatar(profile_avatar);
        beanList.add(bean2);

        final AdapterMsg adapterMsg=new AdapterMsg(beanList, LiveBoardCastActivity.this);
        rv_user.setAdapter(adapterMsg);

        rv_user.scrollToPosition(beanList.size() - 1);
    }

    private void initUserIcon() {
        Bitmap origin = BitmapFactory.decodeResource(getResources(), R.drawable.fake_user_icon);
        RoundedBitmapDrawable drawable = RoundedBitmapDrawableFactory.create(getResources(), origin);
        drawable.setCircular(true);
        ImageView iconView = findViewById(R.id.live_name_board_icon);
        iconView.setImageDrawable(drawable);
    }

    private void initData() {
        mVideoDimension = com.bitz1.live_video.Constants.VIDEO_DIMENSIONS[config().getVideoDimenIndex()];
    }

    @Override
    protected void onGlobalLayoutCompleted() {
    }

    private void startBroadcast() {
        rtcEngine().setClientRole(1);
        SurfaceView surface = prepareRtcVideo(0, true);
        mVideoGridContainer.addUserVideoSurface(0, surface, true);
        mMuteAudioBtn.setActivated(true);
    }

    private void stopBroadcast() {
        rtcEngine().setClientRole(2);
        removeRtcVideo(0, true);
        mVideoGridContainer.removeUserVideo(0, true);
        mMuteAudioBtn.setActivated(false);
    }

    @Override
    public void onJoinChannelSuccess(String channel, int uid, int elapsed) {
        // Do nothing at the moment
    }

    @Override
    public void onUserJoined(int uid, int elapsed) {
        // Do nothing at the moment
    }

    @Override
    public void onUserOffline(final int uid, int reason) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                removeRemoteUser(uid);
            }
        });
    }

    @Override
    public void onFirstRemoteVideoDecoded(final int uid, int width, int height, int elapsed) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                renderRemoteUser(uid);
            }
        });
    }

    private void renderRemoteUser(int uid) {
        SurfaceView surface = prepareRtcVideo(uid, false);
        mVideoGridContainer.addUserVideoSurface(uid, surface, false);
    }

    private void removeRemoteUser(int uid) {
        removeRtcVideo(uid, false);
        mVideoGridContainer.removeUserVideo(uid, false);
    }

    @Override
    public void onLocalVideoStats(IRtcEngineEventHandler.LocalVideoStats stats) {
        if (!statsManager().isEnabled()) return;

        LocalStatsData data = (LocalStatsData) statsManager().getStatsData(0);
        if (data == null) return;

        data.setWidth(mVideoDimension.width);
        data.setHeight(mVideoDimension.height);
        data.setFramerate(stats.sentFrameRate);
    }

    @Override
    public void onRtcStats(IRtcEngineEventHandler.RtcStats stats) {
        if (!statsManager().isEnabled()) return;

        LocalStatsData data = (LocalStatsData) statsManager().getStatsData(0);
        if (data == null) return;

        data.setLastMileDelay(stats.lastmileDelay);
        data.setVideoSendBitrate(stats.txVideoKBitRate);
        data.setVideoRecvBitrate(stats.rxVideoKBitRate);
        data.setAudioSendBitrate(stats.txAudioKBitRate);
        data.setAudioRecvBitrate(stats.rxAudioKBitRate);
        data.setCpuApp(stats.cpuAppUsage);
        data.setCpuTotal(stats.cpuAppUsage);
        data.setSendLoss(stats.txPacketLossRate);
        data.setRecvLoss(stats.rxPacketLossRate);
    }

    @Override
    public void onNetworkQuality(int uid, int txQuality, int rxQuality) {
        if (!statsManager().isEnabled()) return;

        StatsData data = statsManager().getStatsData(uid);
        if (data == null) return;

        data.setSendQuality(statsManager().qualityToString(txQuality));
        data.setRecvQuality(statsManager().qualityToString(rxQuality));
    }

    @Override
    public void onRemoteVideoStats(IRtcEngineEventHandler.RemoteVideoStats stats) {
        if (!statsManager().isEnabled()) return;

        RemoteStatsData data = (RemoteStatsData) statsManager().getStatsData(stats.uid);
        if (data == null) return;

        data.setWidth(stats.width);
        data.setHeight(stats.height);
        data.setFramerate(stats.rendererOutputFrameRate);
        data.setVideoDelay(stats.delay);
    }

    @Override
    public void onRemoteAudioStats(IRtcEngineEventHandler.RemoteAudioStats stats) {
        if (!statsManager().isEnabled()) return;

        RemoteStatsData data = (RemoteStatsData) statsManager().getStatsData(stats.uid);
        if (data == null) return;

        data.setAudioNetDelay(stats.networkTransportDelay);
        data.setAudioNetJitter(stats.jitterBufferDelay);
        data.setAudioLoss(stats.audioLossRate);
        data.setAudioQuality(statsManager().qualityToString(stats.quality));
    }

    @Override
    public void finish() {
        super.finish();
        ref_comment.removeValue();
        ref_live_count.removeValue();
        ref_time.removeValue();
        ref_like.removeValue();
        statsManager().clearAllData();
        updateLiveStatus();
        broad_user_offline();
    }

    public void onLeaveClicked(View view) {
        finish();
    }

    public void onSwitchCameraClicked(View view) {
        rtcEngine().switchCamera();
    }

    public void onBeautyClicked(View view) {
        view.setActivated(!view.isActivated());
        rtcEngine().setBeautyEffectOptions(view.isActivated(), com.bitz1.live_video.Constants.DEFAULT_BEAUTY_OPTIONS);
    }

    public void onMoreClicked(View view) {
        // Do nothing at the moment
    }

    public void onPushStreamClicked(View view) {
        // Do nothing at the moment
    }

    public void onMuteAudioClicked(View view) {
        if (!mMuteVideoBtn.isActivated()) return;

        rtcEngine().muteLocalAudioStream(view.isActivated());
        view.setActivated(!view.isActivated());
    }

    public void onMuteVideoClicked(View view) {
        if (view.isActivated()) {
            stopBroadcast();
        } else {
            startBroadcast();
        }
        view.setActivated(!view.isActivated());
    }

    private void updateLiveStatus() {

        /*final ProgressDialog progressDialog1 = new ProgressDialog(LiveBoardCastActivity.this);
        progressDialog1.setMessage("Please Wait...");
        progressDialog1.setCancelable(false);
        progressDialog1.show();*/
        //finish();
        AndroidNetworking.upload(getString(R.string.LIVE_URL)+"GenerateToken/updateLiveStatus?userId=" + PrefManager.getString(LiveBoardCastActivity.this, PrefManager.ID) + "&liveStatus=0").setOkHttpClient(new OkHttpClient.Builder().connectTimeout(60, TimeUnit.SECONDS).readTimeout(60, TimeUnit.SECONDS).writeTimeout(60, TimeUnit.SECONDS).build())
                .addMultipartParameter("userId", PrefManager.getString(LiveBoardCastActivity.this, PrefManager.ID))
                .addMultipartParameter("liveStatus", "0")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject obj) {
                        System.out.println("check_user_name="+obj);
                        String status = obj.optString("status");
                         //gourav error
                        if(status.equalsIgnoreCase("1"))
                        {
                            finish();
                        }
                        else
                        {
                            makeText(LiveBoardCastActivity.this, "try again.", Toast.LENGTH_SHORT).show();
                        }
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                //progressDialog1.dismiss();
                            }
                        });
                    }

                    @Override
                    public void onError(ANError anError) {
                        //progressDialog1.dismiss();
                        try {

                        } catch (Exception e) {
                            e.printStackTrace();
                            //ProjectUtils.showLog("MAKEPOST", "Exception = " + e.getMessage());
                            makeText(LiveBoardCastActivity.this, "Something Went Wrong", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    @Override
    protected void onResume() {
        super.onResume();
        System.out.println("sasasasasasasasas=onResume");

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        System.out.println("sasasasasasasasas=onDestroy");
        ref_comment.removeValue();
        ref_live_count.removeValue();
        ref_time.removeValue();
        ref_like.removeValue();
        updateLiveStatus();
        broad_user_offline();
    }

    /*@Override
    protected void onPause() {
        super.onPause();
        System.out.println("sasasasasasasasas=onPause");
        ref_comment.removeValue();
        ref_live_count.removeValue();
        ref_time.removeValue();
        ref_like.removeValue();
        updateLiveStatus();
        broad_user_offline();
    }*/

    /*@Override
    protected void onStop() {
        super.onStop();
        System.out.println("sasasasasasasasas=onStop");
        ref_comment.removeValue();
        ref_live_count.removeValue();
        ref_time.removeValue();
        ref_like.removeValue();
        updateLiveStatus();
        broad_user_offline();
    }*/

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        System.out.println("sasasasasasasasas=onBackPressed");
        finish();
        ref_comment.removeValue();
        ref_live_count.removeValue();
        ref_time.removeValue();
        ref_like.removeValue();
        statsManager().clearAllData();
        updateLiveStatus();
        broad_user_offline();
    }

    public void broad_user_offline()
    {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("username",PrefManager.getString(LiveBoardCastActivity.this, PrefManager.LOGIN_NAME_FINAL));
        map.put("blive",false);
        map.put("user_id",PrefManager.getString(LiveBoardCastActivity.this, PrefManager.ID));
        ref_broadcast_live.push().setValue(map);
    }

    public void broad_user_online()
    {
        /*Map<String, Object> map = new HashMap<String, Object>();
        map.put("username",PrefManager.getString(LiveBoardCastActivity.this, PrefManager.LOGIN_NAME_FINAL));
        map.put("blive",true);
        map.put("user_id",PrefManager.getString(LiveBoardCastActivity.this, PrefManager.ID));
        ref_broadcast_live.push().setValue(map);*/
    }
}

package com.bitz1.live_video;

public class MsgBean
{
    String message;
    String audience_name;
    String profile_avatar;

    public Boolean getLike() {
        return isLike;
    }

    public void setLike(Boolean like) {
        isLike = like;
    }

    Boolean isLike;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getAudience_name() {
        return audience_name;
    }

    public void setAudience_name(String audience_name) {
        this.audience_name = audience_name;
    }

    public String getProfile_avatar() {
        return profile_avatar;
    }

    public void setProfile_avatar(String profile_avatar) {
        this.profile_avatar = profile_avatar;
    }
}

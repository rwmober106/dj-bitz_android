package com.bitz1.live_video.utils;

import android.content.Context;
import android.content.SharedPreferences;
import com.bitz1.live_video.Constants;


public class PrefManager {
    public static SharedPreferences getPreferences(Context context) {
        return context.getSharedPreferences(Constants.PREF_NAME, Context.MODE_PRIVATE);
    }
}

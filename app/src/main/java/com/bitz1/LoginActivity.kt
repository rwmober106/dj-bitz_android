package com.bitz1

import android.app.AlertDialog
import android.content.Intent
import android.graphics.Paint
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.bitz1.retrofit2.DJBitzInterFace
import com.bitz1.retrofit2.DJBitzModel
import com.bitz1.utils.Constants
import com.bitz1.utils.PreferencesHelper
import kotlinx.android.synthetic.main.activity_login.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class LoginActivity : AppCompatActivity() {

    val DJBitzApiServe by lazy {
        DJBitzInterFace.create()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        btnLogin.setOnClickListener {
            postLogin()
        }

        textViewTitle.setPaintFlags(textViewTitle.getPaintFlags() or Paint.UNDERLINE_TEXT_FLAG)
        forgotPassword.setPaintFlags(forgotPassword.getPaintFlags() or Paint.UNDERLINE_TEXT_FLAG)
        signUp.setPaintFlags(signUp.getPaintFlags() or Paint.UNDERLINE_TEXT_FLAG)

        signUp.setOnClickListener {
            val intent = Intent(this, RegistationActivity::class.java)
            startActivity(intent)
            finish()
        }

        forgotPassword.setOnClickListener {
            val intent = Intent(this, ForgotPasswordActivity::class.java)
            startActivity(intent)
        }
    }

    fun postLogin(){
        try {
            val params = HashMap<String, String>()

            params["email"] = editEmail.text.toString()
            params["password"] = editPassword.text.toString()
            params["FCM_token"] = PrefManager.getString(this,PrefManager.FIREBAE_LOGIN_TOKEN)
            showHideProgressbar(true)
            DJBitzApiServe.postLogin(params).enqueue(object : Callback<DJBitzModel.DJBitzUserInfo.Result> {

                override fun onResponse(call: Call<DJBitzModel.DJBitzUserInfo.Result>, response: Response<DJBitzModel.DJBitzUserInfo.Result>) {
                    showHideProgressbar(false)
                    //System.out.println("asnmmsnasamnnmsa="+response.body()!!.userInfo.isDjs)
                    if (response.isSuccessful && response.body() != null && response.body()!!.status.toLowerCase().equals("success")){
                        try {
                            PreferencesHelper.preferencesHelper.setFirstOpenApp(this@LoginActivity, false)
                            PreferencesHelper.preferencesHelper.setEmailLogin(this@LoginActivity, editEmail.text.toString())
                            PreferencesHelper.preferencesHelper.setPasswordLogin(this@LoginActivity, editPassword.text.toString())
                            Constants.userInfo = response.body()

                            System.out.println("check_user_name="+response.body()!!.userInfo.username)

                            PrefManager.setString(this@LoginActivity, PrefManager.ID, response.body()!!.userInfo.id)
                            PrefManager.setString(this@LoginActivity, PrefManager.OTHER_ID, response.body()!!.userInfo.id)
                            PrefManager.setString(this@LoginActivity, PrefManager.NMAE, response.body()!!.userInfo.username)
                            PrefManager.setString(this@LoginActivity, PrefManager.LOGIN_NAME_FINAL, response.body()!!.userInfo.username)
                            PrefManager.setString(this@LoginActivity, PrefManager.OTHER_NAME, response.body()!!.userInfo.username)
                            PrefManager.setString(this@LoginActivity, PrefManager.USERTYPE, response.body()!!.userInfo.isDjs)

                            if (response.body()!!.userInfo.profile_avatar != null && !response.body()!!.userInfo.profile_avatar.isEmpty()) {
                                PrefManager.setString(this@LoginActivity, PrefManager.PROFILE_AVATAR, response.body()!!.userInfo.profile_avatar)
                            } else {
                                PrefManager.setString(this@LoginActivity, PrefManager.PROFILE_AVATAR, "")
                            }


                            val intent = Intent(this@LoginActivity, MainActivity1::class.java)
                            startActivity(intent)
                            finish()
                        }catch (ex: Exception){
                            ex.printStackTrace()
                            showError(null)
                        }
                    }
                    else{
                        //error
                        if (response.body()!= null) {
                            showError(response.body()!!.msg)
                        }else{
                            showError(null)
                        }
                    }
                }
                override fun onFailure(call: Call<DJBitzModel.DJBitzUserInfo.Result>, t: Throwable) {
                    System.out.println("check_user_name="+t.message)
                    showError(null)
                }
            })
        }catch (e: Exception){
            e.stackTrace
        }
    }

    fun showError(error: String?){
        try {
            showHideProgressbar(false)
            var message = error?: getString(R.string.messager_load_server_error)
            AlertDialog.Builder(this)
                .setTitle(R.string.title_load_server_error)
                .setMessage(message)
                .setPositiveButton(
                    android.R.string.ok
                ) { dialog, which -> dialog.dismiss() }

                .show()
        }catch (ex: java.lang.Exception){

        }
    }

    fun showHideProgressbar(show: Boolean){
        if (show){
            progressIndicator.visibility = View.VISIBLE
        }
        else{
            progressIndicator.visibility = View.GONE
        }
    }
}

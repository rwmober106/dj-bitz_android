package com.bitz1

import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.SearchView
import android.widget.TextView
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bitz1.retrofit2.DJBitzInterFace
import com.bitz1.retrofit2.DJBitzModel
import com.bitz1.utils.Constants
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_artistprofile.view.*
import kotlinx.android.synthetic.main.row_song.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ArtistProfileFragment : Fragment() {

    companion object {

        fun newInstance(): ArtistProfileFragment {
            return ArtistProfileFragment()
        }
    }


    val DJBitzApiServe by lazy {
        DJBitzInterFace.create()
    }

    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var adapter: ArtistAdapter
    private var songsList = ArrayList<DJBitzModel.DJBitzMusicList.MusicRecords>()
    private lateinit var recyclerView: RecyclerView
    private lateinit var beatAvatar: ImageView
    private lateinit var beatBackground: ImageView
    private  lateinit var description: TextView

    var djProfile: DJBitzModel.DJBitzDJList.DJRecords? = null
    var countsSongs: TextView? = null
    private var songsListAll = ArrayList<DJBitzModel.DJBitzMusicList.MusicRecords>()
    var textSearch: String = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var view = inflater?.inflate(R.layout.fragment_artistprofile, container, false)

        recyclerView = view.findViewById<RecyclerView>(R.id.recyclerView)
        beatAvatar = view.findViewById<ImageButton>(R.id.beat_avatar)
        beatBackground = view.findViewById<ImageButton>(R.id.beatBackground)
        description = view.findViewById<TextView>(R.id.tvDescription)
        description.text = djProfile?.description

        // Get first name
        var name = djProfile!!.name
        val index = name.indexOf(" ")
        if (index > 0){
            name = name.substring(0, index - 1)
        }

        view.fistName.text = "DJ " +  name
        view.lastName.text = djProfile?.name
        view.listerning.text = "0 listerning"
        view.listerning.visibility = View.GONE
        countsSongs = view.findViewById<TextView>(R.id.countsSongs)

        linearLayoutManager = LinearLayoutManager(context)

        recyclerView.layoutManager = linearLayoutManager

        var mainActivity = (activity as MainActivity1)
        if (mainActivity.isPlaying) {
            mainActivity.rlPlayer.visibility = View.VISIBLE
        }
        else{
            mainActivity.rlPlayer.visibility = View.GONE
        }

        adapter = ArtistAdapter(songsList)
        adapter.onItemClick = { song , index->


            (activity as MainActivity1).listMusicsPlayer = songsList

            (activity as MainActivity1).listMusicsPlayerShuffle = ArrayList<DJBitzModel.DJBitzMusicList.MusicRecords>()
            (activity as MainActivity1).listMusicsPlayerShuffle!!.addAll(songsList)
            val listShuffer = (activity as MainActivity1).listMusicsPlayerShuffle!!.toList().shuffled()
            (activity as MainActivity1).listMusicsPlayerShuffle!!.clear()
            (activity as MainActivity1).listMusicsPlayerShuffle!!.addAll(listShuffer)

            (activity as MainActivity1).bShowCurrentMixFragment = true


            (activity as MainActivity1).currentMusicPlayer = index

//            val sharedPref = activity!!.getPreferences(Context.MODE_PRIVATE)
//            var bShuffle = sharedPref.getBoolean("booleanShuffle", false)
//            if (bShuffle) {
//                (activity as MainActivity).currentMusicPlayer =
//                    (activity as MainActivity).listMusicsPlayerShuffle!!.indexOf(song)
//            }

            var url = Constants.BASE_URL + song.music
            (activity as MainActivity1).playMedia(url)
        }

        recyclerView.adapter = adapter


        if (djProfile?.profile_cover != null) {
            val profile_cover = djProfile!!.profile_cover

            if (!profile_cover.isEmpty()) {
                var url = Constants.BASE_URL + profile_cover.replace("\\", "")

                Picasso.get()
                    .load(url)
                    //.placeholder(R.mipmap.samplemixbg)
                    .into(beatAvatar)
            }
        }

        return view
    }

    fun getListMusic(id: String){
        if (activity != null) {
            (activity as MainActivity1).showHideProgressbar(true)
        }

        val params = java.util.HashMap<String, String>()
        params["djId"] = id
        params["uid"] = Constants.userInfo?.userInfo?.id ?: ""

        DJBitzApiServe.getMusicListWithDJ(params).enqueue(object : Callback<DJBitzModel.DJBitzMusicList.Result> {

            override fun onResponse(
                call: Call<DJBitzModel.DJBitzMusicList.Result>,
                result: Response<DJBitzModel.DJBitzMusicList.Result>
            ) {

                if (result != null && result.body() != null &&  result.body()!!.status.toLowerCase().equals(
                        "success"
                    )
                ) {
                    if (activity != null) {
                        (activity as MainActivity1).showHideProgressbar(false)
                    }

                    showResult(result.body()!!)
                }
                else {
                    //error
                    if (result.body() != null && result.body()!!.msg != null) {
                        showError(result.body()!!.msg)
                    }else{
                        showError(null)
                    }
                }

            }

            override fun onFailure(call: Call<DJBitzModel.DJBitzMusicList.Result>, t: Throwable) {
                showError(null)
            }
        })

    }

    fun showError(error: String?) {
        try {
            if (activity != null) {
                (activity as MainActivity1).showHideProgressbar(false)
            }

            var message = error ?: getString(R.string.messager_load_server_error)
            AlertDialog.Builder(activity)
                .setTitle(R.string.title_load_server_error)
                .setMessage(message)
                .setPositiveButton(
                    android.R.string.ok
                ) { dialog, which -> dialog.dismiss() }

                .show()
        } catch (ex: java.lang.Exception) {

        }
    }

    val searchQueryListener = object : SearchView.OnQueryTextListener {
        override fun onQueryTextSubmit(query: String): Boolean {

            return false
        }

        override fun onQueryTextChange(newText: String): Boolean {

            textSearch = newText

            filterSong()
            return true
        }

    }


    fun showResult(result: DJBitzModel.DJBitzMusicList.Result){
        songsListAll.clear()
        result.result.musicRecords.forEach { song ->
            if(!song.music.isEmpty()){
                songsListAll.add(song)
            }
        }
//        songsListAll.addAll(result.result.musicRecords)
        filterSong()

    }

    fun filterSong(){
        songsList.clear()
        if (!textSearch.isEmpty()) {
            songsListAll.forEach { song ->
                if (song.name.toLowerCase().contains(textSearch.toLowerCase())) {
                    songsList.add(song)

                }
            }
        }
        else{
            songsList.addAll(songsListAll)
        }

        if (songsListAll.size > 1) {
            countsSongs?.text = String.format("%d Mixes", songsListAll.size)
        }
        else{
            countsSongs?.text = String.format("%d Mix", songsListAll.size)
        }

        adapter.notifyDataSetChanged()
    }



   inner class ArtistAdapter(private val songs: ArrayList<DJBitzModel.DJBitzMusicList.MusicRecords>, private val viewStyle: Int? = 0) : RecyclerView.Adapter<ArtistAdapter.SongRowHolder>() {
        var onItemClick: (((DJBitzModel.DJBitzMusicList.MusicRecords), adapterPosition: Int) -> Unit)? = null

        override fun getItemCount() = songs.size

        override fun onBindViewHolder(holder: ArtistAdapter.SongRowHolder, position: Int) {
            val song = songs[position]
            holder.bind(song)  }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArtistAdapter.SongRowHolder {

            var inflatedView = parent.inflate(R.layout.row_song, false)

            return SongRowHolder(inflatedView)
        }

        inner class SongRowHolder(v: View) : RecyclerView.ViewHolder(v), View.OnClickListener {
            private var view: View = v
            private var song: DJBitzModel.DJBitzMusicList.MusicRecords? = null

            init {
                v.setOnClickListener(this)
            }

            override fun onClick(v: View) {
                onItemClick?.invoke(songs[adapterPosition], adapterPosition)

            }

            fun totalTime(totalTimes: String):String{
                var timesResult = ""
                try {
                    val split = totalTimes.split(":")




                    if (!split.get(0).equals("00")){
                        if (split.get(0).substring(0,1).equals("0")){
                            timesResult += split.get(0).substring(1,2) + ":"
                        }
                        else {
                            timesResult += split.get(0) + ":"
                        }
                    }

                    timesResult += split.get(1) + ":"
                    timesResult += split.get(2)

                }catch (e: Exception){
                    e.stackTrace
                }

                return timesResult
            }

            fun bind(song: DJBitzModel.DJBitzMusicList.MusicRecords) {
                if (viewStyle == 0) {
                    this.song = song
                    //Picasso.with(view.context).load(photo.url).into(view.itemImage)
                    view.textViewTitle.text = song.DJ
                    view.textViewTitle.visibility = View.GONE
                    view.textViewDescription.text = song.name
                    if (song.duration != null) {
                        view.textViewTime.text = totalTime(song.duration)
                    }
                    else{
                        view.textViewTime.text = ""
                    }

                    if (song.thumb != null && !song.thumb.isEmpty()) {
                        var url = Constants.BASE_URL + song.thumb.replace("\\","")

                        Picasso.get()
                            .load(url)
                            .placeholder(R.mipmap.images)
                            .into(view.imageView)
                    }


                    //test

                    if(!song.comment_count.isNullOrEmpty()) {
                        view.countComment.text = song.comment_count
                    }
                    else{
                        view.countComment.text = "0"
                    }

                    if(!song.likes.isNullOrEmpty()) {
                        view.countFavourite.text = song.likes
                    }
                    else{
                        view.countFavourite.text = "0"
                    }

                    if(!song.playCounts.isNullOrEmpty()) {
                        view.countListened.text = song.playCounts
                    }
                    else{
                        view.countListened.text = "0"
                    }

                    if (song.is_liked.equals("0")){
                        view.imgFavourite.setImageResource(R.mipmap.ic_favourite) // transparent
                    }
                    else{
                        view.imgFavourite.setImageResource(R.mipmap.ic_favourite_filled) // White Tint
                    }

                    view.imgFavourite.setOnClickListener {

                        if (song.is_liked.equals("0")) {
                            try {
                                val params = HashMap<String, String>()
                                params["email"] = Constants.userInfo?.userInfo?.email ?: ""
                                params["music_id"] = song.id

                                if (activity != null) {
                                    (activity as MainActivity1).showHideProgressbar(true)
                                }

                                DJBitzApiServe.postLike(params).enqueue(object : Callback<DJBitzModel.ResponseBody.Result> {

                                    override fun onResponse(
                                        call: Call<DJBitzModel.ResponseBody.Result>,
                                        response: Response<DJBitzModel.ResponseBody.Result>
                                    ) {
                                        if (activity != null) {
                                            (activity as MainActivity1).showHideProgressbar(false)
                                        }

                                        if (response.isSuccessful && response.body() != null && response.body()!!.status.toLowerCase().equals(
                                                "success"
                                            )
                                        ) {
                                            songs.get(adapterPosition).likes =
                                                (songs.get(adapterPosition).likes.toInt() + 1).toString()

                                            songs.get(adapterPosition).is_liked = "1"
                                            notifyItemChanged(adapterPosition)
                                        }
                                        else {
                                            //error
                                            if (response.body() != null && response.body()!!.msg != null) {
                                                showError(response.body()!!.msg)
                                            }else{
                                                showError(null)
                                            }
                                        }
                                    }

                                    override fun onFailure(call: Call<DJBitzModel.ResponseBody.Result>, t: Throwable) {
                                        showError(null)
                                    }
                                })
                            } catch (e: Exception) {
                                e.stackTrace
                                showError(null)
                            }
                        } else {
                            try {
                                val params = HashMap<String, String>()
                                params["email"] = Constants.userInfo?.userInfo?.email ?: ""
                                params["music_id"] = song.id

                                if (activity != null) {
                                    (activity as MainActivity1).showHideProgressbar(true)
                                }

                                DJBitzApiServe.postDisLike(params)
                                    .enqueue(object : Callback<DJBitzModel.ResponseBody.Result> {

                                        override fun onResponse(
                                            call: Call<DJBitzModel.ResponseBody.Result>,
                                            response: Response<DJBitzModel.ResponseBody.Result>
                                        ) {
                                            if (activity != null) {
                                                (activity as MainActivity1).showHideProgressbar(false)
                                            }

                                            if (response.isSuccessful && response.body() != null && response.body()!!.status.toLowerCase().equals(
                                                    "success"
                                                )
                                            ) {
                                                songs.get(adapterPosition).likes =
                                                    (songs.get(adapterPosition).likes.toInt() - 1).toString()

                                                songs.get(adapterPosition).is_liked = "0"
                                                notifyItemChanged(adapterPosition)
                                            }
                                            else {
                                                //error
                                                if (response.body()!= null) {
                                                    showError(response.body()!!.msg)
                                                }else{
                                                    showError(null)
                                                }
                                            }
                                        }

                                        override fun onFailure(call: Call<DJBitzModel.ResponseBody.Result>, t: Throwable) {
                                            showError(null)
                                        }
                                    })
                            } catch (e: Exception) {
                                e.stackTrace
                            }
                        }
                    }

                    ////

                    if (!song.comment.isNullOrEmpty()){
                        view.line3.visibility = View.VISIBLE
                    }
                    else{
                        view.line3.visibility = View.GONE
                    }

                    view.imgComment.setOnClickListener {

                        if (view.line3.visibility == View.GONE){
                            view.line3.visibility = View.VISIBLE
                        }
                        else{
                            view.line3.visibility = View.GONE
                        }
                    }

                    view.btnSendComment.setOnClickListener {
                        view.btnSendComment.hideKeyboard()
                        try {
                            val params = HashMap<String, String>()
                            params["email"] = Constants.userInfo?.userInfo?.email?: ""
                            params["music_id"] = song.id
                            params["comment"] = songs.get(adapterPosition).comment

                            if (activity != null) {
                                (activity as MainActivity1).showHideProgressbar(true)
                            }

                            DJBitzApiServe.postAddComment(params).enqueue(object : Callback<DJBitzModel.ResponseBody.Result> {

                                override fun onResponse(
                                    call: Call<DJBitzModel.ResponseBody.Result>,
                                    response: Response<DJBitzModel.ResponseBody.Result>
                                ) {
                                    if (activity != null) {
                                        (activity as MainActivity1).showHideProgressbar(false)
                                    }

                                    if (response.isSuccessful && response.body() != null && response.body()!!.status.toLowerCase().equals("success")){
                                        view.editComment.setText("")
                                        songs.get(adapterPosition).comment = ""
                                        view.line3.visibility = View.GONE
                                        if (djProfile != null) {
                                            getListMusic(djProfile!!.id)
                                        }
                                    }
                                    else {
                                        //error
                                        if (response.body()!= null) {
                                            showError(response.body()!!.msg)
                                        }else{
                                            showError(null)
                                        }
                                    }
                                }

                                override fun onFailure(call: Call<DJBitzModel.ResponseBody.Result>, t: Throwable) {
                                    showError(null)
                                }
                            })
                        } catch (e: Exception) {
                            e.stackTrace
                            showError(null)
                        }
                    }

                    view.editComment.addTextChangedListener {
                            text ->
                        songs.get(adapterPosition).comment = text.toString()
                    }

                    view.editComment.setText(song.comment)
                }
            }
        }
    }


    fun View.hideKeyboard() {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(windowToken, 0)
    }
}
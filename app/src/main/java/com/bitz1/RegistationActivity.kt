package com.bitz1

import android.app.AlertDialog
import android.content.Intent
import android.graphics.Paint
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.bitz1.retrofit2.DJBitzInterFace
import com.bitz1.retrofit2.DJBitzModel
import com.bitz1.utils.Constants
import com.bitz1.utils.PreferencesHelper
import kotlinx.android.synthetic.main.activity_registation.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class RegistationActivity : AppCompatActivity() {

    var value_selectd: String = "2"

    val DJBitzApiServe by lazy {
        DJBitzInterFace.create()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registation)


        btnRegistation.setOnClickListener {
            postRegistation()
        }


        textViewTitle.setPaintFlags(textViewTitle.getPaintFlags() or Paint.UNDERLINE_TEXT_FLAG)
        login.setPaintFlags(login.getPaintFlags() or Paint.UNDERLINE_TEXT_FLAG)

        login.setOnClickListener({
            val intent = Intent(this@RegistationActivity, LoginActivity::class.java)
            startActivity(intent)
            finish()
        })

        chdj.setOnClickListener(View.OnClickListener {
            if (chdj.isChecked())
            {
                value_selectd="1";
            }
            else
            {
                value_selectd="2";
            }
        })
    }

    fun postRegistation(){
        try {

            if (editPassword.text.toString().equals(editComfirmPassword.text.toString())) {
                val params = HashMap<String, String>()
                params["username"] = editName.text.toString()
                params["email"] = editEmail.text.toString()
                params["password"] = editPassword.text.toString()
                params["isDjs"] = value_selectd
//            params["comfirm_password"] = editComfirmPassword.text.toString()
                showHideProgressbar(true)
                DJBitzApiServe.postRegistation(params).enqueue(object : Callback<DJBitzModel.ResponseBody.Result> {

                    override fun onResponse(call: Call<DJBitzModel.ResponseBody.Result>, response: Response<DJBitzModel.ResponseBody.Result>)
                    {
                        showHideProgressbar(false)
                        if (response.isSuccessful && response.body() != null && response.body()!!.status.toLowerCase().equals("success")){
                            System.out.println("saasaasaassasa="+response.body())
                            postLogin()
                        }
                        else{
                            //error
                            if (response.body()!= null) {
                                showError(response.body()!!.msg)
                            }else{
                                showError(null)
                            }
                        }
                    }

                    override fun onFailure(call: Call<DJBitzModel.ResponseBody.Result>, t: Throwable) {
                        showError(null)
                    }
                })
            }
            else{

            }
        }catch (e: Exception){
            e.stackTrace
        }
    }

    fun postLogin(){
        try {
            // 1->for DJs Customer 2-> Normal User not Djs // required Field
            val params = HashMap<String, String>()
//            params["name"] = editName.text.toString()
            params["email"] = editEmail.text.toString()
            params["password"] = editPassword.text.toString()
            params["FCM_token"] = PrefManager.getString(this,PrefManager.TOKEN)
            showHideProgressbar(true)
            DJBitzApiServe.postLogin(params).enqueue(object : Callback<DJBitzModel.DJBitzUserInfo.Result> {

                override fun onResponse(call: Call<DJBitzModel.DJBitzUserInfo.Result>, response: Response<DJBitzModel.DJBitzUserInfo.Result>)
                {
                    showHideProgressbar(false)
                    if (response.isSuccessful && response.body() != null && response.body()!!.status.toLowerCase().equals("success")){
                        try {
                            PreferencesHelper.preferencesHelper.setFirstOpenApp(this@RegistationActivity, false)
                            PreferencesHelper.preferencesHelper.setEmailLogin(this@RegistationActivity, editEmail.text.toString())
                            PreferencesHelper.preferencesHelper.setPasswordLogin(this@RegistationActivity, editPassword.text.toString())
                            Constants.userInfo = response.body()

                            PrefManager.setString(this@RegistationActivity, PrefManager.ID, response.body()!!.userInfo.id)
                            PrefManager.setString(this@RegistationActivity, PrefManager.OTHER_ID, response.body()!!.userInfo.id)
                            PrefManager.setString(this@RegistationActivity, PrefManager.NMAE, response.body()!!.userInfo.username)
                            PrefManager.setString(this@RegistationActivity, PrefManager.LOGIN_NAME_FINAL, response.body()!!.userInfo.username)
                            PrefManager.setString(this@RegistationActivity, PrefManager.OTHER_NAME, response.body()!!.userInfo.username)
                            PrefManager.setString(this@RegistationActivity, PrefManager.USERTYPE, response.body()!!.userInfo.isDjs)

                            val intent = Intent(this@RegistationActivity, MainActivity1::class.java)
                            startActivity(intent)
                            finish()
                        }catch (ex: Exception){
                            ex.printStackTrace()
                            showError(null)
                        }

                    }
                    else{
                        //error
                        if (response.body()!= null) {
                            showError(response.body()!!.msg)
                        }else{
                            showError(null)
                        }
                    }
                }

                override fun onFailure(call: Call<DJBitzModel.DJBitzUserInfo.Result>, t: Throwable) {
                    showError(null)
                }
            })
        }catch (e: Exception){
            e.stackTrace
        }
    }

    fun showError(error: String?){
        try {
            showHideProgressbar(false)
            var message = error?: getString(R.string.messager_load_server_error)
            AlertDialog.Builder(this)
                .setTitle(R.string.title_load_server_error)
                .setMessage(message)
                .setPositiveButton(
                    android.R.string.ok
                ) { dialog, which -> dialog.dismiss() }

                .show()
        }catch (ex: java.lang.Exception){

        }
    }

    fun showHideProgressbar(show: Boolean){
        if (show){
            progressIndicator.visibility = View.VISIBLE
        }
        else{
            progressIndicator.visibility = View.GONE
        }
    }
}

package com.bitz1.extensions

import android.content.Context
import android.content.Intent
import android.util.TypedValue
import com.bitz1.*
import com.bitz1.helpers.Config
import com.bitz1.utils.CALL_SETUP_AFTER
import com.bitz1.utils.PAUSE
import com.bitz1.utils.PREFS_KEY
import com.bitz1.utils.REFRESH_LIST

fun Context.getSharedPrefs() = getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE)

fun Context.sendIntent(action: String) {
    Intent(this, MusicService::class.java).apply {
        this.action = action
        try {
            startService(this)
        } catch (ignored: Exception) {
        }
    }
}

val Context.config: Config get() = Config.newInstance(applicationContext)

//val Context.playlistDAO: PlaylistsDao get() = getSongsDB().PlaylistsDao()

//val Context.songsDAO: SongsDao get() = getSongsDB().SongsDao()

fun Context.playlistChanged(newID: Int, callSetup: Boolean = true) {
    config.currentPlaylist = newID
    sendIntent(PAUSE)
    Intent(this, MusicService::class.java).apply {
        putExtra(CALL_SETUP_AFTER, callSetup)
        action = REFRESH_LIST
        startService(this)
    }
}

fun Context.getActionBarHeight(): Int {
    val textSizeAttr = intArrayOf(R.attr.actionBarSize)
    val attrs = obtainStyledAttributes(TypedValue().data, textSizeAttr)
    val actionBarSize = attrs.getDimensionPixelSize(0, -1)
    attrs.recycle()
    return actionBarSize
}

//fun Context.getSongsDB() = SongsDatabase.getInstance(this)

//fun Context.getPlaylistIdWithTitle(title: String) = playlistDAO.getPlaylistWithTitle(title)?.id ?: -1
//
//fun Context.getPlaylistSongs(playlistId: Int): ArrayList<Song> {
//    val validSongs = ArrayList<Song>()
//    val invalidSongs = ArrayList<Song>()
//    val songs = songsDAO.getSongsFromPlaylist(playlistId)
//    songs.forEach {
//        if (File(it.path).exists()) {
//            validSongs.add(it)
//        } else {
//            invalidSongs.add(it)
//        }
//    }
//
//    getSongsDB().runInTransaction {
//        invalidSongs.forEach {
//            songsDAO.removeSongPath(it.path)
//        }
//    }
//
//    return validSongs
//}
//
//fun Context.deletePlaylists(playlists: ArrayList<Playlist>) {
//    playlistDAO.deletePlaylists(playlists)
//    playlists.forEach {
//        songsDAO.removePlaylistSongs(it.id)
//    }
//}

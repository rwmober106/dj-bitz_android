package com.bitz1

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.activity_main.*

class LibraryFragment : Fragment() {

    companion object {

        fun newInstance(): LibraryFragment {
            return LibraryFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var root = inflater?.inflate(R.layout.fragment_library, container, false)

        displayFragment(LibrarySongGenresFragment.newInstance(), "LibrarySongGenresFragment", false)
        var mainActivity = (activity as MainActivity1)
        if (mainActivity.isPlaying) {
            mainActivity.rlPlayer.visibility = View.VISIBLE
        }
        else{
            mainActivity.rlPlayer.visibility = View.GONE
        }

        return root
    }

    fun displayFragment(fragment: Fragment, tag: String, addToBackStack: Boolean){
        if (addToBackStack) {
            childFragmentManager.beginTransaction().add(R.id.frame, fragment, tag).addToBackStack(tag).commit()
        }
        else {
            childFragmentManager.beginTransaction().replace(R.id.frame, fragment, tag).commit()
        }
    }
}
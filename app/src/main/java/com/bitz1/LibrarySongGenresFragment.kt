package com.bitz1

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager.widget.ViewPager

class LibrarySongGenresFragment : Fragment() {

    companion object {

        fun newInstance(): LibrarySongGenresFragment {
            return LibrarySongGenresFragment()
        }
    }


    private lateinit var viewPager: ViewPager
    private lateinit var pagerAdapter: ViewPagerAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var root = inflater?.inflate(R.layout.fragment_library_songs_genres, container, false)

        viewPager = root.findViewById<ViewPager>(R.id.viewPager)
        pagerAdapter = ViewPagerAdapter(fragmentManager)
        viewPager.adapter = pagerAdapter

        var buttonRecents = root.findViewById<Button>(R.id.buttonRecents)
        var buttonGenres = root.findViewById<Button>(R.id.buttonGenres)
        var buttonLive = root.findViewById<Button>(R.id.buttonLive)
        var buttonTopTen = root.findViewById<Button>(R.id.buttonTopTen)

        buttonRecents.setOnClickListener(View.OnClickListener {
            viewPager.setCurrentItem(0, false)
            buttonRecents.setTextColor(resources.getColor(R.color.colorPurple))
            buttonGenres.setTextColor(resources.getColor(android.R.color.darker_gray))
            buttonLive.setTextColor(resources.getColor(android.R.color.darker_gray))
            buttonTopTen.setTextColor(resources.getColor(android.R.color.darker_gray))
        })

        buttonTopTen.setOnClickListener(View.OnClickListener {
            viewPager.setCurrentItem(1, false)
            buttonTopTen.setTextColor(resources.getColor(R.color.colorPurple))
            buttonRecents.setTextColor(resources.getColor(android.R.color.darker_gray))
            buttonGenres.setTextColor(resources.getColor(android.R.color.darker_gray))
            buttonLive.setTextColor(resources.getColor(android.R.color.darker_gray))
        })

        buttonGenres.setOnClickListener(View.OnClickListener {
            viewPager.setCurrentItem(2, false)
            buttonGenres.setTextColor(resources.getColor(R.color.colorPurple))
            buttonRecents.setTextColor(resources.getColor(android.R.color.darker_gray))
            buttonLive.setTextColor(resources.getColor(android.R.color.darker_gray))
            buttonTopTen.setTextColor(resources.getColor(android.R.color.darker_gray))
        })

        buttonLive.setOnClickListener(View.OnClickListener {
            viewPager.adapter = pagerAdapter
            viewPager.setCurrentItem(3, false)
            buttonLive.setTextColor(resources.getColor(R.color.colorPurple))
            buttonRecents.setTextColor(resources.getColor(android.R.color.darker_gray))
            buttonGenres.setTextColor(resources.getColor(android.R.color.darker_gray))
            buttonTopTen.setTextColor(resources.getColor(android.R.color.darker_gray))
            //live_popup();
        })

        return root
    }


    class ViewPagerAdapter(fragmentManager: FragmentManager?) :
        FragmentStatePagerAdapter(fragmentManager) {

        // 2
        override fun getItem(position: Int): Fragment {
            if (position == 0){
                var fragment = LibrarySongsFragment.newInstance()
                fragment.showToolbar = false
                fragment.getListMusic()
                fragment.textTitle = ""
                return fragment
            }
            else if (position == 1){
                var fragment = LibrarySongsFragment.newInstance()
                fragment.showToolbar = false
                fragment.getListMusicTopten()
                fragment.textTitle = ""
                return fragment
            }
            else if (position == 3){
                var fragment = LiveUserFragment.newInstance()
                fragment.showToolbar = false
                //fragment.getAllLiveCustomers()
                fragment.textTitle = "g"
                return fragment
            }

            return LibraryGenresFragment.newInstance()
        }

        // 3
        override fun getCount(): Int {
            return 4
        }
    }
}
package com.bitz1

import android.app.Activity.RESULT_OK
import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.ImageDecoder
import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.util.Base64
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat.checkSelfPermission
import androidx.fragment.app.Fragment
import com.bitz1.retrofit2.DJBitzInterFace
import com.bitz1.retrofit2.DJBitzModel
import com.bitz1.utils.Constants
import com.bitz1.utils.PreferencesHelper
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.fragment_profile.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.ByteArrayOutputStream
import java.io.File


class ProfileFragment : Fragment() {

    companion object {
        fun newInstance(): ProfileFragment {
            return ProfileFragment()
        }
        //image pick code
        private val IMAGE_PICK_CODE = 1000
        //camera code
        private val IMAGE_CAPTURE_CODE = 1001
        //Permission code
        private val PERMISSION_CODE = 1002
    }

    val DJBitzApiServe by lazy {
        DJBitzInterFace.create()
    }

    var value_selected: String = ""
    var base64string: String = ""
    var base64string1: String = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view = inflater?.inflate(R.layout.fragment_profile, container, false)


        var profileImageView = view.findViewById<ImageView>(R.id.ivProfile)
        var fullnameTextView = view.findViewById<EditText>(R.id.tvFullname)
        var saveButton = view.findViewById<Button>(R.id.btnSave)
        var deleteButton = view.findViewById<Button>(R.id.btnDelete)
        var djCB = view.findViewById<CheckBox>(R.id.cbDj)


        fullnameTextView.setText(PrefManager.getString(this.context, PrefManager.NMAE))

        if (Constants.userInfo?.userInfo?.isDjs!! == "1") {
            djCB.isChecked = true
            value_selected = "1"
        } else {
            djCB.isChecked = false
            value_selected = "2"
        }


        var profile_avatar = Constants.userInfo?.userInfo?.profile_avatar ?: ""
        if (profile_avatar!= null && !profile_avatar.isEmpty()) {
            var url = Constants.BASE_URL + profile_avatar.replace("\\","")

            Picasso.get()
                .load(url)
                .placeholder(R.mipmap.ic_avatar)
                .into(profileImageView)

            Picasso.get()
                .load(url)
                .into(object : Target {
                    override fun onPrepareLoad(placeHolderDrawable: Drawable?) {

                    }
                    override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                        base64string1 = encodeImage(bitmap!!)!!
                    }

                    override fun onBitmapFailed(e: java.lang.Exception?, errorDrawable: Drawable?) {

                    }
                })
        }



        profileImageView.setOnClickListener(View.OnClickListener {

            //check runtime permission
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                if (checkSelfPermission(this.context!!, android.Manifest.permission.READ_EXTERNAL_STORAGE) ==
                    PackageManager.PERMISSION_DENIED){
                    //permission denied
                    val permissions = arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE);
                    //show popup to request runtime permission
                    requestPermissions(permissions, PERMISSION_CODE);
                }
                else{
                    //permission already granted
                    selectImage()
                }
            }
            else{
                //system OS is < Marshmallow
                selectImage()
            }

        })

        saveButton.setOnClickListener(View.OnClickListener {


            val params = HashMap<String, String>()
            params["username"] = fullnameTextView.text.toString()
            params["email"] = Constants.userInfo?.userInfo?.email!!
            if (base64string.isEmpty()) {
                params["base64string"] = base64string1
            } else {
                params["base64string"] = base64string
            }

            params["isDjs"] = value_selected

            if (activity != null) {
                (activity as MainActivity1).showHideProgressbar(true)
            }
            DJBitzApiServe.postEditProfile(params).enqueue(object :
                Callback<DJBitzModel.DJBitzUserInfo.Result> {

                override fun onResponse(call: Call<DJBitzModel.DJBitzUserInfo.Result>, response: Response<DJBitzModel.DJBitzUserInfo.Result>) {
                    if (activity != null) {
                        (activity as MainActivity1).showHideProgressbar(false)
                    }
                    //System.out.println("asnmmsnasamnnmsa="+response.body()!!.userInfo.isDjs)
                    if (response.isSuccessful && response.body() != null && response.body()!!.status.toLowerCase().equals("success")){
                        try {

                            Constants.userInfo = response.body()

                            System.out.println("check_user_name="+response.body()!!.userInfo.username)

                            PrefManager.setString(activity, PrefManager.ID, response.body()!!.userInfo.id)
                            PrefManager.setString(activity, PrefManager.OTHER_ID, response.body()!!.userInfo.id)
                            PrefManager.setString(activity, PrefManager.NMAE, response.body()!!.userInfo.username)
                            PrefManager.setString(activity, PrefManager.LOGIN_NAME_FINAL, response.body()!!.userInfo.username)
                            PrefManager.setString(activity, PrefManager.OTHER_NAME, response.body()!!.userInfo.username)
                            PrefManager.setString(activity, PrefManager.USERTYPE, response.body()!!.userInfo.isDjs)

                            if (response.body()!!.userInfo.profile_avatar != null && !response.body()!!.userInfo.profile_avatar.isEmpty()) {
                                PrefManager.setString(activity, PrefManager.PROFILE_AVATAR, response.body()!!.userInfo.profile_avatar)
                            } else {
                                PrefManager.setString(activity, PrefManager.PROFILE_AVATAR, "")
                            }

                            var ll = PrefManager.PROFILE_AVATAR
                            System.out.println("check_user_name="+response.body()!!.userInfo.username)
                        }catch (ex: Exception){
                            ex.printStackTrace()
                            showError(null)
                        }
                    }
                    else{
                        //error
                        if (response.body()!= null) {
                            showError(response.body()!!.msg)
                        }else{
                            showError(null)
                        }
                    }
                }
                override fun onFailure(call: Call<DJBitzModel.DJBitzUserInfo.Result>, t: Throwable) {
                    System.out.println("check_user_name="+t.message)
                    showError(null)
                }
            })


        })

        deleteButton.setOnClickListener(View.OnClickListener {

            val params = HashMap<String, String>()
            params["email"] = Constants.userInfo?.userInfo?.email!!

            if (activity != null) {
                (activity as MainActivity1).showHideProgressbar(true)
            }
            DJBitzApiServe.postDeleteAccount(params).enqueue(object :
                Callback<DJBitzModel.ResponseBody.Result> {

                override fun onResponse(call: Call<DJBitzModel.ResponseBody.Result>, response: Response<DJBitzModel.ResponseBody.Result>) {
                    if (activity != null) {
                        (activity as MainActivity1).showHideProgressbar(false)
                    }
                    if (response.isSuccessful && response.body() != null && response.body()!!.status.toLowerCase().equals("success")){
                        try {

                            Constants.userInfo = null

                            PreferencesHelper.preferencesHelper.setFirstOpenApp(activity!!, true)
                            PreferencesHelper.preferencesHelper.setEmailLogin(activity!!, "")
                            PreferencesHelper.preferencesHelper.setPasswordLogin(activity!!, "")

                            PrefManager.setString(activity, PrefManager.ID, "")
                            PrefManager.setString(activity, PrefManager.OTHER_ID, "")
                            PrefManager.setString(activity, PrefManager.NMAE, "")
                            PrefManager.setString(activity, PrefManager.LOGIN_NAME_FINAL, "")
                            PrefManager.setString(activity, PrefManager.OTHER_NAME, "")
                            PrefManager.setString(activity, PrefManager.USERTYPE, "")

                            try {
                                clearApplicationData()
                            } catch (e: java.lang.Exception) {
                                e.printStackTrace()
                            }

                            val intent = Intent(activity, LoginActivity::class.java)
                            startActivity(intent)
                            activity!!.finish()

                        }catch (ex: Exception){
                            ex.printStackTrace()
                            showError(null)
                        }
                    }
                    else{
                        //error
                        if (response.body()!= null) {
                            showError(response.body()!!.msg)
                        }else{
                            showError(null)
                        }
                    }
                }
                override fun onFailure(call: Call<DJBitzModel.ResponseBody.Result>, t: Throwable) {
                    System.out.println("check_user_name="+t.message)
                    showError(null)
                }
            })

        })

        djCB.setOnClickListener(View.OnClickListener {
            if (djCB.isChecked())
            {
                value_selected="1"
            }
            else
            {
                value_selected="2"
            }
        })

        return view
    }

    private fun selectImage() {
        val items = arrayOf("Camera", "Photo Album", "Cancel")
        val builder = AlertDialog.Builder(this.context)
        with(builder){
            setTitle("Image Selection")
//            setMessage("From where you want to pick this image?")
            setItems(items){dialog, which ->
                val item = items[which]
                if (item.equals("Camera")) {
                    val intent =
                        Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                    startActivityForResult(intent, IMAGE_CAPTURE_CODE)
                } else if (item.equals("Photo Album")) {
                    val intent = Intent(
                        Intent.ACTION_PICK,
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                    )
                    intent.type = "image/*"
                    startActivityForResult(intent, IMAGE_PICK_CODE)
                } else {
                    dialog.dismiss()
                }
            }
            show()
        }
    }

    fun clearApplicationData() {
        val cache: File = activity!!.getCacheDir()
        val appDir = File(cache.parent)
        if (appDir.exists()) {
            val children = appDir.list()
            for (s in children) {
                if (s != "lib") {
                    deleteDir(File(appDir, s))
                    Log.i(
                        "TAG",
                        "**************** File /data/data/APP_PACKAGE/$s DELETED *******************"
                    )
                }
            }
        }
    }

    fun deleteDir(dir: File?): Boolean {
        if (dir != null && dir.isDirectory) {
            val children = dir.list()
            for (i in children.indices) {
                val success = deleteDir(File(dir, children[i]))
                if (!success) {
                    return false
                }
            }
        }
        return dir!!.delete()
    }

    private fun encodeImage(bm: Bitmap): String? {
        val baos = ByteArrayOutputStream()
        bm.compress(Bitmap.CompressFormat.JPEG, 10, baos)
        val b: ByteArray = baos.toByteArray()
        return Base64.encodeToString(b, Base64.DEFAULT)
    }

    fun showError(error: String?){
        try {
            showHideProgressbar(false)
            var message = error?: getString(R.string.messager_load_server_error)
            AlertDialog.Builder(this.context)
                .setTitle(R.string.title_load_server_error)
                .setMessage(message)
                .setPositiveButton(
                    android.R.string.ok
                ) { dialog, which -> dialog.dismiss() }

                .show()
        }catch (ex: java.lang.Exception){

        }
    }

    fun showHideProgressbar(show: Boolean){
        if (show){
            progressIndicator.visibility = View.VISIBLE
        }
        else{
            progressIndicator.visibility = View.GONE
        }
    }
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when(requestCode){
            PERMISSION_CODE -> {
                if (grantResults.size >0 && grantResults[0] ==
                    PackageManager.PERMISSION_GRANTED){
                    //permission from popup granted
                    selectImage()
                }
                else{
                    //permission from popup denied
                    Toast.makeText(this.context!!, "Permission denied", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.P)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK) {
            if (requestCode == IMAGE_CAPTURE_CODE) {
                val bitmap = data?.extras?.get("data") as Bitmap
                base64string = encodeImage(bitmap)!!
                ivProfile.setImageBitmap(bitmap)
            }
            else if (requestCode == IMAGE_PICK_CODE) {
                val uri = data?.getData()
                val source: ImageDecoder.Source =
                    ImageDecoder.createSource(
                        this.context!!.getContentResolver(),
                        uri!!
                    )
                val bitmap =
                    ImageDecoder.decodeBitmap(source)
                base64string = encodeImage(bitmap)!!
                ivProfile.setImageURI(uri)
            }
        }
    }

}

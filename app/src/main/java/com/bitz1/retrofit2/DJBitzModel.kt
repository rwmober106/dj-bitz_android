package com.bitz1.retrofit2

import java.io.Serializable

class DJBitzModel {

    object DJBitzGenresList{
        data class Result(val status: String,
                          val msg: String,
                          val result: ResultItem)

        data class ResultItem(val searchText: String,
                              val genresRecords: List<GenresRecords>)

        data class GenresRecords(val id: String,
                                 val name: String,
                                 val thumb_img: String)
    }

    object DJBitzDJList{
        data class Result(val status: String,
                          val msg: String,
                          val result: ResultItem)

        data class ResultItem(val searchText: String,
                              val djsRecords: List<DJRecords>)

        data class DJRecords(val id: String,
                             val name: String,
                             val avatar_url: String,
                             val profile_cover: String,
                             val email: String,
                             val mobile: String,
                             val description: String)
    }

    object DJBitzMusicList{
        data class Result(val status: String,
                          val msg: String,
                          val result: ResultItem)

        data class ResultItem(val searchText: String,
                              val musicRecords: List<MusicRecords>)

        data class MusicRecords(val id: String,
                                val name: String,
                                val description: String,
                                val thumb: String,
                                val music: String,
                                val genre: String,
                                val artist: String,
                                val createdBy: String,
                                val updatedBy: String,
                                val isDeleted: String,
                                val created_date: String,
                                val updated_date: String,
                                val duration: String,
                                val dj: String,
                                val DJ: String,
                                var likes: String,
                                var is_liked: String,
                                var playCounts: String,
                                var play_count: PlayCount,
                                var comment_count: String,
                                var comment: String):Serializable

        data class PlayCount(var count: String):Serializable

        data class Comment(
            val id: String = "",
            val username: String = "",
            val user_id: String = "",
            val music_id: String = "",
            val comment: String = "",
            val is_deleted: String = "",
            val created_at: String = "",
            val updated_at: String = "",
            var type: Int = 1,
            val profile_avatar: String = ""
        )

    }

    object ResponseBody{
        data class Result(val status: String,
                          val msg: String)
    }

    object DJBitzUserInfo{
        data class Result(val status: String,
                          val msg: String,
                          val userInfo: UserInfo)


        data class UserInfo(val id: String,
                                val username: String,
                                val email: String,
                                val password: String,
                                val isDeleted: String,
                                val isDjs: String,
                                val created_at: String,
                                val updated_at: String,
                                val profile_avatar: String)
    }

    object DJBitzTopMusicList{
        data class Result(val status: String,
                          val result: List<DJBitzMusicList.MusicRecords>)

    }

    object DJBitzMusicInfo{
        data class Result(val status: String,
                          val result: List<DJBitzMusicList.MusicRecords>,
                          var comments: List<DJBitzMusicList.Comment>)




    }

    object DJBitzReQuest{
        data class Result(val status: String,
                          val msg: String)
    }

    object DJBitzMyInfo{
        data class Info(val name: String,
                          val email: String)
    }


}
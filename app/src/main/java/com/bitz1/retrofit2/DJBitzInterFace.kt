package com.bitz1.retrofit2

import com.bitz1.utils.Constants
import io.reactivex.Observable
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import retrofit2.http.FieldMap



interface DJBitzInterFace{

    companion object {
        fun create(): DJBitzInterFace {

            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(
                    RxJava2CallAdapterFactory.create())
                .addConverterFactory(
                    GsonConverterFactory.create())
                .baseUrl(Constants.BASE_URL)
                .build()
            return retrofit.create(DJBitzInterFace::class.java)
        }
    }

    @GET("index.php/getgenreslist")
    fun getGenresList(): Observable<DJBitzModel.DJBitzGenresList.Result>

    @GET("index.php/getdjslist")
    fun getDJList(): Observable<DJBitzModel.DJBitzDJList.Result>

    @POST("index.php/getmusiclist")
    @FormUrlEncoded
    fun getMusicList(@FieldMap params: Map<String, String>): Call<DJBitzModel.DJBitzMusicList.Result>

    @POST("index.php/gettopmusiclist")
    @FormUrlEncoded
    fun getTopMusicList(@FieldMap params: Map<String, String>): Call<DJBitzModel.DJBitzTopMusicList.Result>

    @POST("index.php/getmusicswithgenre")
    @FormUrlEncoded
    fun getMusicListWithGenre(@FieldMap params: Map<String, String>): Call<DJBitzModel.DJBitzMusicList.Result>

    @POST("index.php/getmusicswithdj")
    @FormUrlEncoded
    fun getMusicListWithDJ(@FieldMap params: Map<String, String>): Call<DJBitzModel.DJBitzMusicList.Result>

    @POST("index.php/getmusicinfo")
    @FormUrlEncoded
    fun getMusicInfo(@FieldMap params: Map<String, String>): Call<DJBitzModel.DJBitzMusicInfo.Result>

    @POST("index.php/sendrequest")
    @FormUrlEncoded
    fun postRequest(@FieldMap params: Map<String, String>): Call<DJBitzModel.DJBitzReQuest.Result>

    @POST("index.php/login")
    @FormUrlEncoded
    fun postLogin(@FieldMap params: Map<String, String>): Call<DJBitzModel.DJBitzUserInfo.Result>

    @POST("index.php/resetpassword")
    @FormUrlEncoded
    fun postResetPassword(@FieldMap params: Map<String, String>): Call<DJBitzModel.ResponseBody.Result>

    @POST("index.php/register")
    @FormUrlEncoded
    fun postRegistation(@FieldMap params: Map<String, String>): Call<DJBitzModel.ResponseBody.Result>

    @POST("index.php/like-music")
    @FormUrlEncoded
    fun postLike(@FieldMap params: Map<String, String>): Call<DJBitzModel.ResponseBody.Result>

    @POST("index.php/dislike-music")
    @FormUrlEncoded
    fun postDisLike(@FieldMap params: Map<String, String>): Call<DJBitzModel.ResponseBody.Result>

    @POST("index.php/add-comment")
    @FormUrlEncoded
    fun postAddComment(@FieldMap params: Map<String, String>): Call<DJBitzModel.ResponseBody.Result>

    @POST("index.php/update-comment")
    @FormUrlEncoded
    fun postUpdateComment(@FieldMap params: Map<String, String>): Call<DJBitzModel.ResponseBody.Result>

    @POST("index.php/delete-comment")
    @FormUrlEncoded
    fun postDeleteComment(@FieldMap params: Map<String, String>): Call<DJBitzModel.ResponseBody.Result>

    @POST("index.php/setprofile")
    @FormUrlEncoded
    fun postEditProfile(@FieldMap params: Map<String, String>): Call<DJBitzModel.DJBitzUserInfo.Result>

    @POST("index.php/deletecustomer")
    @FormUrlEncoded
    fun postDeleteAccount(@FieldMap params: Map<String, String>): Call<DJBitzModel.ResponseBody.Result>
}
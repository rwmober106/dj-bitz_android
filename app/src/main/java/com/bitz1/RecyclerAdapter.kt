package com.bitz1

import android.app.AlertDialog
import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.row_song.view.*
import androidx.core.widget.addTextChangedListener
import com.bitz1.retrofit2.DJBitzInterFace
import com.bitz1.retrofit2.DJBitzModel
import com.bitz1.utils.Constants
import com.squareup.picasso.Picasso
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class RecyclerAdapter(private val songs: ArrayList<DJBitzModel.DJBitzMusicList.MusicRecords>, private val viewStyle: Int? = 0, val mContext: MainActivity1) : RecyclerView.Adapter<RecyclerAdapter.SongRowHolder>() {
    var onItemClick: (((DJBitzModel.DJBitzMusicList.MusicRecords), adapterPosition: Int) -> Unit)? = null

    val DJBitzApiServe by lazy {
        DJBitzInterFace.create()
    }

    override fun getItemCount() = songs.size

    override fun onBindViewHolder(holder: RecyclerAdapter.SongRowHolder, position: Int) {
        val song = songs[position]
        holder.bind(song)
    }

    fun showError(error: String?) {
        try {
            if (mContext != null) {
                mContext.showHideProgressbar(false)
            }

            var message = error ?: mContext.getString(R.string.messager_load_server_error)
            AlertDialog.Builder(mContext)
                .setTitle(R.string.title_load_server_error)
                .setMessage(message)
                .setPositiveButton(
                    android.R.string.ok
                ) { dialog, which -> dialog.dismiss() }

                .show()
        } catch (ex: java.lang.Exception) {

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerAdapter.SongRowHolder {

        var inflatedView = parent.inflate(R.layout.row_song, false)
        if (viewStyle == 1) {
            inflatedView = parent.inflate(R.layout.row_genres, false)
        }

        return SongRowHolder(inflatedView)
    }

    inner class SongRowHolder(v: View) : RecyclerView.ViewHolder(v), View.OnClickListener {
        private var view: View = v
        private var song: DJBitzModel.DJBitzMusicList.MusicRecords? = null

        init {
            v.setOnClickListener(this)
        }

        override fun onClick(v: View) {
            onItemClick?.invoke(songs[adapterPosition], adapterPosition)

        }

        fun totalTime(totalTimes: String): String {
            var timesResult = ""
            try {
                val split = totalTimes.split(":")

                if (!split.get(0).equals("00")) {
                    if (split.get(0).substring(0, 1).equals("0")) {
                        timesResult += split.get(0).substring(1, 2) + ":"
                    } else {
                        timesResult += split.get(0) + ":"
                    }
                }

                timesResult += split.get(1) + ":"
                timesResult += split.get(2)

            } catch (e: Exception) {
                e.stackTrace
            }

            return timesResult
        }

        fun bind(songNow: DJBitzModel.DJBitzMusicList.MusicRecords) {
            if (viewStyle == 0) {
                song = songNow
                //Picasso.with(view.context).load(photo.url).into(view.itemImage)
                view.textViewTitle.text = songNow.DJ
                view.textViewDescription.text = songNow.name

                //test

                if (!songNow.comment_count.isNullOrEmpty()) {
                    view.countComment.text = songNow.comment_count
                } else {
                    view.countComment.text = "0"
                }

                if (!songNow.likes.isNullOrEmpty()) {
                    view.countFavourite.text = songNow.likes
                } else {
                    view.countFavourite.text = "0"
                }

                if (!songNow.playCounts.isNullOrEmpty()) {
                    view.countListened.text = songNow.playCounts
                } else {
                    view.countListened.text = "0"
                }

                ////

                if (!songNow.comment.isNullOrEmpty()) {
                    view.line3.visibility = View.VISIBLE
                } else {
                    view.line3.visibility = View.GONE
                }

                if (songNow.is_liked.equals("0")){
                    view.imgFavourite.setImageResource(R.mipmap.ic_favourite)
                }
                else{
                    view.imgFavourite.setImageResource(R.mipmap.ic_favourite_filled)
                }

                view.imgFavourite.setOnClickListener {

                    if (songNow.is_liked.equals("0")) {
                        try {
                            val params = HashMap<String, String>()
                            params["email"] = Constants.userInfo?.userInfo?.email ?: ""
                            params["music_id"] = songNow.id

                            if (mContext != null) {
                                mContext.showHideProgressbar(true)
                            }

                            DJBitzApiServe.postLike(params).enqueue(object : Callback<DJBitzModel.ResponseBody.Result> {

                                override fun onResponse(
                                    call: Call<DJBitzModel.ResponseBody.Result>,
                                    response: Response<DJBitzModel.ResponseBody.Result>
                                ) {
                                    if (mContext != null) {
                                        mContext.showHideProgressbar(false)
                                    }

                                    if (response.isSuccessful && response.body() != null && response.body()!!.status.toLowerCase().equals(
                                            "success"
                                        )
                                    ) {
                                        songs.get(adapterPosition).likes =
                                            (songs.get(adapterPosition).likes.toInt() + 1).toString()

                                        songs.get(adapterPosition).is_liked = "1"
                                        notifyItemChanged(adapterPosition)
                                    }
                                    else {
                                        //error
                                        if (response.body() != null && response.body()!!.msg != null) {
                                            showError(response.body()!!.msg)
                                        }else{
                                            showError(null)
                                        }
                                    }
                                }

                                override fun onFailure(call: Call<DJBitzModel.ResponseBody.Result>, t: Throwable) {
                                    showError(null)
                                }
                            })
                        } catch (e: Exception) {
                            e.stackTrace
                            showError(null)
                        }
                    } else {
                        try {
                            val params = HashMap<String, String>()
                            params["email"] = Constants.userInfo?.userInfo?.email ?: ""
                            params["music_id"] = songNow.id

                            if (mContext != null) {
                                mContext.showHideProgressbar(true)
                            }

                            DJBitzApiServe.postDisLike(params)
                                .enqueue(object : Callback<DJBitzModel.ResponseBody.Result> {

                                    override fun onResponse(
                                        call: Call<DJBitzModel.ResponseBody.Result>,
                                        response: Response<DJBitzModel.ResponseBody.Result>
                                    ) {
                                        if (mContext != null) {
                                            mContext.showHideProgressbar(false)
                                        }

                                        if (response.isSuccessful && response.body() != null && response.body()!!.status.toLowerCase().equals(
                                                "success"
                                            )
                                        ) {
                                            songs.get(adapterPosition).likes =
                                                (songs.get(adapterPosition).likes.toInt() - 1).toString()


                                            songs.get(adapterPosition).is_liked = "0"
                                            notifyItemChanged(adapterPosition)
                                        }
                                        else {
                                            //error
                                            if (response.body()!= null) {
                                                showError(response.body()!!.msg)
                                            }else{
                                                showError(null)
                                            }
                                        }
                                    }

                                    override fun onFailure(call: Call<DJBitzModel.ResponseBody.Result>, t: Throwable) {
                                        showError(null)
                                    }
                                })
                        } catch (e: Exception) {
                            e.stackTrace
                        }
                    }
                }

                view.imgComment.setOnClickListener {

                    if (view.line3.visibility == View.GONE) {
                        view.line3.visibility = View.VISIBLE
                    } else {
                        view.line3.visibility = View.GONE
                    }
                }

                view.btnSendComment.setOnClickListener {
                    view.btnSendComment.hideKeyboard()

                    try {
                        val params = HashMap<String, String>()
                        params["email"] = Constants.userInfo?.userInfo?.email ?: ""
                        params["music_id"] = songNow.id
                        params["comment"] = songs.get(adapterPosition).comment
                        if (mContext != null) {
                            mContext.showHideProgressbar(true)
                        }
                        DJBitzApiServe.postAddComment(params).enqueue(
                            object : Callback<DJBitzModel.ResponseBody.Result> {

                                override fun onResponse(
                                    call: Call<DJBitzModel.ResponseBody.Result>,
                                    response: Response<DJBitzModel.ResponseBody.Result>
                                ) {
                                    if (mContext != null) {
                                        mContext.showHideProgressbar(false)
                                    }

                                    if (response.isSuccessful && response.body() != null && response.body()!!.status.toLowerCase().equals(
                                            "success"
                                        )
                                    ) {
                                        view.editComment.setText("")
                                        songs.get(adapterPosition).comment = ""
                                        view.line3.visibility = View.GONE
                                        if (!songNow.comment_count.isNullOrEmpty()) {
                                            songNow.comment_count = ((songNow.comment_count).toInt() + 1).toString()
                                            view.countComment.text = songNow.comment_count
                                        } else {
                                            songNow.comment_count =  "1"
                                            view.countComment.text = songNow.comment_count
                                        }
                                    }
                                    else {
                                        //error
                                        if (response.body()!= null) {
                                            showError(response.body()!!.msg)
                                        }else{
                                            showError(null)
                                        }
                                    }
                                }

                                override fun onFailure(call: Call<DJBitzModel.ResponseBody.Result>, t: Throwable) {
                                    showError(null)
                                }

                            })
                    } catch (e: Exception) {
                        e.stackTrace
                    }


                }

                view.editComment.addTextChangedListener { text ->
                    songs.get(adapterPosition).comment = text.toString()
                }

                view.editComment.setText(songNow.comment)
                //////////////
                if (songNow.duration != null) {
                    view.textViewTime.text = totalTime(songNow.duration)
                } else {
                    view.textViewTime.text = ""
                }

                if (songNow.thumb != null && !songNow.thumb.isEmpty()) {
                    var url = Constants.BASE_URL + songNow.thumb.replace("\\", "")

                    Picasso.get()
                        .load(url)
                        .placeholder(R.mipmap.images)
                        .into(view.imageView)
                }
            }
        }
    }

    fun View.hideKeyboard() {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(windowToken, 0)
    }
}

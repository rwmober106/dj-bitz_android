package com.bitz1

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.row_song.view.*
import com.bitz1.retrofit2.DJBitzModel
import com.bitz1.utils.Constants
import com.squareup.picasso.Picasso


class DJRecyclerAdapter(private val artists: ArrayList<DJBitzModel.DJBitzDJList.DJRecords>) : RecyclerView.Adapter<DJRecyclerAdapter.ArtistRowHolder>() {
    var onItemClick: ((DJBitzModel.DJBitzDJList.DJRecords) -> Unit)? = null

    override fun getItemCount() = artists.size

    override fun onBindViewHolder(holder: DJRecyclerAdapter.ArtistRowHolder, position: Int) {
        val artist = artists[position]
        holder.bind(artist)  }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DJRecyclerAdapter.ArtistRowHolder {

        var inflatedView = parent.inflate(R.layout.row_artist, false)
        return ArtistRowHolder(inflatedView)
    }

    inner class ArtistRowHolder(v: View) : RecyclerView.ViewHolder(v), View.OnClickListener {
        private var view: View = v
        private var artist: DJBitzModel.DJBitzDJList.DJRecords? = null

        init {
            v.setOnClickListener(this)
        }

        override fun onClick(v: View) {
            onItemClick?.invoke(artists[adapterPosition])
        }

        fun bind(artist: DJBitzModel.DJBitzDJList.DJRecords) {
            this.artist = artist
            view.textViewTitle.text = artist.name
            if (artist.description != null && !artist.description.isEmpty()) {
                view.textViewDescription.text = artist.description
            } else {
                view.textViewDescription.text = ""
            }

            if (artist.avatar_url != null && !artist.avatar_url.isEmpty()) {
                var url = Constants.BASE_URL + artist.avatar_url.replace("\\","")

                Picasso.get()
                    .load(url)
                    .placeholder(R.mipmap.images)
                    .into(view.imageView)
            }
        }
    }
}
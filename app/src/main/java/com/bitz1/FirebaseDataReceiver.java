package com.bitz1;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.AudioAttributes;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import androidx.core.app.NotificationCompat;
import androidx.legacy.content.WakefulBroadcastReceiver;
import com.bitz1.live_video.AudienceActivity;

import java.util.List;

public class FirebaseDataReceiver extends WakefulBroadcastReceiver
{
    private final String TAG = "FirebaseDataReceiver";
    String title,message;
    public void onReceive(Context context, Intent intent)
    {
        Log.d(TAG, "I'm in!!!");
        if (intent.getExtras() != null)
        {
            for (String key : intent.getExtras().keySet())
            {
                Object value = intent.getExtras().get(key);
                Log.e("FirebaseDataReceiver", "Key: " + key + " Value: " + value);
                System.out.println("asnmsamasmmasmnas="+" KERYY: " + key);
                System.out.println("asnmsamasmmasmnas="+" Value: " + value);
                if(key.trim().equalsIgnoreCase("gcm.notification.title"))
                {
                    title= String.valueOf(value);
                }
                if(key.trim().equalsIgnoreCase("gcm.notification.body"))
                {
                    message= String.valueOf(value);
                }
                if(key.trim().equalsIgnoreCase("gcm.notification.token"))
                {
                    String token = String.valueOf(value);
                    PrefManager.setString(context, PrefManager.TOKEN, token);
                }
                if(key.trim().equalsIgnoreCase("gcm.notification.userId"))
                {
                    String userId = String.valueOf(value);
                    PrefManager.setString(context, PrefManager.Connection_USERID, userId);
                    //PrefManager.setString(context, PrefManager.Connection_USERID, "244");
                }
                if(key.trim().equalsIgnoreCase("gcm.notification.channelName"))
                {
                    String channelName= String.valueOf(value);
                    PrefManager.setString(context, PrefManager.CHANNEL, channelName);
                }
                if(key.trim().equalsIgnoreCase("gcm.notification.name"))
                {
                    String Name= String.valueOf(value);
                    PrefManager.setString(context, PrefManager.NMAE, Name);
                }

                //testing_notify(context,message,title);
                //sendNotification(context,message,title);

                if (!isAppIsInBackground(context.getApplicationContext()))
                {
                    //Toast.makeText(this, "Not Backgound", Toast.LENGTH_SHORT).show();
                    sendNotification(context,message,title);
                }
                else
                {
                    //Toast.makeText(this, "Backgound", Toast.LENGTH_SHORT).show();
                    //sendNotification(context,message,title);
                }
            }
        }
    }

    private void testing_notify(Context context,String message,String title)
    {

        //intent.putExtra(Constants.KEY_CLIENT_ROLE, role);
        //intent.setClass(context, LiveBoardCastActivity.class);

        Intent intent = new Intent(context, AudienceActivity.class);
        //intent.putExtra(Constants.KEY_CLIENT_ROLE, 1);

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0 , intent, PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.fake_user_icon)
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.fake_user_icon))
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);
        NotificationManager notificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(1, notificationBuilder.build());
    }

    public static boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }

        return isInBackground;
    }

    @SuppressLint("WrongConstant")
    private void sendNotification(Context context, String messageBody, String title) {

        String id ="my_channel_01";
        Intent intent = new Intent(context, AudienceActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder notificationBulderOreo = new NotificationCompat.Builder(context,id);
        notificationBulderOreo .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)
                .setPriority(Notification.PRIORITY_MAX);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "Waite customer";// The user-visible name of the channel.
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel(id,name,importance);
            AudioAttributes attributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                    .build();

            mChannel.setDescription(messageBody);
            mChannel.enableLights(true);
            mChannel.enableVibration(true);
            mChannel.setSound(defaultSoundUri,attributes);
            mChannel.setLightColor(context.getResources().getColor(R.color.colorAccent));
            notificationManager.createNotificationChannel(mChannel);

            notificationBulderOreo.setBadgeIconType(R.drawable.main_logo)
                    .setChannelId(id)
                    .setWhen(System.currentTimeMillis());
        }
        else {
            notificationBulderOreo.setColor(context.getResources().getColor(R.color.colorAccent));
        }

        notificationManager.notify(0, notificationBulderOreo.build());

        //Start service:
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            context.startForegroundService(new Intent(context, AudienceActivity.class));
        } else {
            context.startService(new Intent(context, AudienceActivity.class));
        }
    }
}


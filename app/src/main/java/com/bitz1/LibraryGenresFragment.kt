package com.bitz1

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bitz1.retrofit2.DJBitzInterFace
import com.bitz1.retrofit2.DJBitzModel
import com.bitz1.utils.Constants
import com.squareup.picasso.Picasso
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.row_song.view.*

class LibraryGenresFragment : Fragment() {

    companion object {
        fun newInstance(): LibraryGenresFragment {
            return LibraryGenresFragment()
        }
    }

    val DJBitzApiServe by lazy {
        DJBitzInterFace.create()
    }


    private lateinit var gridLayoutManager: GridLayoutManager
    private lateinit var adapter: RecyclerAdapter1
    private var songsList = ArrayList<DJBitzModel.DJBitzGenresList.GenresRecords>()
    private lateinit var recyclerView: RecyclerView


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var root = inflater?.inflate(R.layout.fragment_library_genres, container, false)

        recyclerView = root.findViewById<RecyclerView>(R.id.recyclerView)

        gridLayoutManager = GridLayoutManager(context, 2)
        recyclerView.layoutManager = gridLayoutManager
        var mainActivity = (activity as MainActivity1)

        adapter = RecyclerAdapter1(songsList, 1)
        adapter.onItemClick = { song ->
            var libraryFragment = (activity as MainActivity1).supportFragmentManager .findFragmentByTag("LibraryFragment") as LibraryFragment
            if (libraryFragment != null) {
                var fragment = LibrarySongsFragment.newInstance()
                fragment.showToolbar = true
                fragment.getListMusic(song.id)
                fragment.textTitle = song.name
                libraryFragment.displayFragment(fragment, "LibrarySongsFragment", true)
            }

            if (mainActivity.isPlaying) {
                mainActivity.rlPlayer.visibility = View.VISIBLE
            }
            else{
                mainActivity.rlPlayer.visibility = View.GONE
            }
        }

        recyclerView.adapter = adapter

        if (activity != null) {
            mainActivity.showHideProgressbar(true)
        }

        DJBitzApiServe.getGenresList()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result ->
                    if (activity != null) {
                        mainActivity.showHideProgressbar(false)
                    }

                    if (result != null && result.status.toLowerCase().equals(
                            "success"
                        )
                    ) {
                        showResult(result)
                    }
                    else {
                        //error
                        if (result != null && result.msg != null) {
                            showError(result.msg)
                        }else{
                            showError(null)
                        }
                    }
                },
                { error -> showError(null) }
            )

        return root
    }

    fun showError(error: String?) {
        try {
            if (activity != null) {
                (activity as MainActivity1).showHideProgressbar(false)
            }

            var message = error ?: getString(R.string.messager_load_server_error)
            AlertDialog.Builder(activity)
                .setTitle(R.string.title_load_server_error)
                .setMessage(message)
                .setPositiveButton(
                    android.R.string.ok
                ) { dialog, which -> dialog.dismiss() }

                .show()
        } catch (ex: java.lang.Exception) {

        }
    }

    fun showResult(result: DJBitzModel.DJBitzGenresList.Result){
        songsList.clear()

        songsList.addAll(result.result.genresRecords)
        adapter.notifyDataSetChanged()


    }

    class RecyclerAdapter1(private val songs: ArrayList<DJBitzModel.DJBitzGenresList.GenresRecords>, private val viewStyle: Int? = 0) : RecyclerView.Adapter<RecyclerAdapter1.SongRowHolder>() {
        var onItemClick: ((DJBitzModel.DJBitzGenresList.GenresRecords) -> Unit)? = null

        override fun getItemCount() = songs.size

        override fun onBindViewHolder(holder: RecyclerAdapter1.SongRowHolder, position: Int) {
            val song = songs[position]
            holder.bind(song)  }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerAdapter1.SongRowHolder {

            var inflatedView = parent.inflate(R.layout.row_genres, false)

            return SongRowHolder(inflatedView)
        }

        inner class SongRowHolder(v: View) : RecyclerView.ViewHolder(v), View.OnClickListener {
            private var view: View = v
            private var song: DJBitzModel.DJBitzGenresList.GenresRecords? = null

            init {
                v.setOnClickListener(this)
            }

            override fun onClick(v: View) {
                onItemClick?.invoke(songs[adapterPosition])

            }

            fun bind(song: DJBitzModel.DJBitzGenresList.GenresRecords) {

                this.song = song
                if (song.thumb_img != null && !song.thumb_img.isEmpty()) {
                    var url = Constants.BASE_URL + song.thumb_img.replace("\\","")

                    Picasso.get()
                        .load(url)
                        .placeholder(R.mipmap.images)
                        .into(view.imageView)
                }
                view.textViewTime.text = song.name

            }
        }
    }
}
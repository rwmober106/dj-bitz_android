package com.bitz1

import com.bitz1.retrofit2.DJBitzModel
import java.util.*

class Events {
    class SongChanged internal constructor(val song: DJBitzModel.DJBitzMusicList.MusicRecords?)

    class SongStateChanged internal constructor(val isPlaying: Boolean)

    class PlaylistUpdated internal constructor(val songs: ArrayList<DJBitzModel.DJBitzMusicList.MusicRecords>)

    class ProgressUpdated internal constructor(val progress: Int)

    class HandlePrepare internal constructor()

    class NoStoragePermission internal constructor()
}
